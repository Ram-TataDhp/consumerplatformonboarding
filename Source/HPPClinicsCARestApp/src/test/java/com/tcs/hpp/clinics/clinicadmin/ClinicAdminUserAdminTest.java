/**
 * 
 *//*
package com.tcs.hpp.clinics.clinicadmin;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

*//**
 * @author 219331
 *
 *//*
public class ClinicAdminUserAdminTest {
	private static Logger logger=Logger.getLogger(ClinicAdminUserAdminTest.class);
	private static Client  client= null;
	private WebResource webResource = null;
	
	private String token_third= "MEDI1_1431509378695";
	private static String ipAddress="52.11.103.174";
	private static String adminUserLoginName="JAI2";
	private String adminUserLoginName_third="MEDI1";
	private static String token= "TDH2_bf08f37d-b616-433c-ad8b-00d60df65573";
	private static String password="pass@123";
	private static String organisationId="59";
	private String token_third= "MEDI1_1431509378695";
	private static  String ipAddress="192.168.1.83";
	private static String adminUserLoginName="TDH2";
	private String adminUserLoginName_third="MEDI1";
	private static String token= "TDH2_0d449585-7ed5-45e1-b45c-479abb1ee715";
	private static String password="tatadhp";
	private static String organisationId="8";
	private String tokenLogOut="";
	
	@Before
	public void methodInit()
	{
		
	}
	@BeforeClass
	public static void classInit()
	{
		client = Client.create();
	}
	//(+) The below test case is used to get the home page data..
	@Test
	public void getHomePageData()
	{
		
		webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getHomePageData");
		
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);		
		
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
				
				logger.info("Output from Server .... "+jsonOutput.toString());
				
				
				Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is used to validate the mandatory fields...;
	@Test
	public void appLoginMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/appLogin");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName","");
		loginJson.put("password","");
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		

				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		

		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
			}
			else
			{
				Assert.fail();
			}
				logger.info("Output from Server .... "+jsonOutput.toString());
				logger.info("Token value==>"+token);
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is used to validate the user details...
	@Test
	public void appLoginInvalidCredentials() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/appLogin");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName",adminUserLoginName);
		loginJson.put("password","wrong");
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		

				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9101, Integer.parseInt(jsonOutput.get("errorCode").toString()));
			}
			else
			{
				Assert.fail();
			}
				logger.info("Output from Server .... "+jsonOutput.toString());
				logger.info("Token value==>"+token);
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is used to authenticate the user successfully...
	@Test
	public void appLoginsSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/appLogin");
	
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName",adminUserLoginName);
		loginJson.put("password",password);
		
		
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
			
		

				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		

		logger.info("Output from Server .... ");
		
		try {
			if(response.getStatus()==200)
			{
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				token = jsonOutput.getString("token");
				tokenLogOut=token;
				
				Assert.assertEquals(200, response.getStatus());
				logger.info("Output from Server .... "+jsonOutput.toString());
				logger.info("Token value==>"+token);
			}
			else
			{
				Assert.fail();
			}
			
			
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void logOut() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/logout");
		
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName",adminUserLoginName);
		
		loginJson.put("organisationId",organisationId);
		loginJson.put("token",token);
		
		
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
			
		

				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		

		logger.info("Output from Server .... ");
		
		try {
			if(response.getStatus()==200)
			{
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				
				logger.info("Output from Server .... "+jsonOutput.toString());
				logger.info("Token value==>"+token);
			}
			
		
			
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is used to validate the double login check...
	@Test
	public void appDoubleLogin() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/appLogin");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName",adminUserLoginName);
		loginJson.put("password",password);
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		

				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(8002, jsonOutput.get("errorCode"));
			}
			else
			{
				Assert.fail();
			}
				logger.info("Output from Server .... "+jsonOutput.toString());
				logger.info("Token value==>"+token);
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is used to validate the Mandatory fields...
	@Test
	public void changePasswordMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/changePassword");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName","");
		loginJson.put("password",password);
		loginJson.put("token",token);
		loginJson.put("organisationId", "");
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		Assert.assertEquals(200, response.getStatus());
		
				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		

		logger.info("Output from Server .... ");
		
		try {
			
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
			}
			else
			{
				Assert.fail();
			}
				logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is validate the token...
	@Test
	public void changePasswordInvalidToken() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/changePassword");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName","CARE1");
		loginJson.put("password",password);
		loginJson.put("token","CARE1_1233455");
		loginJson.put("organisationId", organisationId);
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		Assert.assertEquals(200, response.getStatus());
		
				long lEndTime = System.currentTimeMillis();
			 
				long difference = lEndTime - lStartTime;
				
				long differenceInSec = difference;
			 
				logger.info("Elapsed seconds: " + differenceInSec);
		

		logger.info("Output from Server .... ");
		
		try {
			
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9101, Integer.parseInt(jsonOutput.get("errorCode").toString()));
			}
			else
			{
				Assert.fail();
			}
				logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	//(+) this test is used to the change the password successfully...
	@Test
	public void changePasswordSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/changePassword");
		JSONObject loginJson = new JSONObject();
		loginJson.put("userLoginName","CARE1");
		loginJson.put("password",password);
		loginJson.put("token",token);
		loginJson.put("organisationId", organisationId);
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,loginJson.toString());	
		
		
				long lEndTime = System.currentTimeMillis();
				long difference = lEndTime - lStartTime;
				long differenceInSec = difference;
		
				logger.info("Elapsed seconds: " + differenceInSec);
		try {
			
			if(response.getStatus()==200)
			{
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				
				Assert.assertEquals(200, response.getStatus());
				logger.info("Output from Server .... "+jsonOutput.toString());
				
			}
			else
			{
				Assert.fail();
			}
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	/*
	 * Auto Search User Junit test cases starts here...
	 
	//(-) The below test case is to get the list of matching search users...
		@Test
		public void autoSerachUserMandatoryFields() throws JSONException
		{
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/autoSuggestionUsers");
			JSONObject autoSuggestionuser = new JSONObject();
			autoSuggestionuser.put("userLoginName","");
			autoSuggestionuser.put("token","");
			autoSuggestionuser.put("organisationId","");
			autoSuggestionuser.put("searchText","");
			long lStartTime = System.currentTimeMillis();
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,autoSuggestionuser.toString());	
			long lEndTime = System.currentTimeMillis();
			long difference = lEndTime - lStartTime;
			long differenceInSec = difference;
			logger.info("Elapsed seconds: " + differenceInSec);
			logger.info("Output from Server .... ");
			
				try {
					JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
						if(response.getStatus()==200)
						{
							Assert.assertEquals(200, response.getStatus());
							logger.info("Output from Server .... "+jsonOutput.toString());
							
						}
						else
						{
							logger.info("Output from Server .... "+jsonOutput.toString());
							Assert.fail();
						}
				
				} catch (Exception e) {			
					e.printStackTrace();
				}
		}
		//(-) The below test case is to get the list of matching search users...
		@Test
		public void autoSerachUserBusinessValidations() throws JSONException
		{
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/autoSuggestionUsers");
			JSONObject autoSuggestionuser = new JSONObject();
			autoSuggestionuser.put("userLoginName",adminUserLoginName);
			autoSuggestionuser.put("token",token);
			autoSuggestionuser.put("organisationId","2S");
			autoSuggestionuser.put("searchText","Chin");
			long lStartTime = System.currentTimeMillis();
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,autoSuggestionuser.toString());	
			long lEndTime = System.currentTimeMillis();
			long difference = lEndTime - lStartTime;
			long differenceInSec = difference;
			logger.info("Elapsed seconds: " + differenceInSec);
			logger.info("Output from Server .... ");
			
				try {
					JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
					
					if(response.getStatus()==500)
					{
						
						Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
					}
					else
					{
						Assert.fail();
					}
				
				} catch (Exception e) {			
					e.printStackTrace();
				}
		}
	//(+) The below test case is to get the list of matching search users...
	@Test
	public void autoSerachUserSuccess() throws JSONException
	{
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/autoSuggestionUsers");
		JSONObject autoSuggestionuser = new JSONObject();
		autoSuggestionuser.put("userLoginName",adminUserLoginName);
		autoSuggestionuser.put("token",token);
		autoSuggestionuser.put("organisationId","2");
		autoSuggestionuser.put("searchText","Chin");
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,autoSuggestionuser.toString());	
		long lEndTime = System.currentTimeMillis();
		long difference = lEndTime - lStartTime;
		long differenceInSec = difference;
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
			try {
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
					if(response.getStatus()==200)
					{
						Assert.assertEquals(200, response.getStatus());
						logger.info("Output from Server .... "+jsonOutput.toString());
						
					}
					else
					{
						logger.info("Output from Server .... "+jsonOutput.toString());
						Assert.fail();
					}
			
			} catch (Exception e) {			
				e.printStackTrace();
			}
	}
	/*
	 * Auto Search User Junit test cases ends here...
	 
	
	
	 * Create User JUnit Test cases starts here...
	 
	//(-) The below test case is checking mandatory fields of Create User service...
	@Test
	public void createUserMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", "");
		createUserJson.put("adminUserId", "");
		createUserJson.put("siteId", "");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","");
		userJson.put("title","");
		userJson.put("bloodGroup","");
		userJson.put("gender", "");
			
		
		userJson.put("dob","");
		
		userJson.put("mobileNumber","");
		userJson.put("speciality","");
		userJson.put("email","");
		userJson.put("address","");
		userJson.put("medicalRegNo","");
		
		
		userJson.put("signature","");
		userJson.put("userPhoto","");
		userJson.put("roleType","");
		userJson.put("isActive","");
		userJson.put("organisationId",organisationId);
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is checking business validations of Create User service...
	@Test
	public void createUserBusinessValidations() throws JSONException
	{
			
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
			JSONObject createUserJson = new JSONObject();
			createUserJson.put("token", token);
			createUserJson.put("userLoginName", adminUserLoginName);
			createUserJson.put("adminUserId", "1D");
			createUserJson.put("siteId", "1S");
			List<String> roldIdList = new ArrayList<String>();
			 roldIdList.add("1");
			 roldIdList.add("2D");
			 
			
			 
			List<String> locationIdList = new ArrayList<String>(); 
			JSONArray locationArray = new JSONArray();
			locationArray.put("1");
			locationIdList.add("1D");
			createUserJson.put("roleId",roldIdList);
			createUserJson.put("locationId", locationIdList);
			JSONObject userJson = new JSONObject();
			userJson.put("userName","Lakshmi Chintala");
			userJson.put("title","Dr.");
			userJson.put("bloodGroup","O+");
			userJson.put("gender", "Female");
				
			
			userJson.put("dob","1991-05-20");
			
			userJson.put("mobileNumber","123451F2345");
			userJson.put("speciality","Gynacologist");
			userJson.put("email","nag219331@yahoo.com");
			userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
			userJson.put("medicalRegNo","1003");
			
			
			userJson.put("signature","FTP Server URl of Signature File");
			userJson.put("userPhoto","FTP Server URl of userPhoto File");
			userJson.put("roleType","Doctor");
			userJson.put("isActive","A");
			userJson.put("organisationId",organisationId);
			
			createUserJson.put("user", userJson);
			long lStartTime = System.currentTimeMillis();
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
			
			long lEndTime = System.currentTimeMillis();
			 
			long difference = lEndTime - lStartTime;
			
			long differenceInSec = difference;
		 
			logger.info("Elapsed seconds: " + differenceInSec);
			
			
			try {
				
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				
				if(response.getStatus()==500)
				{
					
					Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));
					
				}
				else
				{
					Assert.fail();
				}
				logger.info("Output from Server .... "+jsonOutput.toString());
					
			
			} catch (Exception e) {			
				e.printStackTrace();
			}
		}
	//(-) The below test case is to validating subscription plan activation date,before creating an user...
	@Test
	public void createUserPlanActivateDateFuture() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token_third);
		createUserJson.put("userLoginName", adminUserLoginName_third);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Jhon Chintala");
		userJson.put("title","Master");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1984-05-20");
		
		userJson.put("mobileNumber","1234554321");
		userJson.put("speciality","Radiologist");
		userJson.put("email","jhon.c@chintala.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1010");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","4");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9004, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validating subscription plan activation date,before creating an user...
	@Test
	public void createUserPlanDeActivateDateOlder() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Jhon Chintala");
		userJson.put("title","Master");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1984-05-20");
		
		userJson.put("mobileNumber","1234554321");
		userJson.put("speciality","Radiologist");
		userJson.put("email","jhon.c@chintala.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1010");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","5");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9010, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to create the user successful.
	@Test
	public void createDoctorUserSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Lakshmi Chintala");
		userJson.put("title","Dr.");
		userJson.put("bloodGroup","O+");
		userJson.put("gender", "Female");
			
		
		userJson.put("dob","1991-05-20");
		
		userJson.put("mobileNumber","1234512345");
		userJson.put("speciality","Gynacologist");
		userJson.put("email","nag219331@yahoo.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1003");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","3");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			Assert.assertEquals(String.valueOf(jsonOutput.get("successMessage")),200, response.getStatus());
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to create the user successful.
	@Test
	public void createStaffUserSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Lakshmi Chintala");
		userJson.put("title","Dr.");
		userJson.put("bloodGroup","O+");
		userJson.put("gender", "Female");
			
		
		userJson.put("dob","1991-05-20");
		
		userJson.put("mobileNumber","1234512345");
		userJson.put("speciality","Gynacologist");
		userJson.put("email","nag219331@yahoo.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1003");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Staff");
		userJson.put("isActive","A");
		userJson.put("organisationId","3");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			Assert.assertEquals(String.valueOf(jsonOutput.get("successMessage")),200, response.getStatus());
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validate the Doctors limit based on no of license count defined as per plan..  
	@Test
	public void createUserExceedDoctorCount() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Satya Nadendla");
		userJson.put("title","Master");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1984-05-20");
		
		userJson.put("mobileNumber","1234554321");
		userJson.put("speciality","Radiologist");
		userJson.put("email","jhon.c@chintala.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1010");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","3");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9003, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validate the doctor to staff ratio condition...
	@Test
	public void createUserExceedStaffCount() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/createUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("1");
		 roldIdList.add("2");
		 
		
		 
		List<String> locationIdList = new ArrayList<String>(); 
		JSONArray locationArray = new JSONArray();
		locationArray.put("1");
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userName","Jhon Chintala");
		userJson.put("title","Master");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1984-05-20");
		
		userJson.put("mobileNumber","1234554321");
		userJson.put("speciality","Radiologist");
		userJson.put("email","jhon.c@chintala.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1010");
		
		
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","3");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9007, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}

	/*
	 * Create User JUnit Test cases ends here...
	 
	
	
	 * Update User Junit Test cases starts here...
	 
	
	//(-) The below test case is to validate the mandatory fields...
	@Test
	public  void updateUserMandatoryFields() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/updateUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", "");
		createUserJson.put("userLoginName", "");
		createUserJson.put("adminUserId", "");
		createUserJson.put("siteId", "");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("");
		 roldIdList.add("");
		 
		List<String> locationIdList = new ArrayList<String>(); 
		
		locationIdList.add("");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userId", "");
		userJson.put("userName","");
		userJson.put("title","");
		userJson.put("bloodGroup","");
		userJson.put("gender", "");
			
		
		userJson.put("dob","");
		userJson.put("mobileNumber","");
		userJson.put("speciality","");
		userJson.put("email","");
		userJson.put("address","");
		userJson.put("medicalRegNo","");
		userJson.put("signature","");
		userJson.put("userPhoto","");
		userJson.put("roleType","");
		userJson.put("isActive","");
		userJson.put("organisationId","");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validate the user field values..
	@Test
	public  void updateUserBusinessValidation() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/updateUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1S");
		createUserJson.put("siteId", "1S");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("2S");
		 roldIdList.add("1");
		 
		List<String> locationIdList = new ArrayList<String>(); 
		
		locationIdList.add("1S");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userId", "9a");
		userJson.put("userName","Nagesh Chintala");
		userJson.put("title","Dr.");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1985-06-30");
		userJson.put("mobileNumber","9581153555");
		userJson.put("speciality","Diabetologist");
		userJson.put("email","kumar.chintala@yahoo.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1001");
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","3e");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validate the user existence...
	@Test
	public  void updateUserInvalidUserId() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/updateUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("2");
		 roldIdList.add("1");
		 
		List<String> locationIdList = new ArrayList<String>(); 
		
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userId", "100000");
		userJson.put("userName","Nagesh Chintala");
		userJson.put("title","Dr.");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1985-06-30");
		userJson.put("mobileNumber","9581153555");
		userJson.put("speciality","Diabetologist");
		userJson.put("email","kumar.chintala@yahoo.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1001");
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","2");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
	     ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9110, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to update the user details successfully...
	@Test
	public  void updateUserSuccessful() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/updateUser");
		JSONObject createUserJson = new JSONObject();
		createUserJson.put("token", token);
		createUserJson.put("userLoginName", adminUserLoginName);
		createUserJson.put("adminUserId", "1");
		createUserJson.put("siteId", "1");
		List<String> roldIdList = new ArrayList<String>();
		 roldIdList.add("2");
		 roldIdList.add("1");
		 
		List<String> locationIdList = new ArrayList<String>(); 
		
		locationIdList.add("1");
		createUserJson.put("roleId",roldIdList);
		createUserJson.put("locationId", locationIdList);
		JSONObject userJson = new JSONObject();
		userJson.put("userId", "9");
		userJson.put("userName","Nagesh Chintala");
		userJson.put("title","Dr.");
		userJson.put("bloodGroup","B+");
		userJson.put("gender", "Male");
			
		
		userJson.put("dob","1985-06-30");
		userJson.put("mobileNumber","9581153555");
		userJson.put("speciality","Diabetologist");
		userJson.put("email","kumar.chintala@yahoo.com");
		userJson.put("address","Flat#201,MIG-76,DRC ph-2,HyderNagar,kukatpally,Hyderabad-500072,Telangana");
		userJson.put("medicalRegNo","1001");
		userJson.put("signature","FTP Server URl of Signature File");
		userJson.put("userPhoto","FTP Server URl of userPhoto File");
		userJson.put("roleType","Doctor");
		userJson.put("isActive","A");
		userJson.put("organisationId","2");
		
		createUserJson.put("user", userJson);
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,createUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/*
	 * Update User Junit Test cases ends here...
	 
	
	
	 * Search User Junits starts here...
	 
	//(-) The below test case is to search the user details successfully...
	
	public void searchUserMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/retrieveUsers");
		JSONObject searchUserJson = new JSONObject();
		searchUserJson.put("token", "");
		searchUserJson.put("userLoginName", "");
		searchUserJson.put("searchText", "");		
		searchUserJson.put("organisationId","");
		searchUserJson.put("startRow", "");
		searchUserJson.put("interval", "");
		
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				//Output--> {"user":[{"userName":"Nagesh","userLoginName":"APL19","gender":"Male","mobileNo":"9666712227","signature":"FTP Server URl of Signature File","userPhoto":"FTP Server URl of userPhoto File","roleType":"Doctor","isactive":"A"},{"userName":"Nagesh","userLoginName":"APL19","gender":"Male","mobileNo":"9666712227","signature":"FTP Server URl of Signature File","userPhoto":"FTP Server URl of userPhoto File","roleType":"Doctor","isactive":"A"},{"userName":"Nagesh","userLoginName":"APL19","gender":"Male","mobileNo":"9666712227","signature":"FTP Server URl of Signature File","userPhoto":"FTP Server URl of userPhoto File","roleType":"Doctor","isactive":"A"}]}
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to search the user details successfully...
	@Test
	public void searchUserBusinessValidation() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/retrieveUsers");
		JSONObject searchUserJson = new JSONObject();
		searchUserJson.put("token", token);
		searchUserJson.put("userLoginName", adminUserLoginName);
		searchUserJson.put("searchText", "");		
		searchUserJson.put("organisationId","2s");
		searchUserJson.put("startRow", "1s");
		searchUserJson.put("interval", "2d");
		
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to search the user details successfully...
	@Test
	public void searchUserSuccessful() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/retrieveUsers");
		JSONObject searchUserJson = new JSONObject();
		searchUserJson.put("token", token);
		searchUserJson.put("userLoginName", adminUserLoginName);
		searchUserJson.put("searchText", "");		
		searchUserJson.put("organisationId","2");
		searchUserJson.put("startRow", "");
		searchUserJson.put("interval", "");
		
		long lStartTime = System.currentTimeMillis();
		
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
				Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	/*
	 * Search User Junits end here...
	 
	
	
	
	 * User Status update starts here...
	 
	
	//(-) The below test case is to validate the mandatory fields...
	@Test
	public void userStatusUpdateMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", "");
		userStatusUpdateJson.put("userLoginName", "");
		userStatusUpdateJson.put("isActive", "");		
		userStatusUpdateJson.put("organisationId","");
		userStatusUpdateJson.put("userId","");
		userStatusUpdateJson.put("roleType", "");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is used to validae the field value..check
	@Test
	public void userStatusUpdateBusinessValidations() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("userLoginName", adminUserLoginName);
		userStatusUpdateJson.put("isActive", "I");		
		userStatusUpdateJson.put("organisationId","2ss");
		userStatusUpdateJson.put("userId","9s");
		userStatusUpdateJson.put("roleType", "Doctor");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	//(-) The below test case is to check whether the doctor is having any appointments before making him inactive
	@Test
	public void userStatusUpdateDoctorInactiveCheck() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", "511_1424416401192");
		userStatusUpdateJson.put("userLoginName", "511");
		userStatusUpdateJson.put("isActive", "I");		
		userStatusUpdateJson.put("organisationId","1");
		userStatusUpdateJson.put("userId","511");
		userStatusUpdateJson.put("roleType", "Doctor");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		logger.info("Doctor Inactive Check");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9033, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	//(-) The below test case is validate the doctors count while updating doctor user from inactive to active
	@Test
	public void userStatusUpdateExceedDoctorsCount() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("userLoginName", adminUserLoginName);
		userStatusUpdateJson.put("isActive", "A");		
		userStatusUpdateJson.put("organisationId","2");
		userStatusUpdateJson.put("userId","9");
		userStatusUpdateJson.put("roleType", "Doctor");
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9003, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is update the user status successfully.
	@Test
	public void userStatusUpdateSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token_third);
		userStatusUpdateJson.put("userLoginName", adminUserLoginName_third);
		userStatusUpdateJson.put("isActive", "A");		
		userStatusUpdateJson.put("organisationId","3");
		userStatusUpdateJson.put("userId","2");
		userStatusUpdateJson.put("roleType", "Doctor");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			
			logger.info("Output from Server .... "+jsonOutput.toString());
				Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is validate the dotor to staff ratio while updating staff user from inactive to active
	@Test
	public void userStatusUpdateExceedStaffCount() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userStatusUpdate");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("userLoginName", adminUserLoginName);
		userStatusUpdateJson.put("isActive", "A");		
		userStatusUpdateJson.put("organisationId","2");
		userStatusUpdateJson.put("userId","15");
		userStatusUpdateJson.put("roleType", "Staff");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9003, Integer.parseInt(jsonOutput.get("errorCode").toString()));
				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	/*
	 * User Status update ends here...
	 
	
	 * Get User Details starts here...
	 
	//(-) The below test case is to get theuser details successfully..
	@Test
	public void getUserDetailsMandatoryField() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getUserDetails");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", "");
		userStatusUpdateJson.put("adminUserLoginName", "");
		userStatusUpdateJson.put("userLoginName", "");		
		userStatusUpdateJson.put("organisationId","");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to get theuser details successfully..
	@Test
	public void getUserDetailsBusinessValidation() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getUserDetails");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("adminUserLoginName", adminUserLoginName);
		userStatusUpdateJson.put("userLoginName", "CARE3");		
		userStatusUpdateJson.put("organisationId","2A");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validate the user login name...
	@Test
	public void getUserDetailsInValidUser() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getUserDetails");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("adminUserLoginName", "CARE1");
		userStatusUpdateJson.put("userLoginName", "CARE3333");		
		userStatusUpdateJson.put("organisationId","2");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9110, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			
			logger.info("Output from Server .... "+jsonOutput.toString());
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to get theuser details successfully..
	@Test
	public void getUserDetailsSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getUserDetails");
		JSONObject userStatusUpdateJson = new JSONObject();
		userStatusUpdateJson.put("token", token);
		userStatusUpdateJson.put("adminUserLoginName", adminUserLoginName);
		userStatusUpdateJson.put("userLoginName", "CARE3");		
		userStatusUpdateJson.put("organisationId","2");
		
		
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userStatusUpdateJson.toString());
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
				Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	/*
	 * Get User Details ends here...
	 
	
	
	 *  User Master details starts here...
	 
	//(-) The below test case is to validae the mandatory fields....
	@Test
	public void userMasterDetailsMandatoryFields() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", "");
		userMasterJson.put("userLoginName", "");		
		userMasterJson.put("organisationId","");
		
		long lStartTime = System.currentTimeMillis();
	ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to validae the mandatory fields....
	@Test
	public void userMasterDetailsBusinessValidations() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", token);
		userMasterJson.put("userLoginName", adminUserLoginName);		
		userMasterJson.put("organisationId","2S");
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) the below test case is to get the User master Details successfully...
	@Test
	public void userMasterDetailsSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/userMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", token);
		userMasterJson.put("userLoginName", adminUserLoginName);		
		userMasterJson.put("organisationId","2");
		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	/*
	 *  User Master details ends here...
	 
	
	
	 * Forgot Password Junit starts here...
	 
	
	//(-) The below test case is to reset the password and sent to registered user mobile and email...
	@Test
	public void forgotPasswordMandatoryFiled() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/forgotPassword");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("userLoginName", "");		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}	
			logger.info("Output from Server .... "+jsonOutput.toString());
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case is to reset the password and sent to registered user mobile and email...
	@Test
	public void forgotPasswordInvalidUser() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/forgotPassword");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("userLoginName", "CARE333");		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9110, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}
			
			logger.info("Output from Server .... "+jsonOutput.toString());
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case is to reset the password and sent to registered user mobile and email...
	@Test
	public void forgotPasswordSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/forgotPassword");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("userLoginName", "CCC1");		
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}

	/*
	 * Forgot Password Junit ends here...
	 
	
	
	 * Service Master Junit ends here...
	 
	//(-) The below test case to get the service master details successfully...
	@Test
	public void serviceMasterMandatoryFileds() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/serviceMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", "");
		userMasterJson.put("userLoginName", "");		
		userMasterJson.put("organisationId","");
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}	
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(-) The below test case to get the service master details successfully...
	@Test
	public void serviceMasterBusinessValidation() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/serviceMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", token);
		userMasterJson.put("userLoginName", adminUserLoginName);		
		userMasterJson.put("organisationId","2S");
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
			
			if(response.getStatus()==500)
			{
				Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));				
			}
			else
			{
				Assert.fail();
			}	
			logger.info("Output from Server .... "+jsonOutput.toString());
				
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	//(+) The below test case to get the service master details successfully...
	@Test
	public void serviceMasterSuccess() throws JSONException
	{
		
		WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/serviceMaster");
		JSONObject userMasterJson = new JSONObject();
		userMasterJson.put("token", token);
		userMasterJson.put("userLoginName", adminUserLoginName);		
		userMasterJson.put("organisationId","2");
		long lStartTime = System.currentTimeMillis();
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,userMasterJson.toString());		
		
		long lEndTime = System.currentTimeMillis();
		 
		long difference = lEndTime - lStartTime;
		
		long differenceInSec = difference;
	 
		logger.info("Elapsed seconds: " + differenceInSec);
		logger.info("Output from Server .... ");
		
		try {
			
			String jsonOutput = response.getEntity(String.class) ;
			logger.info("Output from Server .... "+jsonOutput.toString());
			Assert.assertEquals(200, response.getStatus());
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	/*
	 * Service Master Junit ends here...
	 
	
	
	 * GetOperationalRoleList JUnit Test cases starts here...
	 
	//(-) The below test case is checking mandatory fields of GetOperationalRoleList service...
		@Test
		public void getOperationalRoleListMandatoryField() throws JSONException
		{
			
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getOperationalRoleList");
			JSONObject getOperationalRoleListJson = new JSONObject();
			getOperationalRoleListJson.put("token", token);
			getOperationalRoleListJson.put("userLoginName", "");
			getOperationalRoleListJson.put("organisationId","");
			
			long lStartTime = System.currentTimeMillis();
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getOperationalRoleListJson.toString());		
			
			long lEndTime = System.currentTimeMillis();
			 
			long difference = lEndTime - lStartTime;
			
			long differenceInSec = difference;
		    
			logger.info(" GetOperationalRoleList Mandatory fields check");
			logger.info("Elapsed seconds: " + differenceInSec);
			
			
			try {
				
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				
				if(response.getStatus()==500)
				{
					
					Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
					
				}
				else
				{
					Assert.fail();
				}
				logger.info("Output from Server .... "+jsonOutput.toString());
					
			
			} catch (Exception e) {			
				e.printStackTrace();
			}
		}
		
		//(-) The below test case to check business validations of GetOperationalRoleList service...
		@Test
		public void getOperationalRoleListBusinessValidations() throws JSONException
		{
			
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getOperationalRoleList");
			JSONObject getOperationalRoleListJson = new JSONObject();
			getOperationalRoleListJson.put("token", token);
			getOperationalRoleListJson.put("userLoginName", adminUserLoginName);
			getOperationalRoleListJson.put("organisationId","asd");
			
			long lStartTime = System.currentTimeMillis();
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getOperationalRoleListJson.toString());		
			
			long lEndTime = System.currentTimeMillis();
			 
			long difference = lEndTime - lStartTime;
			
			long differenceInSec = difference;
		    
			logger.info(" GetOperationalRoleList Business Validations check");
			logger.info("Elapsed seconds: " + differenceInSec);
			
			
			try {
				
				JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
				
				if(response.getStatus()==500)
				{
					
					Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));
					
				}
				else
				{
					Assert.fail();
				}
				logger.info("Output from Server .... "+jsonOutput.toString());
					
			
			} catch (Exception e) {			
				e.printStackTrace();
			}
		}
		
		//(+) The below test case is for success scenario of GetOperationalRoleList service.
		@Test
		public void getOperationalRoleListSuccess() throws JSONException
		{
			
			WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getOperationalRoleList");
			JSONObject getOperationalRoleListJson = new JSONObject();
			getOperationalRoleListJson.put("token", token);
			getOperationalRoleListJson.put("userLoginName", adminUserLoginName);
			getOperationalRoleListJson.put("organisationId",organisationId);
			
			
			long lStartTime = System.currentTimeMillis();
			
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getOperationalRoleListJson.toString());		
			
			long lEndTime = System.currentTimeMillis();
			 
			long difference = lEndTime - lStartTime;
			
			long differenceInSec = difference;
		 
			logger.info(" GetOperationalRoleList success scenario");
			logger.info("Elapsed seconds: " + differenceInSec);
			logger.info("Output from Server .... ");
			
			try {
				
				String jsonOutput = response.getEntity(String.class) ;
				logger.info("Output from Server .... "+jsonOutput.toString());
				Assert.assertEquals(200, response.getStatus());
					
			
			} catch (Exception e) {			
				e.printStackTrace();
			}
		}
		
		/*
		 * GetOperationalRoleList JUnit Test cases ends here...
		 
		
		
		 * GetSpeciality JUnit Test cases starts here...
		 
		
		//(-) The below test case is checking mandatory fields of GetSpeciality service...
				@Test
				public void getSpecialtyMandatoryField() throws JSONException
				{
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getSpecialty");
					JSONObject getSpecialtyJson = new JSONObject();
					getSpecialtyJson.put("token", token);
					getSpecialtyJson.put("userLoginName", "");
					getSpecialtyJson.put("organisationId",organisationId);
					getSpecialtyJson.put("isClinicAdminUser","");
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getSpecialtyJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				    
					logger.info(" GetSpecialty Mandatory fields check");
					logger.info("Elapsed seconds: " + differenceInSec);
					
					
					try {
						
						JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
						
						if(response.getStatus()==500)
						{
							
							Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
							
						}
						else
						{
							Assert.fail();
						}
						logger.info("Output from Server .... "+jsonOutput.toString());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}
				
				//(-) The below test case to check business validations of GetSpeciality service...
				@Test
				public void getSpecialityBusinessValidations() throws JSONException
				{
					
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getSpecialty");
					JSONObject getSpecialtyJson = new JSONObject();
					getSpecialtyJson.put("token", token);
					getSpecialtyJson.put("userLoginName", adminUserLoginName);
					getSpecialtyJson.put("organisationId","as");
					getSpecialtyJson.put("isClinicAdminUser","Y");
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getSpecialtyJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				    
					logger.info(" GetSpeciality Business Validations check");
					logger.info("Elapsed seconds: " + differenceInSec);
					
					
					try {
						
						JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
						
						if(response.getStatus()==500)
						{
							
							Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));
							
						}
						else
						{
							Assert.fail();
						}
						logger.info("Output from Server .... "+jsonOutput.toString());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}
				
				//(+) The below test case is for success scenario of GetSpeciality service.
				@Test
				public void getSpecialtySuccess() throws JSONException
				{
					
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/getSpecialty");
					JSONObject getSpecialtyJson = new JSONObject();
					getSpecialtyJson.put("token", token);
					getSpecialtyJson.put("userLoginName", adminUserLoginName);
					getSpecialtyJson.put("organisationId",organisationId);
					getSpecialtyJson.put("isClinicAdminUser","Y");
					
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,getSpecialtyJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				 
					logger.info(" GetSpeciality success scenario");
					logger.info("Elapsed seconds: " + differenceInSec);
					logger.info("Output from Server .... ");
					
					try {
						
						String jsonOutput = response.getEntity(String.class) ;
						logger.info("Output from Server .... "+jsonOutput.toString());
						Assert.assertEquals(200, response.getStatus());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}
				
				/*
				 * GetSpeciality JUnit Test cases ends here...
				 
				
				
				 * searchUserGlobal JUnit Test cases starts here...
				 
				
				//(-) The below test case is checking mandatory fields of searchUserGlobal service...
				@Test
				public void searchUserGlobalMandatoryField() throws JSONException
				{
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/searchUserGlobal");
					JSONObject searchUserGlobalJson = new JSONObject();
					searchUserGlobalJson.put("token", token);
					searchUserGlobalJson.put("userLoginName", "");
					searchUserGlobalJson.put("organisationId",organisationId);
					searchUserGlobalJson.put("userType", "");
					searchUserGlobalJson.put("userName", "");
					searchUserGlobalJson.put("roleId", "");
					searchUserGlobalJson.put("phoneNumber", "");
					searchUserGlobalJson.put("speciality", "");
					searchUserGlobalJson.put("isClinicAdminUser", "");
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserGlobalJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				    
					logger.info(" searchUserGlobal Mandatory fields check");
					logger.info("Elapsed seconds: " + differenceInSec);
					
					
					try {
						
						JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
						
						if(response.getStatus()==500)
						{
							
							Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9102, Integer.parseInt(jsonOutput.get("errorCode").toString()));
							
						}
						else
						{
							Assert.fail();
						}
						logger.info("Output from Server .... "+jsonOutput.toString());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}
				
				//(-) The below test case to check business validations of searchUserGlobal service...
				@Test
				public void searchUserGlobalBusinessValidations() throws JSONException
				{
					
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/searchUserGlobal");
					JSONObject searchUserGlobalJson = new JSONObject();
					searchUserGlobalJson.put("token", token);
					searchUserGlobalJson.put("userLoginName", adminUserLoginName);
					searchUserGlobalJson.put("organisationId",organisationId);
					searchUserGlobalJson.put("userType", "Doctor");
					searchUserGlobalJson.put("userName", "Ravindanath Poganulu");
					searchUserGlobalJson.put("roleId", "1a");
					searchUserGlobalJson.put("phoneNumber", "96534289887a");
					searchUserGlobalJson.put("speciality", "Cardiology");
					searchUserGlobalJson.put("isClinicAdminUser", "N");
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserGlobalJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				    
					logger.info(" SearchUserGlobalJson Business Validations check");
					logger.info("Elapsed seconds: " + differenceInSec);
					
					
					try {
						
						JSONObject jsonOutput = response.getEntity(JSONObject.class) ;
						
						if(response.getStatus()==500)
						{
							
							Assert.assertEquals(jsonOutput.get("errorMessage").toString(),9104, Integer.parseInt(jsonOutput.get("errorCode").toString()));
							
						}
						else
						{
							Assert.fail();
						}
						logger.info("Output from Server .... "+jsonOutput.toString());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}
				
				//(+) The below test case is for success scenario of searchUserGlobal service.
				@Test
				public void searchUserGlobalSuccess() throws JSONException
				{
					
					WebResource webResource = client.resource("http://"+ipAddress+":8080/HPPClinicsCARestApp/rest/clinicAdministration/searchUserGlobal");
					JSONObject searchUserGlobalJson = new JSONObject();
					searchUserGlobalJson.put("token", token);
					searchUserGlobalJson.put("userLoginName", adminUserLoginName);
					searchUserGlobalJson.put("organisationId",organisationId);
					searchUserGlobalJson.put("userType", "Doctor");
					searchUserGlobalJson.put("userName", "Ravi");
					searchUserGlobalJson.put("roleId", "1");
					searchUserGlobalJson.put("phoneNumber", "96534289887");
					searchUserGlobalJson.put("speciality", "Cardiology");
					searchUserGlobalJson.put("isClinicAdminUser", "N");
					
					
					long lStartTime = System.currentTimeMillis();
					
					ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,searchUserGlobalJson.toString());		
					
					long lEndTime = System.currentTimeMillis();
					 
					long difference = lEndTime - lStartTime;
					
					long differenceInSec = difference;
				 
					logger.info(" SearchUserGlobal success scenario");
					logger.info("Elapsed seconds: " + differenceInSec);
					logger.info("Output from Server .... ");
					
					try {
						
						String jsonOutput = response.getEntity(String.class) ;
						logger.info("Output from Server .... "+jsonOutput.toString());
						Assert.assertEquals(200, response.getStatus());
							
					
					} catch (Exception e) {			
						e.printStackTrace();
					}
				}

	 
}
*/