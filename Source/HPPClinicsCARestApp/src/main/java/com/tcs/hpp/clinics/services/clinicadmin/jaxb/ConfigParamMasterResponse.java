//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.30 at 11:28:10 AM IST 
//


package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConfigParamMasterResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="ConfigParamMasterResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="OrganisationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="SystemParam" maxOccurs="unbounded" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="ParamId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ParamName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         			   &lt;element name="HelpText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Datatype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
      "systemParam"
})
@XmlRootElement(name = "ConfigParamMasterResponse")
public class ConfigParamMasterResponse {

   
    @XmlElement(name = "SystemParam", required = true)
    private List<SystemParam> systemParam;


    /**
     * Gets the value of the systemParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the systemParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSystemParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SystemParam }
     * 
     * 
     */
    public List<SystemParam> getSystemParam() {
        if (systemParam == null) {
            systemParam = new ArrayList<SystemParam>();
        }
        return this.systemParam;
    }
    
    
    public void setSystemParam(List<SystemParam> systemParam) {
    	this.systemParam = systemParam;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ParamId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ParamName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HelpText" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Datatype" type="{http://www.w3.org/2001/XMLSchema}string"/>     *         
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paramId",
        "paramName",
        "helpText",
        "dataType"
    })
    public static class SystemParam {

        @XmlElement(name = "ParamId", required = true)
        private String paramId;
        @XmlElement(name = "ParamName", required = true)
        private String paramName;
        @XmlElement(name = "HelpText", required = true)
        private String helpText;
        @XmlElement(name = "Datatype", required = true)
        private String dataType;

        /**
         * Gets the value of the paramId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParamId() {
            return paramId;
        }

        /**
         * Sets the value of the paramId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParamId(String value) {
            this.paramId = value;
        }

        /**
         * Gets the value of the paramName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParamName() {
            return paramName;
        }

        /**
         * Sets the value of the paramName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParamName(String value) {
            this.paramName = value;
        }

		/**
		 * Gets the value of the dataType property.
		 * @return the dataType
		 */
		public String getDatatype() {
			return dataType;
		}

		/**
		 * Sets the value of the dataType property.
		 * @param dataType the dataType to set
		 */
		public void setDatatype(String dataType) {
			this.dataType = dataType;
		}
        
		/**
		 * Gets the value of the helpText property.
		 * @return the helpText
		 */
		public String getHelpText() {
			return helpText;
		}

		/**
		 * Sets the value of the helpText property.
		 * @param helpText the helpText to set
		 */
		public void setHelpText(String helpText) {
			this.helpText = helpText;
		}

    }

}
