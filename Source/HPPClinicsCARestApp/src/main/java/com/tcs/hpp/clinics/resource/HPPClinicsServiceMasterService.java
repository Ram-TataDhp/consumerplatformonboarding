/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/*import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;*/






import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.ServiceClassificationDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.ServiceDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.Service;
import com.tcs.hpp.clinics.clinicadmin.persistence.ServiceClassification;
import com.tcs.hpp.clinics.clinicadmin.persistence.Tariff;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.convertor.HPPClinicAdminJAXBConvertor;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateServiceResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetServiceResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchServiceResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceCategoryMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceCategoryMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceClassificationMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceClassificationMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateServiceResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisher;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * This class is used to handle service master services... 
 *
 */

@RestController
@RequestMapping("/clinicAdministration")
public class HPPClinicsServiceMasterService {
	public static final String CLASSNAME = "HPPClinicsServiceMasterService";
	
	
	private static Logger logger=Logger.getLogger(HPPClinicsServiceMasterService.class);
	public HPPClinicsServiceMasterService()
	{
		 String trackId=null;
		 UUID idOne = UUID.randomUUID();
		 trackId=idOne.toString();
		 MDC.put(HPPClinicAdminConstants.TRACKING_ID_KEY, trackId);
	}
	
	/**
	 * The below method is rest service for ServiceCategory...
	 * @param serviceCategoryMasterRequest
	 * @return serviceCategoryMasterResponse
	 */
	@RequestMapping(value = "1.1/serviceCategory", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity serviceCategory(@RequestBody ServiceCategoryMasterRequest serviceCategoryMasterRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_SERVICE);
		 BusinessDelegate delegate = null;
		 ServiceCategoryMasterResponse serviceCategoryMasterResponse =  null;
		 ArrayList<Serializable> isValidTokenList = null;
		 List<Serializable> serviceCategoryList=null;
 		 boolean isValidToken = false;
 		 Map<String,List<String>> categoryMap = null;

 		 List<String> errorMsgList = null;
 		 List<String> formatMsgList = null;
 	
 		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
 		try
		{
 		 serviceTimeStart = System.currentTimeMillis();
 		 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
		 monitoringEvent =new  MonitoringEvent();
		 errorMsgList = HPPClinicsServiceMasterServiceHelper.serviceCategoryFieldValidations(serviceCategoryMasterRequest);
			
		 if(errorMsgList.size()==0)
			 //Mandatory fields valid check...
		 {
			formatMsgList = HPPClinicsServiceMasterServiceHelper.serviceCategoryBusinessValidations(serviceCategoryMasterRequest);
			if(formatMsgList.size()==0)
			{
			/*
			 * Validating the token...
			 */
			    BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(serviceCategoryMasterRequest.getUserLoginName(),serviceCategoryMasterRequest.getToken(),Long.parseLong(serviceCategoryMasterRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						BusinessComponentMap bcMap = populateBCMForServiceCategory(serviceCategoryMasterRequest.getOrganisationId());
					
						businessProcessStartTime = System.currentTimeMillis();
						serviceCategoryList = (ArrayList<Serializable>) delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						serviceCategoryMasterResponse=new ServiceCategoryMasterResponse();
						if(serviceCategoryList!=null && serviceCategoryList.size()>0)
						{
							
							categoryMap=(Map<String, List<String>>) serviceCategoryList.get(0);
							
							for (Entry<String, List<String>> entry : categoryMap.entrySet()) 
							 {
								String key=entry.getKey();
						            if(key.equals(HPPClinicAdminConstants.CATEGORY))
						            {
						            	serviceCategoryMasterResponse.setCategory(entry.getValue());
						            }
						           
						           if(key.equals(HPPClinicAdminConstants.SERVICE_TYPE))
						            {
						            	   serviceCategoryMasterResponse.setServiceType(entry.getValue());
						            }
									
						        }
						}
						 
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
			 
				}
			  }
			  else
				  //Fail in Business Validations
		       {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			   }
		   }
		   else
			   //Fail in mandatory fields check...
	       {
			 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
		   }
		   
		    serviceTimeEnd = System.currentTimeMillis();
			if(serviceCategoryMasterRequest.getOrganisationId()!=null && serviceCategoryMasterRequest.getOrganisationId()!="" && !serviceCategoryMasterRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(serviceCategoryMasterRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SERVICE_CATEGORY_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		   
		 }
		 catch(HPPApplicationException e)
		 {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(-HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
		
 		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_SERVICE);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(serviceCategoryMasterResponse)).build();
 		return new ResponseEntity(serviceCategoryMasterResponse,HttpStatus.OK);
	}	
	
	/**
	 * The below method is used to prepare the BCM map for Service Category Master service...
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForServiceCategory(String orgId) {
		
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_SERVICE_CATEGORY);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getServiceCategory",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_SERVICE_CATEGORY);
		return bcMap;
	}
	
	
	/**
	 * This method is used to get all the service classifications
	 * @param serviceClassificationMasterRequest
	 * @return serviceClassificationMasterResponse
	 */
	@RequestMapping(value = "1.1/serviceClassification", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity serviceClassification(@RequestBody ServiceClassificationMasterRequest serviceClassificationMasterRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_SERVICE);
		BusinessDelegate delegate = null;
		 ServiceClassificationMasterResponse serviceClassificationMasterResponse =  null;
		 ServiceClassificationMasterResponse.Classification classification = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 List<Serializable> serviceClassificationList=null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
			{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsServiceMasterServiceHelper.serviceClassificationFieldValidations(serviceClassificationMasterRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList=HPPClinicsServiceMasterServiceHelper.serviceClassificationBusinessValidations(serviceClassificationMasterRequest);
			  if(formatMsgList.size()==0)
			  {
				/*
				 * Validating the token...
				 */
			    BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(serviceClassificationMasterRequest.getUserLoginName(),serviceClassificationMasterRequest.getToken(),Long.parseLong(serviceClassificationMasterRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						BusinessComponentMap bcMap = populateBCMForServiceClassification(serviceClassificationMasterRequest.getOrganisationId(),serviceClassificationMasterRequest.getCategory());
						businessProcessStartTime = System.currentTimeMillis();
						serviceClassificationList = delegate.execute(bcMap);
						 businessProcessEndTime = System.currentTimeMillis();
                         totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
				        serviceClassificationMasterResponse = new ServiceClassificationMasterResponse();
						if(serviceClassificationList!=null && serviceClassificationList.size()>0)
						{
							List<ServiceClassificationDTO> classificationDTOs = (List<ServiceClassificationDTO>)serviceClassificationList.get(0);
							List<ServiceClassificationMasterResponse.Classification> classifications = new ArrayList< ServiceClassificationMasterResponse.Classification>();
							for(ServiceClassificationDTO serviceClassificationDTO : classificationDTOs)
							{
								classification = new ServiceClassificationMasterResponse.Classification();
								classification.setClassificationId(serviceClassificationDTO.getClassificationId());
								classification.setClassificationName(serviceClassificationDTO.getClassificationName());
								
								classifications.add(classification);
							}
							

							serviceClassificationMasterResponse.setClassification(classifications);

						}
						
					}
					else
						//Invalid token
					{
						
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
				}
			  }
			  else
				  //Fail in Business validations
			     {
				  	//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
				
			 }
			 else
				 //Fail in mandatory fields check...
		     {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
			 serviceTimeEnd = System.currentTimeMillis();
				if(serviceClassificationMasterRequest.getOrganisationId()!=null && serviceClassificationMasterRequest.getOrganisationId()!="" && !serviceClassificationMasterRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(serviceClassificationMasterRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SERVICE_CLASSIFICATION_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		 }
		 catch(HPPApplicationException e)
		 {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
		
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_SERVICE);
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(serviceClassificationMasterResponse)).build();
		 return new ResponseEntity(serviceClassificationMasterResponse,HttpStatus.OK);
	}
	
	
	
	/**
	 * This method is used to populate the businessComponentMap for serviceClassification
	 * @param orgId
	 * @param category
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForServiceClassification(String orgId,String category) {
		
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_SERVICE_CLASSIFICATION);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(category);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getServiceClassification",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_SERVICE_CLASSIFICATION);
		return bcMap;
	}

	
	/**
	 * This method is used to create the service
	 * @param createServiceRequest
	 * @return createServiceResponse
	 */
	@RequestMapping(value = "1.1/createService", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity createService(@RequestBody CreateServiceRequest createServiceRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.CREATE_SERVICE_SERVICE);
		 BusinessDelegate delegate = null;
		 CreateServiceResponse createServiceResponse =  null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<Serializable> createServiceList = null;
		 List<String> creatServiceList=null;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
			{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsServiceMasterServiceHelper.createServiceFieldValidations(createServiceRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
				formatMsgList=HPPClinicsServiceMasterServiceHelper.createServiceBusinessValidations(createServiceRequest);
				if(formatMsgList.size()==0)
				{	
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(createServiceRequest.getUserLoginName(),createServiceRequest.getToken(),Long.parseLong(createServiceRequest.getService().getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();

                businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
						Service service = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoServiceEntity(createServiceRequest);
						String locationId =createServiceRequest.getService().getLocationId();
						String siteId = createServiceRequest.getService().getSiteId();						
						BusinessComponentMap bcMap = populateBCMForCreateService(service,createServiceRequest.getService().getOrganisationId(),createServiceRequest.getService().getTariff(), locationId, siteId);						

                        businessProcessStartTime = System.currentTimeMillis();
						createServiceList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						createServiceResponse = new CreateServiceResponse();
						if(createServiceList!=null && createServiceList.size()>0)
						{
							
							creatServiceList =(List<String>) createServiceList.get(0); 						
							createServiceResponse.setServiceId(creatServiceList.get(0));
							createServiceResponse.setServiceName(creatServiceList.get(1));
							createServiceResponse.setTariffId(creatServiceList.get(2));
							createServiceResponse.setSuccessMessage(HPPClinicAdminConstants.CREATE_SERVICE_SUCCESS_MSG.replace("<Service Name>", createServiceResponse.getServiceName()));
						}
						
						
					  
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}

				}
				}
				else
				  //Fail in Business validations
			     {
					if(formatMsgList.toString().contains(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD))
	  				 {	 
	  					 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"),HttpStatus.INTERNAL_SERVER_ERROR);
	  				 } 				
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
			 }
			 else
				 //Fail in mandatory fields check...
		     {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
			 serviceTimeEnd = System.currentTimeMillis();
				if(createServiceRequest.getService().getOrganisationId()!=null && createServiceRequest.getService().getOrganisationId()!="" && !createServiceRequest.getService().getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(createServiceRequest.getService().getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CREATE_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			}
			catch (HPPApplicationException e) 
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				
			}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CREATE_SERVICE_SERVICE);
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(createServiceResponse)).build();
		 return new ResponseEntity(createServiceResponse,HttpStatus.OK);
	}
	
	
 
	/**
	 * This method is used to populate the businessComponentMap for createService.
	 * @param service
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForCreateService(Service service,String orgId,String tariff, String locationId, String siteId) {
		
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_CREATE_SERVICE);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(service);
		paramList.add(tariff);
		paramList.add(locationId);
		paramList.add(siteId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"createService",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_CREATE_SERVICE);
		return bcMap;
	}
	


	/**
	 * This method is used to search the clinicServices by autoSuggest
	 * @param SearchServiceRequest
	 * @return SearchServiceResponse
	 */

	@RequestMapping(value = "1.1/searchClinicService", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity searchClinicService(@RequestBody SearchServiceRequest request)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE);
		BusinessDelegate delegate = null;
		SearchServiceResponse response =  null;
		SearchServiceResponse.Service service = null;
		ArrayList<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<Serializable> serviceList = null;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
		    errorMsgList = HPPClinicsServiceMasterServiceHelper.searchClinicServiceFieldValidations(request);
				
	      if(errorMsgList.size()==0)
	    	  //Mandatory fields valid check...
		  {
	       formatMsgList = HPPClinicsServiceMasterServiceHelper.searchClinicServiceBusinessValidations(request);
	       if(formatMsgList.size()==0)
	       {
			/*
			 * Validating the token...
			 */
			BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganisationId()));
			
			delegate = new BusinessDelegateImpl();
            businessProcessStartTime = System.currentTimeMillis();
			isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
			businessProcessEndTime = System.currentTimeMillis();
			totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
			if(isValidTokenList!=null && isValidTokenList.size()>0)
			{
				isValidToken = (Boolean) isValidTokenList.get(0);
				if(isValidToken)
					//Valid token condition...
				{
					String searchKey = request.getSearchText();
					String orgId = request.getOrganisationId();
					BusinessComponentMap bcMap = populateBCMForSearchService(searchKey,orgId);
					businessProcessStartTime = System.currentTimeMillis();
					serviceList = delegate.execute(bcMap);
					businessProcessEndTime = System.currentTimeMillis();
                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
					response = new SearchServiceResponse();
					if(serviceList!=null && serviceList.size()>0)
					{
						List<ServiceDTO> serviceDTOs = (List<ServiceDTO>)serviceList.get(0);
						List<SearchServiceResponse.Service> services = new ArrayList<SearchServiceResponse.Service>();
						for(ServiceDTO serviceDTO : serviceDTOs)
						{
							service = new SearchServiceResponse.Service();
							service.setServiceId(serviceDTO.getServiceId());
							service.setServiceName(serviceDTO.getServiceName());
							if(serviceDTO.getTariffId()==null)
							{
								service.setTariffId("");
							}
							else
							{
								service.setTariffId(serviceDTO.getTariffId());
							}
							if(serviceDTO.getTariff()==null)
							{
								service.setTariff("");
							}
							else
							{
								service.setTariff(serviceDTO.getTariff());
							}
							
							
							services.add(service);
						}
						
						response.setService(services);
					}
				}
				else
					//Invalid token
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
	       }
	       else
	    	   //Fail in Business Validations
			  {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
	    	     return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			  }
		  }
		  else
			  //Fail in mandatory fields check...
		  {
			 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	      serviceTimeEnd = System.currentTimeMillis();
			if(request.getOrganisationId()!=null && request.getOrganisationId()!="" && !request.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(request.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
	   
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(response)).build();
		return new ResponseEntity(response,HttpStatus.OK);
	}
	
	
	/**
	 * This method is used to retrieve the clinicservices.
	 * @param GetServiceRequest
	 * @return GetServiceResponse
	 */

	@RequestMapping(value = "1.1/retrieveClinicServices", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity retrieveClinicServices(@RequestBody GetServiceRequest request)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE);
		BusinessDelegate delegate = null;
		GetServiceResponse response =  null;
		GetServiceResponse.Service service = null;
		ArrayList<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<Serializable> serviceList = null;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		String count = null;
		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
		 errorMsgList = HPPClinicsServiceMasterServiceHelper.retrieveClinicServiceFieldValidations(request);
	
		   if(errorMsgList.size()==0)
			   //Mandatory fields valid check...
		   {
		    formatMsgList = HPPClinicsServiceMasterServiceHelper.retrieveClinicServiceBusinessValidations(request);	
		    if(formatMsgList.size()==0)
			{
			/*
			 * Validating the token...
			 */
			BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganisationId()));
			
			delegate = new BusinessDelegateImpl();
			businessProcessStartTime = System.currentTimeMillis();
			isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
			businessProcessEndTime = System.currentTimeMillis();
			totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
			if(isValidTokenList!=null && isValidTokenList.size()>0)
			{
				isValidToken = (Boolean) isValidTokenList.get(0);
				if(isValidToken)
					//Valid token condition...
				{
					String serviceId = request.getServiceId();
					String searchKey = request.getSearchText();
					String orgId = request.getOrganisationId();
					String startRow = request.getStartRow();
					String interval = request.getInterval();
					String locationId = request.getLocationId();
					BusinessComponentMap bcMap = populateBCMForRetrieveClinicService(serviceId,locationId, searchKey,orgId,startRow,interval);
					businessProcessStartTime = System.currentTimeMillis();
					serviceList = delegate.execute(bcMap);
					businessProcessEndTime = System.currentTimeMillis();
                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
					response = new GetServiceResponse();
					if(serviceList!=null && serviceList.size()>0)
					{
						List searchServiceCountList = (List)serviceList.get(0);
						if(searchServiceCountList.size()>0)
						{
							count = searchServiceCountList.get(0).toString();
							List<ServiceDTO> serviceDTOs = (List<ServiceDTO>)searchServiceCountList.get(1);
							List<GetServiceResponse.Service> services = new ArrayList<GetServiceResponse.Service>();
							for(ServiceDTO serviceDTO : serviceDTOs)
							{
								service = new GetServiceResponse.Service();
								service.setServiceId(serviceDTO.getServiceId());
								service.setServiceName(serviceDTO.getServiceName());
								service.setChargable(serviceDTO.getChargable());
								service.setClassificationId(serviceDTO.getClassificationId());
								service.setClassificationName(serviceDTO.getClassificationName());
								service.setResultApplicable(serviceDTO.getResultApplicable());
								service.setServiceCategory(serviceDTO.getServiceCategory());
								service.setServiceType(serviceDTO.getServiceType());
								service.setStatus(serviceDTO.getStatus());
								service.setOrganisationId(serviceDTO.getOrganisationId());
								service.setTariff(serviceDTO.getTariff());
								service.setTariffId(serviceDTO.getTariffId());
								
								services.add(service);
							}
							
							response.setService(services);
							response.setCount(count);
						}
						
					}
				}
				else
					//Invalid token
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			 }
					
			}
		       else
		    	   //Fail in Business Validations
				  {
					 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
		    	     return new  ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				  }
		    }
			else
				//Fail in mandatory fields check...
			{
			  //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		    serviceTimeEnd = System.currentTimeMillis();
			if(request.getOrganisationId()!=null && request.getOrganisationId()!="" && !request.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(request.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICES_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(response)).build();
		return new ResponseEntity(response,HttpStatus.OK);
	}

	/**
	 * This method is used to update the service.
	 * @param updateServiceRequest
	 * @return updateServiceResponse
	 */
	@RequestMapping(value = "1.1/updateService", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity updateService(@RequestBody UpdateServiceRequest updateServiceRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_SERVICE);
		 BusinessDelegate delegate = null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 UpdateServiceResponse updateServiceResponse=null;
		 
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
		 {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsServiceMasterServiceHelper.updateServiceFieldValidations(updateServiceRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList=HPPClinicsServiceMasterServiceHelper.updateServiceBusinessValidations(updateServiceRequest);
		      if(formatMsgList.size()==0)
			  {	
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(updateServiceRequest.getUserLoginName(),updateServiceRequest.getToken(),Long.parseLong(updateServiceRequest.getService().getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
			
			    if(isValidTokenList!=null && isValidTokenList.size()>0)
			    {
				  isValidToken = (Boolean) isValidTokenList.get(0);
				  if(isValidToken)
					  //Valid token condition...
				  {
					oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
					Service service = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUpdateServiceEntity(updateServiceRequest);
					Tariff tariff = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUpdateOrCreateTariffEntity(updateServiceRequest);
					BusinessComponentMap bcMap = populateBCMForUpdateService(service,tariff,updateServiceRequest.getService().getOrganisationId());
					businessProcessStartTime = System.currentTimeMillis();
					List<Serializable> updateServiceList = delegate.execute(bcMap);
					updateServiceResponse=new UpdateServiceResponse();
					if(updateServiceList!=null && updateServiceList.size()>0)
					{
						String tariffId = (String)updateServiceList.get(0);
						updateServiceResponse.setTariffId(tariffId);
						
					}
					businessProcessEndTime = System.currentTimeMillis();
                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
					
					updateServiceResponse.setSuccessMessage(HPPClinicAdminConstants.UPDATE_SERVICE_SUCCESS_MSG.replace("<Service Name>", updateServiceRequest.getService().getServiceName()));
				}
				else
					//Invalid token
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			    }
			 }
			    else
					  //Fail in Business validations
				     {
					 	//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
			    	   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					 } 
		   }
		   else
			   //Fail in mandatory fields check...
		   {
			 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
		   }
			 serviceTimeEnd = System.currentTimeMillis();
				if(updateServiceRequest.getService().getOrganisationId()!=null && updateServiceRequest.getService().getOrganisationId()!="" && !updateServiceRequest.getService().getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(updateServiceRequest.getService().getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.UPDATE_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_SERVICE); 		
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(updateServiceResponse)).build();
		 return new ResponseEntity(updateServiceResponse,HttpStatus.OK);
	

	}
	
	/**
	 * This method is used to populate the businessComponentMap for updateService
	 * @param service
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForUpdateService(Service service,Tariff tariff,String orgId) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.BCM_UPDATE_SERVICE); 	
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(service);
		paramList.add(tariff);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"updateService",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.BCM_UPDATE_SERVICE); 
		return bcMap;
	}
	/**
	 * This is the service to get Service Master Data...
	 * @param serviceMasterRequest
	 * @return serviceMasterResponse
	 */

	@RequestMapping(value = "1.1/serviceMaster", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getServiceMaster(@RequestBody ServiceMasterRequest serviceMasterRequest)
	{
			 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_SERVICE_MASTER);
			 BusinessDelegate delegate = new BusinessDelegateImpl();
			 ServiceMasterResponse serviceMasterResponse =  null;
			
			 ArrayList<Serializable> isValidTokenList = null;
			boolean isValidToken = false;
	 		
	 		 List<Serializable> serviceMasterList=null;
	 		List<String> errorMsgList = null;
	 		List<String> formatMsgList = null;
	 		List<ServiceClassification> serviceClassificationList = null;
	 		long serviceTimeStart = 0L;
			 long serviceTimeEnd = 0L;
			 long businessProcessStartTime=0L;
			 long businessProcessEndTime=0L;
			 long totalBusinessProcessingTime = 0L;
			 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
			 MonitoringEvent monitoringEvent = null;
	 		try
			{
	 			serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsServiceMasterServiceHelper.serviceMasterFieldValidations(serviceMasterRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList = HPPClinicsServiceMasterServiceHelper.serviceMasterBusinessValidations(serviceMasterRequest);
			  if(formatMsgList.size()==0)
			  {		
				/*
				 * Validating the token...
				 */
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(serviceMasterRequest.getUserLoginName(),serviceMasterRequest.getToken(),Long.parseLong(serviceMasterRequest.getOrganisationId()));
				 businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							
							
							BusinessComponentMap bcMap = populateBCMForServiceMaster(serviceMasterRequest.getOrganisationId());
						
							businessProcessStartTime = System.currentTimeMillis();
							serviceMasterList = (ArrayList<Serializable>) delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							
							if(serviceMasterList!=null && serviceMasterList.size()>0)
							{
								serviceMasterResponse = new ServiceMasterResponse();
								serviceClassificationList = (List<ServiceClassification>)serviceMasterList.get(0);
								 if(serviceClassificationList!=null && serviceClassificationList.size()>0)
								 {
									 for(ServiceClassification serviceClassification : serviceClassificationList)
									 {
										 ServiceMasterResponse.Classification jaxbClassification = new ServiceMasterResponse.Classification();
										 jaxbClassification.setCategory(serviceClassification.getCategory());
										 jaxbClassification.setClassificationId(String.valueOf(serviceClassification.getServiceClassificationId()));
										 jaxbClassification.setClassificationName(serviceClassification.getServiceClassificationName());
										 serviceMasterResponse.getClassification().add(jaxbClassification);
									 }
								 }
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
				 
					}
			  }
				else
					//Fail in Business validations...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			   }
			   else
				   //Fail in mandatory fields check...
		       {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			   }
			 serviceTimeEnd = System.currentTimeMillis();
				if(serviceMasterRequest.getOrganisationId()!=null && serviceMasterRequest.getOrganisationId()!="" && !serviceMasterRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(serviceMasterRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SERVICE_MASTER_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			 }
			 catch(HPPApplicationException e)
			 {
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
	 		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_SERVICE_MASTER);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(serviceMasterResponse)).build();
	 		return new ResponseEntity(serviceMasterResponse,HttpStatus.OK);
		}
	
	
	/**
	 * The below method is used to prepare the BCM map for search clinic service..
	 * @param searchKey
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForSearchService(String searchKey,String orgId) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG + HPPClinicAdminConstants.BCM_SEARCH_SERVICE);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(searchKey);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"searchClinicService",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG + HPPClinicAdminConstants.BCM_SEARCH_SERVICE);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the BCM map for retrieve clinic service..
	 * @param serviceId
	 * @param searchKey
	 * @param orgId
	 * @param startRow
	 * @param interval
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForRetrieveClinicService(String serviceId,String locationId, String searchKey, String orgId, String startRow,	String interval) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.BCM_RETRIEVE_SERVICE);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(Long.parseLong(locationId));
		paramList.add(serviceId);
		paramList.add(searchKey);
		paramList.add(startRow);
		paramList.add(interval);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"retrieveClinicService",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.BCM_RETRIEVE_SERVICE);
		return bcMap;
	}
	/**
     * This method is used to populate the businessComponentMap for serviceMaster.
     * @param orgId
     * @return BusinessComponentMap
     */
    private BusinessComponentMap populateBCMForServiceMaster(String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_USER_STATUS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getServiceMaster",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_USER_STATUS);
		return bcMap;
	}
    
/*    private ResponseBuilder setHeaders(ResponseBuilder entity)
	{
		entity.header("Strict-Transport-Security", "max-age=60");
		entity.header("X-Content-Type-Options", "nosniff");
		entity.header("X-Frame-Options", "DENY");
		entity.header("X-WebKit-CSP", "default-src *");
		entity.header("X-XSS-Protection", "1 mode=block");
		return entity;

	}*/
}
