package com.tcs.hpp.clinics.resource;

import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_FIELD_VALIDATION;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.METHOD_ENTRY_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.METHOD_EXIT_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.IS_APP_TYPE_VALID;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.CLINIC_APP_STRING;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.PATIENT_APP_STRING;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.DOCTOR_APP_STRING;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.services.clinicadmin.jaxb.FetchAppUpdateRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;


public final class HPPClinicsAppUpdateServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsAppUpdateServiceHelper.class);
	private HPPClinicsAppUpdateServiceHelper()
	{

	}
	/**
	 * This method is used to perform fetchAppUpdate FieldValidations.
	 * @param fetchAppUpdateRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> fetchAppUpdateFieldValidations(FetchAppUpdateRequest fetchAppUpdateRequest)throws HPPApplicationException
	{
		logger.info(FETCH_APP_UPDATE_FIELD_VALIDATION + METHOD_ENTRY_MSG);

		List<String> errorMsgList = null;
		errorMsgList = new ArrayList<String>();
		String appType = fetchAppUpdateRequest.getAppType();

		if(isEmpty(appType)){
			errorMsgList.add("AppType");
		}
		logger.info(FETCH_APP_UPDATE_FIELD_VALIDATION + METHOD_EXIT_MSG);
		return errorMsgList;
	}
	
	/**
	 * This method checks whether appType is Clinic App or Patient App
	 * @param fetchAppUpdateRequest
	 * @return boolean
	 * @throws HPPApplicationException
	 */
	public static Boolean isAppTypeValid(FetchAppUpdateRequest fetchAppUpdateRequest)throws HPPApplicationException
	{
		logger.info(IS_APP_TYPE_VALID + METHOD_ENTRY_MSG);

		boolean isAppTypeValid = true;
		String appType = fetchAppUpdateRequest.getAppType().trim();
		if(!appType.equalsIgnoreCase(CLINIC_APP_STRING) && !appType.equalsIgnoreCase(PATIENT_APP_STRING)&& !appType.equalsIgnoreCase(DOCTOR_APP_STRING)) {
			isAppTypeValid = false;
		}
		logger.info(IS_APP_TYPE_VALID + METHOD_EXIT_MSG);
		return isAppTypeValid;
	}

	/**
	 * This method returns true if inputString parameter is null or empty, else returns false
	 * @param inputString
	 * @return
	 */
	private static boolean isEmpty(String inputString)
	{
		return (inputString == null || (inputString != null && inputString.trim().equalsIgnoreCase(""))) ;
	}

}
