/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceCategoryMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceClassificationMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ServiceMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateServiceRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331(Nagesh Chintala)
 * This class used to handle the Service Master Module field and Business validations...
 */
public final class HPPClinicsServiceMasterServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsServiceMasterServiceHelper.class);
	private HPPClinicsServiceMasterServiceHelper()
	{
		
	}
	/**
	 * This method is used to perform serviceCategory FieldValidations.
	 * @param serviceCategoryMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceCategoryFieldValidations(ServiceCategoryMasterRequest serviceCategoryMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(serviceCategoryMasterRequest.getToken()==null || "".equals(serviceCategoryMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(serviceCategoryMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(serviceCategoryMasterRequest.getUserLoginName()==null || "".equals(serviceCategoryMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(serviceCategoryMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(serviceCategoryMasterRequest.getOrganisationId()==null || "".equals(serviceCategoryMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(serviceCategoryMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   }
	   catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform serviceCategory BusinessValidations.
	 * @param serviceCategoryMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceCategoryBusinessValidations(ServiceCategoryMasterRequest serviceCategoryMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
		   Long.parseLong(serviceCategoryMasterRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	     catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SERVICE_CATEGORY_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SERVICE_CATEGORY_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform serviceMaster FieldValidations.
	 * @param serviceMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceMasterFieldValidations(ServiceMasterRequest serviceMasterRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		if(serviceMasterRequest.getToken()==null || "".equals(serviceMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(serviceMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(serviceMasterRequest.getUserLoginName()==null || "".equals(serviceMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(serviceMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(serviceMasterRequest.getOrganisationId()==null || "".equals(serviceMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(serviceMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform serviceMaster BusinessValidations.
	 * @param serviceMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceMasterBusinessValidations(ServiceMasterRequest serviceMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_MASTER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
			Long.parseLong(serviceMasterRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SERVICE_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SERVICE_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform serviceClassification FieldValidations. 
	 * @param serviceClassificationMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceClassificationFieldValidations(ServiceClassificationMasterRequest serviceClassificationMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_FIELD_VALIDATION);
		
       List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(serviceClassificationMasterRequest.getToken()==null || "".equals(serviceClassificationMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(serviceClassificationMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}	
		if(serviceClassificationMasterRequest.getUserLoginName()==null || "".equals(serviceClassificationMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(serviceClassificationMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}	
		if(serviceClassificationMasterRequest.getOrganisationId()==null || "".equals(serviceClassificationMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(serviceClassificationMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}	
		if(serviceClassificationMasterRequest.getCategory()==null || "".equals(serviceClassificationMasterRequest.getCategory()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CATEGORY_FIELD);
		}
		else if(serviceClassificationMasterRequest.getCategory().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.CATEGORY_FIELD);
		}
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SERVICE_CLASSIFICATION_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SERVICE_CLASSIFICATION_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform serviceClassification BusinessValidations.
	 * @param serviceClassificationMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> serviceClassificationBusinessValidations(ServiceClassificationMasterRequest serviceClassificationMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	    
		try
		{
			Long.parseLong(serviceClassificationMasterRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}

	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SERVICE_CLASSIFICATION_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SERVICE_CLASSIFICATION_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SERVICE_CLASSIFICATION_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform createService FieldValidations.
	 * @param createServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createServiceFieldValidations(CreateServiceRequest createServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_SERVICE_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(createServiceRequest.getToken()==null || "".equals(createServiceRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(createServiceRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(createServiceRequest.getUserLoginName()==null || "".equals(createServiceRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(createServiceRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(createServiceRequest.getAdminUserId()==null || "".equals(createServiceRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(createServiceRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(createServiceRequest.getService()==null || "".equals(createServiceRequest.getService().toString()) )
		{
			errorMsgList.add(HPPClinicAdminConstants.SERVICE_OBJECT_FIELD);
		}
		else
		{
			if(createServiceRequest.getService().getServiceName()==null || "".equals(createServiceRequest.getService().getServiceName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			else if(createServiceRequest.getService().getServiceName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			if(createServiceRequest.getService().getServiceCategory()==null || "".equals(createServiceRequest.getService().getServiceCategory()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD);
			}
			else if(createServiceRequest.getService().getServiceCategory().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_CATEGORY_FIELD);
			}
			if(createServiceRequest.getService().getServiceType()==null || "".equals(createServiceRequest.getService().getServiceType()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_TYPE_FIELD);
			}
			else if(createServiceRequest.getService().getServiceType().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_TYPE_FIELD);
			}
			if(createServiceRequest.getService().getChargable()==null || "".equals(createServiceRequest.getService().getChargable()))
			{
				errorMsgList.add(HPPClinicAdminConstants.CHARGABLE_FIELD);
			}
			else if(createServiceRequest.getService().getChargable().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.CHARGABLE_FIELD);
			}
			if(createServiceRequest.getService().getResultApplicable()==null || "".equals(createServiceRequest.getService().getResultApplicable()))
			{
				errorMsgList.add(HPPClinicAdminConstants.RESULT_APPLICABLE_FIELD);
			}
			else if(createServiceRequest.getService().getResultApplicable().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.RESULT_APPLICABLE_FIELD);
			}
			if(createServiceRequest.getService().getClassificationId()==null || "".equals(createServiceRequest.getService().getClassificationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.CLASSIFICATION_ID_FIELD);
			}
			else if(createServiceRequest.getService().getClassificationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.CLASSIFICATION_ID_FIELD);
			}
			if(createServiceRequest.getService().getOrganisationId()==null || "".equals(createServiceRequest.getService().getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(createServiceRequest.getService().getOrganisationId().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(createServiceRequest.getService().getStatus()==null || "".equals(createServiceRequest.getService().getStatus()))
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			else if(createServiceRequest.getService().getStatus().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			if(createServiceRequest.getService().getTariff()==null || "".equals(createServiceRequest.getService().getTariff()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			else if(createServiceRequest.getService().getTariff().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			if(createServiceRequest.getService().getLocationId()==null || "".equals(createServiceRequest.getService().getLocationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			else if(createServiceRequest.getService().getLocationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			if(createServiceRequest.getService().getSiteId()==null || "".equals(createServiceRequest.getService().getSiteId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			else if(createServiceRequest.getService().getLocationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			
		}
		if(errorMsgList.size()==0)
		{
			if(!HPPClinicAdminConstants.ACTIVE.equalsIgnoreCase(createServiceRequest.getService().getStatus()) && !HPPClinicAdminConstants.INACTIVE.equalsIgnoreCase(createServiceRequest.getService().getStatus()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_MSG);
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(createServiceRequest.getService().getChargable()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(createServiceRequest.getService().getChargable()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_MSG);
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(createServiceRequest.getService().getResultApplicable()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(createServiceRequest.getService().getResultApplicable()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_MSG);
			}
		}
	   }
	   catch(HPPApplicationException hae)
		{
		   logger.error(hae.getMessage());
		   throw hae;
		}
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CREATE_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		   throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_SERVICE_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform createService BusinessValidations.
	 * @param createServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createServiceBusinessValidations(CreateServiceRequest createServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_SERVICE_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		   
		   try
			{
				Long.parseLong(createServiceRequest.getAdminUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
			}
		   try
			{
				Long.parseLong(createServiceRequest.getService().getClassificationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.CLASSIFICATION_ID_FIELD);
			}
			try
			{
				Long.parseLong(createServiceRequest.getService().getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			try
			{
				new BigDecimal(createServiceRequest.getService().getTariff());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
	   }
	   catch(Exception e)
	   {
		 logger.error(HPPClinicAdminConstants.CREATE_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
	   }
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_SERVICE_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform updateService FieldValidations.
	 * @param updateServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateServiceFieldValidations(UpdateServiceRequest updateServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(updateServiceRequest.getToken()==null || "".equals(updateServiceRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(updateServiceRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}	
		if(updateServiceRequest.getUserLoginName()==null || "".equals(updateServiceRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(updateServiceRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(updateServiceRequest.getAdminUserId()==null || "".equals(updateServiceRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(updateServiceRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(updateServiceRequest.getService()==null || "".equals(updateServiceRequest.toString()))
		{
			errorMsgList.add(HPPClinicAdminConstants.SERVICE_OBJECT_FIELD);
		}
		else
		{
			if(updateServiceRequest.getService().getServiceId()==null || "".equals(updateServiceRequest.getService().getServiceId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			else if(updateServiceRequest.getService().getServiceId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			if(updateServiceRequest.getService().getServiceName()==null || "".equals(updateServiceRequest.getService().getServiceName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			else if(updateServiceRequest.getService().getServiceName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			if(updateServiceRequest.getService().getServiceType()==null || "".equals(updateServiceRequest.getService().getServiceType()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_TYPE_FIELD);
			}
			else if(updateServiceRequest.getService().getServiceType().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_TYPE_FIELD);
			}
			if(updateServiceRequest.getService().getChargable()==null || "".equals(updateServiceRequest.getService().getChargable()))
			{
				errorMsgList.add(HPPClinicAdminConstants.CHARGABLE_FIELD);
			}
			else if(updateServiceRequest.getService().getChargable().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.CHARGABLE_FIELD);
			}
			if(updateServiceRequest.getService().getResultApplicable()==null || "".equals(updateServiceRequest.getService().getResultApplicable()))
			{
				errorMsgList.add(HPPClinicAdminConstants.RESULT_APPLICABLE_FIELD);
			}
			else if(updateServiceRequest.getService().getResultApplicable().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.RESULT_APPLICABLE_FIELD);
			}
			if(updateServiceRequest.getService().getOrganisationId()==null || "".equals(updateServiceRequest.getService().getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(updateServiceRequest.getService().getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(updateServiceRequest.getService().getStatus()==null || "".equals(updateServiceRequest.getService().getStatus()))
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			else if(updateServiceRequest.getService().getStatus().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			if(updateServiceRequest.getService().getTariff()==null || "".equals(updateServiceRequest.getService().getTariff()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			else if(updateServiceRequest.getService().getTariff().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			
		}
		if(errorMsgList.size()==0)
		{
			if(!HPPClinicAdminConstants.ACTIVE.equalsIgnoreCase(updateServiceRequest.getService().getStatus()) && !HPPClinicAdminConstants.INACTIVE.equalsIgnoreCase(updateServiceRequest.getService().getStatus()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_INVALID_MSG);
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(updateServiceRequest.getService().getChargable()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(updateServiceRequest.getService().getChargable()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_CHARGABLE_VALUE_INVALID_MSG);
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(updateServiceRequest.getService().getResultApplicable()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(updateServiceRequest.getService().getResultApplicable()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_RESULTAPPLICABLE_VALUE_INVALID_MSG);
			}
		}
	   }
	   catch(HPPApplicationException hae)
		{
   			logger.error(hae.getMessage());
   			throw hae;
		}
	   catch(Exception e)
		{
		  logger.error(HPPClinicAdminConstants.UPDATE_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform updateService BusinessValidations.
	 * @param updateServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateServiceBusinessValidations(UpdateServiceRequest updateServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		   
		   try
			{
				Long.parseLong(updateServiceRequest.getAdminUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
			}
		   try
			{
				Long.parseLong(updateServiceRequest.getService().getServiceId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			try
			{
				Long.parseLong(updateServiceRequest.getService().getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			try
			{
				if(updateServiceRequest.getService().getTariffId()!=null && !updateServiceRequest.getService().getTariffId().isEmpty())
				{
					Long.parseLong(updateServiceRequest.getService().getTariffId());
				}
				
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_ID_FIELD);
			}
			try
			{
				new BigDecimal(updateServiceRequest.getService().getTariff());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
	   }
	   catch(Exception e)
	   {
		   logger.error(HPPClinicAdminConstants.UPDATE_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		   throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
	   }
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_SERVICE_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform retrieveClinicService FieldValidations.
	 * @param getServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveClinicServiceFieldValidations(GetServiceRequest getServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
	    errorMsgList = new ArrayList<String>();
		if(getServiceRequest.getToken()==null || "".equals(getServiceRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(getServiceRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(getServiceRequest.getUserLoginName()==null || "".equals(getServiceRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(getServiceRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(getServiceRequest.getOrganisationId()==null || "".equals(getServiceRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(getServiceRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(getServiceRequest.getLocationId()==null || "".equals(getServiceRequest.getLocationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
		else if(getServiceRequest.getLocationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform retrieveClinicService BusinessValidations.
	 * @param getServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveClinicServiceBusinessValidations(GetServiceRequest getServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		   
		   try
			{
			   if(getServiceRequest.getServiceId()!=null && !("".equals(getServiceRequest.getServiceId())) && !getServiceRequest.getServiceId().trim().isEmpty())
			   {
				Long.parseLong(getServiceRequest.getServiceId());
			   }
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
		   try
			{
			   if(getServiceRequest.getStartRow()!=null && !("".equals(getServiceRequest.getStartRow())) && !getServiceRequest.getStartRow().trim().isEmpty())
			   {
					Integer.parseInt(getServiceRequest.getStartRow());			   
			   }

			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.STARTROW_FIELD);
			}
		   try
			{
			   if(getServiceRequest.getInterval()!=null && !("".equals(getServiceRequest.getInterval())) && !getServiceRequest.getInterval().trim().isEmpty())
			   {
				   Integer.parseInt(getServiceRequest.getInterval());	   
			   }

			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.INTERVAL_FIELD);
			}
			try
			{
				Long.parseLong(getServiceRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
		
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_CLINIC_SERVICE_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform searchClinicService FieldValidations.
	 * @param searchServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchClinicServiceFieldValidations(SearchServiceRequest searchServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(searchServiceRequest.getToken()==null || "".equals(searchServiceRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(searchServiceRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(searchServiceRequest.getUserLoginName()==null || "".equals(searchServiceRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(searchServiceRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(searchServiceRequest.getSearchText()==null || "".equals(searchServiceRequest.getSearchText()))
		{
			errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
		}
		else if(searchServiceRequest.getSearchText().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
		}
		if(searchServiceRequest.getOrganisationId()==null || "".equals(searchServiceRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(searchServiceRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
	   
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform searchClinicService BusinessValidations.
	 * @param searchServiceRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchClinicServiceBusinessValidations(SearchServiceRequest searchServiceRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		  		Long.parseLong(searchServiceRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
	  catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
	   
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_BUSS_VALIDATION);
		return formatMsgList;
	}
}
