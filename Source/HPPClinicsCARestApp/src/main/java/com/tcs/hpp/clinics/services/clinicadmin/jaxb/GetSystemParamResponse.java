//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.12.18 at 12:06:56 PM IST 
//


package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetSystemParamResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="GetSystemParamResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="SystemParam" maxOccurs="unbounded" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="SystemParamterId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ParamNameId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ParamName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Level" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="LevelValueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="LevelValueName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"count",
    "systemParamList"
})
@XmlRootElement(name = "GetSystemParamResponse")
public class GetSystemParamResponse {

	@XmlElement(name = "Count", required = true)
	private String count;
    @XmlElement(name = "SystemParamList", required = true)
    private List<SystemParam> systemParamList;

    /**
     * Gets the value of the systemParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the systemParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSystemParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SystemParam }
     * 
     * 
     */
    public List<SystemParam> getSystemParamList() {
        if (systemParamList == null) {
            systemParamList = new ArrayList<SystemParam>();
        }
        return this.systemParamList;
    }
    
    /**
     * Sets the value of the systemParam property.
     * @param systemParamList
     */
    public void setSystemParamList(List<SystemParam> systemParamList) {
    	this.systemParamList = systemParamList;
    }


    /**
     * Gets the value of the count property.
     * @return count
     */
    public String getCount() {
		return count;
	}

	/**
	 * Sets the value of the count property.
	 * @param count
	 */
	public void setCount(String count) {
		this.count = count;
	}


	/**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SystemParamterId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ParamNameId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ParamName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Level" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LevelValueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LevelValueName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "systemParamterId",
        "paramId",
        "paramName",
        "paramValue",
        "level",
        "levelValueId",
        "levelValueName",
        "overrideOption"
    })
    public static class SystemParam {

        @XmlElement(name = "SystemParamterId", required = true)
        private String systemParamterId;
        @XmlElement(name = "ParamId", required = true)
        private String paramId;
        @XmlElement(name = "ParamName", required = true)
        private String paramName;
        @XmlElement(name = "ParamValue", required = true)
        private String paramValue;
        @XmlElement(name = "Level", required = true)
        private String level;
        @XmlElement(name = "LevelValueId", required = true)
        private String levelValueId;
        @XmlElement(name = "LevelValueName", required = true)
        private String levelValueName;
        @XmlElement(name = "OverrideOption", required = true)
        private String overrideOption;
        

        /**
         * Gets the value of the systemParamterId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSystemParamterId() {
            return systemParamterId;
        }

        /**
         * Sets the value of the systemParamterId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSystemParamterId(String value) {
            this.systemParamterId = value;
        }

        /**
         * Gets the value of the paramId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParamId() {
            return paramId;
        }

        /**
         * Sets the value of the paramId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParamId(String value) {
            this.paramId = value;
        }

        /**
         * Gets the value of the paramName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParamName() {
            return paramName;
        }

        /**
         * Sets the value of the paramName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParamName(String value) {
            this.paramName = value;
        }

        /**
         * Gets the value of the paramValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getParamValue() {
            return paramValue;
        }

        /**
         * Sets the value of the paramValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setParamValue(String value) {
            this.paramValue = value;
        }

        /**
         * Gets the value of the level property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLevel() {
            return level;
        }

        /**
         * Sets the value of the level property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLevel(String value) {
            this.level = value;
        }

        /**
         * Gets the value of the levelValueId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLevelValueId() {
            return levelValueId;
        }

        /**
         * Sets the value of the levelValueId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLevelValueId(String value) {
            this.levelValueId = value;
        }

        /**
         * Gets the value of the levelValueName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLevelValueName() {
            return levelValueName;
        }

        /**
         * Sets the value of the levelValueName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLevelValueName(String value) {
            this.levelValueName = value;
        }

		/**
		 * Gets the value of the overrideOption property.
		 * @return the overrideOption
		 */
		public String getOverrideOption() {
			return overrideOption;
		}

		/**
		 * Sets the value of the overrideOption property.
		 * @param overrideOption the overrideOption to set
		 */
		public void setOverrideOption(String overrideOption) {
			this.overrideOption = overrideOption;
		}

    }

}
