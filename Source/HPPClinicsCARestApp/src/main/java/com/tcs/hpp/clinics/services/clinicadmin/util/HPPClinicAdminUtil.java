/**
 * 
 */
package com.tcs.hpp.clinics.services.clinicadmin.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ErrorResponse;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 *This is Utility class for rest service functionalities
 */
public final class HPPClinicAdminUtil {
	private static Logger logger = Logger.getLogger(HPPClinicAdminUtil.class);
	private HPPClinicAdminUtil()
	{
		
	}
	
	/**
	  * This method is used to populate the businessComponentMap for IsValidToken
	  * @param userName
	  * @param token
	  * @param orgId
	  * @return BusinessComponentMap
	  */
	 public static BusinessComponentMap populateBCMForIsValidToken(String userName,String token,Long orgId) {
			logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_ISVALIDTOKEN);
			ArrayList<Serializable> paramList = new ArrayList<Serializable>();
			paramList.add(userName);
			paramList.add(token);
			paramList.add(orgId);
			BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICSERVICE_GATEWAY_LOCAL,"isValidToken",paramList);		
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_ISVALIDTOKEN);
			return bcMap;
		} 
	    /**
	     * This method is used to populate the businessComponentMap for RolesMasterData
	     * @param roleIdList
	     * @param orgId
	     * @return BusinessComponentMap
	     */
	    public static BusinessComponentMap populateBCMForRolesMasterData(List<Long> roleIdList,String orgId) {
			logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_ROLESMASTER_DATA);
			ArrayList<Serializable> paramList = new ArrayList<Serializable>();
			paramList.add((Serializable) roleIdList);
			paramList.add(Long.parseLong(orgId));
			BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getRolesData",paramList);		
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_ROLESMASTER_DATA);
			return bcMap;
		}
	    /**
	     * The below method is used  prepare the BCM map for LocationMaster service...
	     * @param orgId
	     * @return BusinessComponentMap
	     */
	    public static BusinessComponentMap populateBCMForLocation(String orgId) {
			
		    logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_FOR_LOCATION);
	    	ArrayList<Serializable> paramList = new ArrayList<Serializable>();
			paramList.add(Long.parseLong(orgId));
			BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getLocation",paramList);		
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_FOR_LOCATION);
			return bcMap;
		}
	    /**
		 * This method is for preparing the error response for all services..
		 * @param errorCode
		 * @param errorMessage
		 * @return HomePageServiceErrorResponse
		 */
		public static ErrorResponse prepareErrorResponse(int errorCode, String errorMessage) {
			logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.PREPARE_ERROR_RESPONSE);
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setErrorCode(errorCode);
			errorResponse.setErrorMessage(errorMessage);
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.PREPARE_ERROR_RESPONSE);
			return errorResponse;
		}
		/**
		 * This method is used to separate the HPPApplicationException object from the Actual error message...
		 * @param HPPApplicationException
		 * @return errorMsg
		 */
		public static String manageExecptionMsg(HPPApplicationException e)
		{
			logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.MANAGE_EXCEPTION_MESSAGE);
			String errorMsg = null;
			
			 try
			 {
				 String oriStr = e.getMessage();
				 if(oriStr.contains(HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE))
					{
				 		String[] msgArray = oriStr.split("\\"+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE);
				 		errorMsg = msgArray[0];
				 	}
				 	else
				 	{
				 		errorMsg = e.getExceptionMessage();
				 	}
			 }
			 catch(Exception ex)
			 {
				 logger.error(ex.getMessage());
				 return ex.getMessage();
			 }
			 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.MANAGE_EXCEPTION_MESSAGE);
			 	return errorMsg;
		}
	
	 
}
