//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.19 at 01:05:22 PM IST 
//


package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetServiceRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="GetServiceRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="adminUserLoginName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="clinicUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="organisationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "adminUserLoginName",
    "clinicUserId",
    "organisationId",
    "consultationTabDetails"
})
@XmlRootElement(name = "UpdateUserConsultationTabDetailsRequest")
public class UpdateUserConsultationTabDetailsRequest {

    @XmlElement(name = "Token", required = true)
    private String token;
    @XmlElement(name = "AdminUserLoginName", required = true)
    private String adminUserLoginName;
    @XmlElement(name = "ClinicUserId", required = true)
    private String clinicUserId;
    @XmlElement(name = "OrganisationId", required = true)
    private String organisationId;
    
    @XmlElement(name = "ConsultationTabDetails", required = true)
    private List<String> consultationTabDetails;
    
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the adminUserLoginName
	 */
	public String getAdminUserLoginName() {
		return adminUserLoginName;
	}
	/**
	 * @param adminUserLoginName the adminUserLoginName to set
	 */
	public void setAdminUserLoginName(String adminUserLoginName) {
		this.adminUserLoginName = adminUserLoginName;
	}
	/**
	 * @return the clinicUserId
	 */
	public String getClinicUserId() {
		return clinicUserId;
	}
	/**
	 * @param clinicUserId the clinicUserId to set
	 */
	public void setClinicUserId(String clinicUserId) {
		this.clinicUserId = clinicUserId;
	}
	/**
	 * @return the organisationId
	 */
	public String getOrganisationId() {
		return organisationId;
	}
	/**
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}
	/**
	 * @return the consultationTabDetails
	 */
	public List<String> getConsultationTabDetails() {
		return consultationTabDetails;
	}
	/**
	 * @param consultationTabDetails the consultationTabDetails to set
	 */
	public void setConsultationTabDetails(List<String> consultationTabDetails) {
		this.consultationTabDetails = consultationTabDetails;
	}

  
	
}
