package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

public class UpdateCommunicationResponse {
	
	String message;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	

}
