package com.tcs.hpp.clinics.services.clinicadmin.jaxb;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetUserConsultationTabDetailsResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="GetUserConsultationTabDetailsResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="ServiceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {   
    "consultationTabDetails",
    "success"
})
@XmlRootElement(name = "GetUserConsultationTabDetailsResponse")

public class GetUserConsultationTabDetailsResponse 
{
	
	  @XmlElement(name="ConsultationTabDetails", required = true)
	  private Map<String,List<String>> consultationTabDetails;
	  
	  @XmlElement(name="Success", required = true)
	  private String success;
	  
	  
	  
	  public Map<String, List<String>> getConsultationTabDetails(){ 
			if(consultationTabDetails==null)
			{
				consultationTabDetails=new HashMap<String, List<String>>();
			}	
			return consultationTabDetails;
	   }



	public String getSuccess() {
		return success;
	}



	public void setSuccess(String success) {
		this.success = success;
	}
	  
	  

	  
	  

}
