package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	 	"token",
	    "clinicUserId",
	    "acceptTermAndCondition",
	    "userLoginName",
	    "ipAddress"
	    
	    
})
@XmlRootElement(name = "TermAndConditionRequest")
public class TermAndConditionRequest {
	

    @XmlElement(name = "Token", required = true)
    private String token;
    @XmlElement(name = "ClinicUserId", required = true)
    private String clinicUserId;
    @XmlElement(name = "AcceptTermAndCondition", required = true)
    private String acceptTermAndCondition;
    @XmlElement(name = "UserLoginName", required = true)
    private String userLoginName;
    @XmlElement(name = "IpAddress", required = true)
    private String ipAddress;
    
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the clinicUserId
	 */
	public String getClinicUserId() {
		return clinicUserId;
	}
	/**
	 * @param clinicUserId the clinicUserId to set
	 */
	public void setClinicUserId(String clinicUserId) {
		this.clinicUserId = clinicUserId;
	}
	/**
	 * @return the acceptTermAndCondition
	 */
	public String getAcceptTermAndCondition() {
		return acceptTermAndCondition;
	}
	/**
	 * @param acceptTermAndCondition the acceptTermAndCondition to set
	 */
	public void setAcceptTermAndCondition(String acceptTermAndCondition) {
		this.acceptTermAndCondition = acceptTermAndCondition;
	}
	/**
	 * @return the userLoginName
	 */
	public String getUserLoginName() {
		return userLoginName;
	}
	/**
	 * @param userLoginName the userLoginName to set
	 */
	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}
	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
    
	      

}
