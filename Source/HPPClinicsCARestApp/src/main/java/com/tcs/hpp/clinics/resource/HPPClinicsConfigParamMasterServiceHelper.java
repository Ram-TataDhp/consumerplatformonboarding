/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSysParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ConfigParamMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSystemParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateSystemParamRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Description: This class is used to handle the Configuration Master Module field and business validations...
 */
public final class HPPClinicsConfigParamMasterServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsConfigParamMasterServiceHelper.class);
	private HPPClinicsConfigParamMasterServiceHelper()
	{
		
	}
	/**
	 * This method is used to perform getAutoSuggestedSysParam FieldValidations.
	 * @param AutoSearchSysParamRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestedSysParamFieldValidations(AutoSearchSysParamRequest autoSearchSysParamRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_SYS_PARAM_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(autoSearchSysParamRequest.getToken()==null || "".equals(autoSearchSysParamRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(autoSearchSysParamRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(autoSearchSysParamRequest.getUserLoginName()==null || "".equals(autoSearchSysParamRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(autoSearchSysParamRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(autoSearchSysParamRequest.getOrganisationId()==null || "".equals(autoSearchSysParamRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(autoSearchSysParamRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(autoSearchSysParamRequest.getSearchText()==null || "".equals(autoSearchSysParamRequest.getSearchText()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
			}
			else if(autoSearchSysParamRequest.getSearchText().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_SYS_PARAM_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_SYS_PARAM_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_SYS_PARAM_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getAutoSuggestedSysParam BusinessValidations.
	 * @param autoSearchSysParamRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestedSysParamBusinessValidations(AutoSearchSysParamRequest autoSearchSysParamRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_SYS_PARAM_BUSINESS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
			Long.parseLong(autoSearchSysParamRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_SYS_PARAM_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_SYS_PARAM_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_SYS_PARAM_BUSINESS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform retrieveSystemParameters FieldValidations.
	 * @param getSystemParamRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveSystemParametersFieldValidations(GetSystemParamRequest getSystemParamRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
	    errorMsgList = new ArrayList<String>();
		if(getSystemParamRequest.getToken()==null || "".equals(getSystemParamRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(getSystemParamRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(getSystemParamRequest.getUserLoginName()==null || "".equals(getSystemParamRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(getSystemParamRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(getSystemParamRequest.getOrganisationId()==null || "".equals(getSystemParamRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(getSystemParamRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform retrieveSystemParameters BusinessValidations.
	 * @param getSystemParamRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveSystemParametersBusinessValidations(GetSystemParamRequest getSystemParamRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		 
		   try
			{
			   if(getSystemParamRequest.getSysParamId()!=null && !("".equals(getSystemParamRequest.getSysParamId())) && !getSystemParamRequest.getSysParamId().trim().isEmpty())
			   {
				Long.parseLong(getSystemParamRequest.getSysParamId());
			   }
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SYS_PARAM_ID_FIELD);
			}
		   try
			{
			   if(getSystemParamRequest.getStartRow()!=null && !("".equals(getSystemParamRequest.getStartRow())) && !getSystemParamRequest.getStartRow().trim().isEmpty())
			   {
				   Integer.parseInt(getSystemParamRequest.getStartRow());			   
			   }

			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.STARTROW_FIELD);
			}
		   try
			{
			   if(getSystemParamRequest.getInterval()!=null && !("".equals(getSystemParamRequest.getInterval())) && !getSystemParamRequest.getInterval().trim().isEmpty())
			   {
				   Integer.parseInt(getSystemParamRequest.getInterval());	   
			   }

			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.INTERVAL_FIELD);
			}
			try
			{
				Long.parseLong(getSystemParamRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
		
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform updateSystemParam FieldValidations.
	 * @param updateSystemParamRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateSystemParamFieldValidations(UpdateSystemParamRequest updateSystemParamRequest)throws HPPApplicationException
	{
       logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
		{
		errorMsgList = new ArrayList<String>();
		if(updateSystemParamRequest.getToken()==null || "".equals(updateSystemParamRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(updateSystemParamRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(updateSystemParamRequest.getUserLoginName()==null || "".equals(updateSystemParamRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(updateSystemParamRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(updateSystemParamRequest.getAdminUserId()==null || "".equals(updateSystemParamRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(updateSystemParamRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(updateSystemParamRequest.getOrganisationId()==null || "".equals(updateSystemParamRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(updateSystemParamRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(updateSystemParamRequest.getSystemParam()==null || "".equals(updateSystemParamRequest.getSystemParam().toString()))
		{
			errorMsgList.add(HPPClinicAdminConstants.SYSTEMPARAM_OBJECT_FIELD);
		}
		else if(updateSystemParamRequest.getSystemParam().toString().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.SYSTEMPARAM_OBJECT_FIELD);
		}
		else
		{
			if(updateSystemParamRequest.getSystemParam().getParamId()==null || "".equals(updateSystemParamRequest.getSystemParam().getParamId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.PARAM_ID_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getParamId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.PARAM_ID_FIELD);
			}
			if(updateSystemParamRequest.getSystemParam().getSysParamNameId()==null || "".equals(updateSystemParamRequest.getSystemParam().getSysParamNameId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SYS_PARAM_NAME_ID_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getSysParamNameId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SYS_PARAM_NAME_ID_FIELD);
			}
			if(updateSystemParamRequest.getSystemParam().getSysParamName()==null || "".equals(updateSystemParamRequest.getSystemParam().getSysParamName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SYS_PARAM_NAME_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getSysParamName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SYS_PARAM_NAME_FIELD);
			}
			if(updateSystemParamRequest.getSystemParam().getParamValue()==null || "".equals(updateSystemParamRequest.getSystemParam().getParamValue()))
			{
				errorMsgList.add(HPPClinicAdminConstants.PARAM_VALUE_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getParamValue().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.PARAM_VALUE_FIELD);
			}
			if(updateSystemParamRequest.getSystemParam().getLevel()==null || "".equals(updateSystemParamRequest.getSystemParam().getLevel()))
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getLevel().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_FIELD);
			}
			if(updateSystemParamRequest.getSystemParam().getLevelValue()==null || "".equals(updateSystemParamRequest.getSystemParam().getLevelValue()))
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_FIELD_VALUE);
			}
			else if(updateSystemParamRequest.getSystemParam().getLevelValue().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_FIELD_VALUE);
			}
			else if(!updateSystemParamRequest.getSystemParam().getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LEVEL_ORGANIZATION) && !updateSystemParamRequest.getSystemParam().getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LEVEL_LOCATION) && !updateSystemParamRequest.getSystemParam().getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LEVEL_SITE))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_LEVEL_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_LEVEL_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_LEVEL_VALUE_INVALID_MSG);
			}
			if(updateSystemParamRequest.getSystemParam().getLevelValueId()==null || "".equals(updateSystemParamRequest.getSystemParam().getLevelValueId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_VALUE_ID_FIELD);
			}
			else if(updateSystemParamRequest.getSystemParam().getLevelValueId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.LEVEL_VALUE_ID_FIELD);
			}
			if(HPPClinicAdminConstants.LEVEL_LOCATION.equalsIgnoreCase(updateSystemParamRequest.getSystemParam().getLevel()) )
			{
			 if(updateSystemParamRequest.getSystemParam().getSiteId()==null || "".equals(updateSystemParamRequest.getSystemParam().getSiteId()))
			 {
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			 }
			 else if(updateSystemParamRequest.getSystemParam().getSiteId().trim().isEmpty())
			 {
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			 }
			}	
		}
		
	   }
	   catch(HPPApplicationException hae)
		{
  			logger.error(hae.getMessage());
  			throw hae;
		}
	   catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform updateSystemParam BusinessValidations.
	 * @param updateSystemParamRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateSystemParamBusinessValidations(UpdateSystemParamRequest updateSystemParamRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
		{
			Long.parseLong(updateSystemParamRequest.getAdminUserId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		try
		{
			Long.parseLong(updateSystemParamRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		try
		{
			Long.parseLong(updateSystemParamRequest.getSystemParam().getParamId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.PARAM_ID_FIELD);
		}
		try
		{
			Long.parseLong(updateSystemParamRequest.getSystemParam().getSysParamNameId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.SYS_PARAM_NAME_FIELD);
		}
		try
		{
			Long.parseLong(updateSystemParamRequest.getSystemParam().getLevelValueId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.LEVEL_VALUE_ID_FIELD);
		}
		if(HPPClinicAdminConstants.LEVEL_LOCATION.equalsIgnoreCase(updateSystemParamRequest.getSystemParam().getLevel()))
		{
			try
			{
				Long.parseLong(updateSystemParamRequest.getSystemParam().getSiteId());
				
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform getParametersMaster field validations
	 * @param configParamMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getParametersMasterFieldValidations(ConfigParamMasterRequest configParamMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
	    errorMsgList = new ArrayList<String>();
		if(configParamMasterRequest.getToken()==null || "".equals(configParamMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(configParamMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(configParamMasterRequest.getUserLoginName()==null || "".equals(configParamMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(configParamMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(configParamMasterRequest.getOrganisationId()==null || "".equals(configParamMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(configParamMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   }catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.GET_PARAMETERS_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.GET_PARAMETERS_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getParametersMaster business validations
	 * @param configParamMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getParametersMasterBusinessValidations(ConfigParamMasterRequest configParamMasterRequest)throws HPPApplicationException
	{

		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER_BUSS_VALIDATION);
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			Long.parseLong(configParamMasterRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			catch(Exception e)
			{
				logger.error(HPPClinicAdminConstants.GET_PARAMETERS_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.GET_PARAMETERS_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
			}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER_BUSS_VALIDATION);
		   return formatMsgList;
	}
}
