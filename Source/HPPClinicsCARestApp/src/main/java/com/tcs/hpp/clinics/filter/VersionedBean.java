package com.tcs.hpp.clinics.filter;

public class VersionedBean {
	
	String  wsHandler;
	String version;
	public String getWsHandler() {
		return wsHandler;
	}
	public void setWsHandler(String wsHandler) {
		this.wsHandler = wsHandler;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

}
