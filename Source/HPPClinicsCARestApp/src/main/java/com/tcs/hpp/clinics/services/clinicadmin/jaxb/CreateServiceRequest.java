//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.12.31 at 05:53:07 PM IST 
//


package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateServiceRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="CreateServiceRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="UserLoginName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="AdminUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="Service">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="ServiceName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ServiceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Chargable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ResultApplicable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="ClassificationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="OrganisationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "userLoginName",
    "adminUserId",
    "service"
})
@XmlRootElement(name = "CreateServiceRequest")
public class CreateServiceRequest {

    @XmlElement(name = "Token", required = true)
    private String token;
    @XmlElement(name = "UserLoginName", required = true)
    private String userLoginName;
    @XmlElement(name = "AdminUserId", required = true)
    private String adminUserId;
    @XmlElement(name = "Service", required = true)
    private Service service;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }
    
    /**
     * Gets the value of the UserLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserLoginName() {
        return userLoginName;
    }

    /**
     * Sets the value of the userLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }


    /**
     * Gets the value of the adminUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdminUserId() {
        return adminUserId;
    }

    /**
     * Sets the value of the adminUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdminUserId(String value) {
        this.adminUserId = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link Service }
     *     
     */
    public Service getService() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link Service }
     *     
     */
    public void setService(Service value) {
        this.service = value;
    }

   
    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ServiceName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ServiceCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Chargable" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ResultApplicable" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ClassificationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OrganisationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceName",
        "serviceCategory",
        "serviceType",
        "chargable",
        "resultApplicable",
        "classificationId",
        "organisationId",
        "status",
        "tariff",
        "siteId",
        "locationId"
    })
    public static class Service {

        @XmlElement(name = "ServiceName", required = true)
        private String serviceName;
        @XmlElement(name = "ServiceCategory", required = true)
        private String serviceCategory;
        @XmlElement(name = "ServiceType", required = true)
        private String serviceType;
        @XmlElement(name = "Chargable", required = true)
        private String chargable;
        @XmlElement(name = "ResultApplicable", required = true)
        private String resultApplicable;
        @XmlElement(name = "ClassificationId", required = true)
        private String classificationId;
        @XmlElement(name = "OrganisationId", required = true)
        private String organisationId;
        @XmlElement(name = "Status", required = true)
        private String status;
        @XmlElement(name = "Tariff", required = true)
        private String tariff;
        
        @XmlElement(name = "SiteId", required = true)
        private String siteId;
        @XmlElement(name = "LocationId", required = true)
        private String locationId;

        /**
         * Gets the value of the serviceName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceName() {
            return serviceName;
        }

        /**
         * Sets the value of the serviceName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceName(String value) {
            this.serviceName = value;
        }

        /**
         * Gets the value of the serviceCategory property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceCategory() {
            return serviceCategory;
        }

        /**
         * Sets the value of the serviceCategory property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceCategory(String value) {
            this.serviceCategory = value;
        }

        /**
         * Gets the value of the serviceType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceType() {
            return serviceType;
        }

        /**
         * Sets the value of the serviceType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceType(String value) {
            this.serviceType = value;
        }

        /**
         * Gets the value of the chargable property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChargable() {
            return chargable;
        }

        /**
         * Sets the value of the chargable property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChargable(String value) {
            this.chargable = value;
        }

        /**
         * Gets the value of the resultApplicable property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResultApplicable() {
            return resultApplicable;
        }

        /**
         * Sets the value of the resultApplicable property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResultApplicable(String value) {
            this.resultApplicable = value;
        }

        /**
         * Gets the value of the classificationId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClassificationId() {
            return classificationId;
        }

        /**
         * Sets the value of the classificationId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClassificationId(String value) {
            this.classificationId = value;
        }

        /**
         * Gets the value of the organisationId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrganisationId() {
            return organisationId;
        }

        /**
         * Sets the value of the organisationId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrganisationId(String value) {
            this.organisationId = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

		/**
		 * @return the tariff
		 */
		public String getTariff() {
			return tariff;
		}

		/**
		 * @param tariff the tariff to set
		 */
		public void setTariff(String tariff) {
			this.tariff = tariff;
		}

		public String getSiteId() {
			return siteId;
		}

		public void setSiteId(String siteId) {
			this.siteId = siteId;
		}

		public String getLocationId() {
			return locationId;
		}

		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}
        
        

    }

}
