//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0-b52-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.30 at 05:32:53 PM IST 
//


package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlobalUserSearchRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="GlobalUserSearchRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="AdminUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="OrganisationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="UserType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="RoleId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="Speciality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "userLoginName",
    "organisationId",
    "userType",
    "userName",
    "roleId",
    "phoneNumber",
    "speciality",
    "isClinicAdminUser"
})
@XmlRootElement(name = "GlobalUserSearchRequest")
public class GlobalUserSearchRequest {

    @XmlElement(name = "Token", required = true)
    private String token;
    @XmlElement(name = "UserLoginName", required = true)
    private String userLoginName;
    @XmlElement(name = "OrganisationId", required = true)
    private String organisationId;
    @XmlElement(name = "UserType", required = true)
    private String userType;
    @XmlElement(name = "UserName", required = true)
    private String userName;
    @XmlElement(name = "RoleId", required = true)
    private String roleId;
    @XmlElement(name = "PhoneNumber", required = true)
    private String phoneNumber;
    @XmlElement(name = "Speciality", required = true)
    private String speciality;
    @XmlElement(name = "IsClinicAdminUser", required = true)
    private String isClinicAdminUser;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the UserLoginName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserLoginName() {
        return userLoginName;
    }

    /**
     * Sets the value of the UserLoginName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLoginName(String value) {
        this.userLoginName = value;
    }

    /**
     * Gets the value of the organisationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganisationId() {
        return organisationId;
    }

    /**
     * Sets the value of the organisationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganisationId(String value) {
        this.organisationId = value;
    }

    /**
     * Gets the value of the userType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Sets the value of the userType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserType(String value) {
        this.userType = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the roleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * Sets the value of the roleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleId(String value) {
        this.roleId = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the speciality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpeciality() {
        return speciality;
    }

    /**
     * Sets the value of the speciality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpeciality(String value) {
        this.speciality = value;
    }

	/**
	 * Gets the value of the isClinicAdminUser property.
	 * @return the isClinicAdminUser
	 */
	public String getIsClinicAdminUser() {
		return isClinicAdminUser;
	}

	/**
	 * Sets the value of the isClinicAdminUser property.
	 * @param isClinicAdminUser the isClinicAdminUser to set
	 */
	public void setIsClinicAdminUser(String isClinicAdminUser) {
		this.isClinicAdminUser = isClinicAdminUser;
	}

}
