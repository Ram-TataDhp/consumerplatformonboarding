package com.tcs.hpp.clinics.resource;

import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.APK_FILE_DOWNLOAD_DIR_NAME;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.APP_NAME;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.APP_TYPE_FIELD_ERROR_CODE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.APP_TYPE_FIELD_ERROR_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.ERROR_STATUS_CODE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_ERROR_CODE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_ERROR_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_METHOD;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_SERVICE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.HTTP_DOWNLOAD_URL_KEY;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.HTTP_PROPERTY_FILE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.JAVA_GLOBAL;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.METHOD_ENTRY_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.METHOD_EXIT_MSG;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.PERFORMANCE_MONITOR_FETCH_CLINICS_APP_DATA;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.POPULATE_APP_UPDATE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.SET_PERFORMANCE_MONITOR_ACTIVITY_METHOD;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.SLASH_SYMBOL;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.SUCCESS_STATUS_CODE;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.TRACKING_ID_KEY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;*/



import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.dto.FetchAppUpdateDTO;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.FetchAppUpdateRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.FetchAppUpdateResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;
import com.tcs.hpp.platform.properties.PropertiesUtil;


@RestController
@RequestMapping("/HPPClinicsAppUpdate")
public class HPPClinicsAppUpdateService {
	public static final String CLASSNAME = "HPPClinicsAppUpdateService";
	private static Logger logger=Logger.getLogger(HPPClinicsAppUpdateService.class);
	private String trackId;
	private PropertiesUtil utilProperties;
	
	public HPPClinicsAppUpdateService()
	{
		UUID idOne = UUID.randomUUID();
		trackId=idOne.toString();
		MDC.put(TRACKING_ID_KEY, trackId);
	}

	/**
	 * This method is a rest service for fetchAppUpdateHandler...
	 * @param fetchAppUpdateRequest
	 * @return fetchAppUpdateResponse
	 */

	@RequestMapping(value = "1.1/fetchAppUpdateHandler", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity fetchAppUpdateHandler(@RequestBody FetchAppUpdateRequest fetchAppUpdateRequest)
	{
		logger.info(FETCH_APP_UPDATE_SERVICE + METHOD_ENTRY_MSG);
		long startServiceActivity = 0L;
		long endServiceActivity = 0L;
		long startBusinessActivity =0L ;
		long endBusinessActivity = 0L;
		long serviceActivity = 0L;
		long businessActivity = 0L;
		startServiceActivity = System.currentTimeMillis();
		FetchAppUpdateResponse fetchAppUpdateResponse = new FetchAppUpdateResponse();
		BusinessDelegate delegate = new BusinessDelegateImpl();
		List<Serializable> resultList = null;
		try
		{
			List<String> errorMsgList =  HPPClinicsAppUpdateServiceHelper.fetchAppUpdateFieldValidations(fetchAppUpdateRequest);
			boolean isAppTypeValid = HPPClinicsAppUpdateServiceHelper.isAppTypeValid(fetchAppUpdateRequest);
			if(!errorMsgList.isEmpty()) {
				//throw new HPPApplicationException(CLINICADMIN_MANDATORY_FIELDS_CODE, CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString());
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(CLINICADMIN_MANDATORY_FIELDS_CODE,CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			} else if(!isAppTypeValid) {
				//throw new HPPApplicationException(APP_TYPE_FIELD_ERROR_CODE, APP_TYPE_FIELD_ERROR_MSG);
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(APP_TYPE_FIELD_ERROR_CODE, APP_TYPE_FIELD_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				String updateType = null;
				String versionNo = null;
				String apkFileLink = null;
				String isactive = null;
				String appType = fetchAppUpdateRequest.getAppType().trim();
				List<Serializable> paramList = getParamList(appType);
				BusinessComponentMap bcMap = populateAppUpdate(paramList);
				startBusinessActivity = System.currentTimeMillis();
				resultList = delegate.execute(bcMap);
				endBusinessActivity = System.currentTimeMillis();
				FetchAppUpdateDTO fetchAppUpdateDTO = new FetchAppUpdateDTO();
				if(resultList != null && resultList.size() > 0)
				{
					fetchAppUpdateDTO = (FetchAppUpdateDTO)resultList.get(0);
				}
				if(fetchAppUpdateDTO != null) {
					updateType = fetchAppUpdateDTO.getUpdateType();
					versionNo = fetchAppUpdateDTO.getVersionNo();
					apkFileLink = fetchAppUpdateDTO.getApkFileLink();
					utilProperties = new PropertiesUtil(HTTP_PROPERTY_FILE);
					isactive = fetchAppUpdateDTO.getIsactive();
					if(apkFileLink == null) {
						apkFileLink = "";
					} else {
						apkFileLink = utilProperties.getPropertyValue(HTTP_DOWNLOAD_URL_KEY) + APK_FILE_DOWNLOAD_DIR_NAME + SLASH_SYMBOL + apkFileLink;
					}
					fetchAppUpdateResponse.setUpdateType(updateType == null ? "" : updateType);
					fetchAppUpdateResponse.setVersionNo(versionNo == null ? "" : versionNo);
					fetchAppUpdateResponse.setApkFileLink(apkFileLink);
					fetchAppUpdateResponse.setIsactive(isactive == null ? "" : isactive);
				}
				endServiceActivity = System.currentTimeMillis();
				businessActivity = endBusinessActivity-startBusinessActivity;
				serviceActivity = endServiceActivity-startServiceActivity;
				setPerformanceMonitorActivity(serviceActivity, businessActivity, "0", PERFORMANCE_MONITOR_FETCH_CLINICS_APP_DATA, trackId);
			}
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),e.getMessage()))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch(Exception e)
		{
			//return setHeaders(Response.status(ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(FETCH_APP_UPDATE_ERROR_CODE, FETCH_APP_UPDATE_ERROR_MSG))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(FETCH_APP_UPDATE_ERROR_CODE, FETCH_APP_UPDATE_ERROR_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
		}

		logger.info(FETCH_APP_UPDATE_SERVICE + METHOD_EXIT_MSG);
		//return setHeaders(Response.status(SUCCESS_STATUS_CODE).entity(fetchAppUpdateResponse)).build();
		return new ResponseEntity(fetchAppUpdateResponse,HttpStatus.OK);
	}

	/**
	 * This method is used to prepare the business component map for app update service
	 * @param paramList
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateAppUpdate(List paramList) {
		logger.info(POPULATE_APP_UPDATE + METHOD_ENTRY_MSG);
		BusinessComponentMap bcMap = new BusinessComponentMap(APP_NAME, JAVA_GLOBAL+APP_NAME+HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL, FETCH_APP_UPDATE_METHOD, paramList);
		logger.info(POPULATE_APP_UPDATE + METHOD_EXIT_MSG);
		return bcMap;
	}

	/**
	 * This method will add all input parameters into the paramList
	 * @param params
	 * @return paramList
	 */
	private List<Serializable> getParamList(Serializable... params)
	{
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		for(Serializable param : params){
			paramList.add(param);
		}

		return paramList;
	}

	/**
	 * Method to monitor the performance of any activity
	 * @param serviceActivityTime
	 * @param businessActivityTime
	 * @param orgId
	 * @param businessActivityName
	 * @param trackingId
	 */
	private void setPerformanceMonitorActivity(long serviceActivityTime, long businessActivityTime, String orgId, String businessActivityName, String trackingId) {
		logger.info(SET_PERFORMANCE_MONITOR_ACTIVITY_METHOD+METHOD_ENTRY_MSG);
		MonitoringMessagePublisherImpl monitoringMessagePublisherImpl = new MonitoringMessagePublisherImpl();
		MonitoringEvent event = new MonitoringEvent();
		event.setServiceLayerProcessingTime(serviceActivityTime);
		event.setBusinessLayerProcessingTime(businessActivityTime);
		event.setOrgId(Long.parseLong(orgId));
		event.setBusinessActivity(businessActivityName);
		event.setTrackingId(trackingId);		
		try {
			monitoringMessagePublisherImpl.publishMonitoringMessage(event);
		} catch (HPPApplicationException e) {
			logger.error(e);
		}
		logger.info(SET_PERFORMANCE_MONITOR_ACTIVITY_METHOD+METHOD_EXIT_MSG);
	}

	/* private ResponseBuilder setHeaders(ResponseBuilder entity)
	 {
			entity.header("Strict-Transport-Security", "max-age=60");
			entity.header("X-Content-Type-Options", "nosniff");
			entity.header("X-Frame-Options", "DENY");
			entity.header("X-WebKit-CSP", "default-src *");
			entity.header("X-XSS-Protection", "1 mode=block");
			return entity;

	}*/
}
