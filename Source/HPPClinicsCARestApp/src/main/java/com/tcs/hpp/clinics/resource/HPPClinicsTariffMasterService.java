/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;*/






import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.LocationDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.SiteDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.TariffDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.Tariff;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.convertor.HPPClinicAdminJAXBConvertor;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateTariffResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetTariffResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LocationMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LocationMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SiteMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SiteMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateTariffResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisher;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331(Nagesh Chintala)
 * This class provides rest services for Tariff Master module...
 */

@RestController
@RequestMapping("/clinicAdministration")
public class HPPClinicsTariffMasterService {
	
	public static final String CLASSNAME = "HPPClinicsTariffMasterService";	
	
	private static Logger logger=Logger.getLogger(HPPClinicsTariffMasterService.class);
	public HPPClinicsTariffMasterService()
	{
		 String trackId=null;
		 UUID idOne = UUID.randomUUID();
		 trackId=idOne.toString();
		 MDC.put(HPPClinicAdminConstants.TRACKING_ID_KEY, trackId);
	}
	
/**
 * The below method is Restful service of locationMaster service...
 * @param locationMasterRequest
 * @return locationMasterResponse
 */

	@RequestMapping(value = "1.1/locationMaster", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getLocationData(@RequestBody LocationMasterRequest locationMasterRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.LOCATION_MASTER_SERVICE);
		 BusinessDelegate delegate = null;
		 LocationMasterResponse locationMasterResponse =  null;
		 LocationMasterResponse.Location location = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 List<Serializable> locationList=null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
		 try
		  {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			errorMsgList = HPPClinicsTariffMasterServiceHelper.locationMasterFieldValidations(locationMasterRequest);
			
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList=HPPClinicsTariffMasterServiceHelper.locationMasterBusinessValidations(locationMasterRequest);
			 if(formatMsgList.size()==0)
			 {	
				/*
				 * Validating the token...
				 */
			    BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(locationMasterRequest.getUserLoginName(),locationMasterRequest.getToken(),Long.parseLong(locationMasterRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						
						BusinessComponentMap bcMap = HPPClinicAdminUtil.populateBCMForLocation(locationMasterRequest.getOrganisationId());
					    
						businessProcessStartTime = System.currentTimeMillis();
						locationList = (ArrayList<Serializable>) delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);

						locationMasterResponse=new LocationMasterResponse();	
						if(locationList!=null && locationList.size()>0)
						{
							List<LocationDTO> locationDTOs = (List<LocationDTO>)locationList.get(0);
							List<LocationMasterResponse.Location> locations = new ArrayList<LocationMasterResponse.Location>();
							for(LocationDTO locationDTO : locationDTOs)
							  {
								location = new LocationMasterResponse.Location();
								location.setLocationId(locationDTO.getLocationId());
						        location.setName(locationDTO.getName());
						        location.setSiteId(locationDTO.getSiteId());
								
						        locations.add(location);
							  }
							 locationMasterResponse.setLocation(locations);

						}
					}
					else
						//Invalid token
					{
						
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
				}
			    }
				else
					  //Fail in Business validations
				     {
					 	//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					 } 
				
		    }
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(locationMasterRequest.getOrganisationId()!=null && locationMasterRequest.getOrganisationId()!="" && !locationMasterRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(locationMasterRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.LOCATION_MASTER_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);	
		 }
		 catch(HPPApplicationException e)
		 {
			 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
		
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.LOCATION_MASTER_SERVICE);
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(locationMasterResponse)).build();
		 return new ResponseEntity(locationMasterResponse,HttpStatus.OK);
	}
	
	
	/**
	 * The below method is Restful service of siteMaster service...
	 * @param siteMasterRequest
	 * @return siteMasterResponse
	 */
	@RequestMapping(value = "1.1/siteMaster", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getSiteData(@RequestBody SiteMasterRequest siteMasterRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.SITE_MASTER_SERVICE);
		 BusinessDelegate delegate = null;
		 SiteMasterResponse siteMasterResponse =  null;
		 SiteMasterResponse.Site site = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 List<Serializable> siteList=null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
		  {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsTariffMasterServiceHelper.siteMasterFieldValidations(siteMasterRequest);
			
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList=HPPClinicsTariffMasterServiceHelper.siteMasterBusinessValidations(siteMasterRequest);
			 if(formatMsgList.size()==0)
			 {	
				/*
				 * Validating the token...
				 */
			    BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(siteMasterRequest.getUserLoginName(),siteMasterRequest.getToken(),Long.parseLong(siteMasterRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						
						BusinessComponentMap bcMap = populateBCMForSite(siteMasterRequest.getOrganisationId());
					
						
						siteMasterResponse = new SiteMasterResponse();
						
						businessProcessStartTime = System.currentTimeMillis();
						siteList = (ArrayList<Serializable>) delegate.execute(bcMap);	
						 businessProcessEndTime = System.currentTimeMillis();
                         totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(siteList!=null && siteList.size()>0)
						{
							List<SiteDTO> siteDTOs = (List<SiteDTO>)siteList.get(0);
							
							for(SiteDTO siteDTO : siteDTOs)
							  {
								site = new SiteMasterResponse.Site();
								site.setSiteId(siteDTO.getSiteId());
								site.setName(siteDTO.getName());
								
						        
						        siteMasterResponse.getSite().add(site);
							  }
							

						}
					}
					else
						//Invalid token
					{
						
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
				}
			   }
			   else
				//Fail in Business validations
				{
				  //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				   
				} 
				
		    }
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(siteMasterRequest.getOrganisationId()!=null && siteMasterRequest.getOrganisationId()!="" && !siteMasterRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(siteMasterRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SITE_MASTER_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
				
		 }
		 catch(HPPApplicationException e)
		 {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
		
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.SITE_MASTER_SERVICE);
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(siteMasterResponse)).build();
		 return new ResponseEntity(siteMasterResponse,HttpStatus.OK);
	}
	
	
	/**
	 * The below method is restful service for createTariff service...
	 * @param createTariffRequest
	 * @return createTariffResponse
	 */

	@RequestMapping(value = "1.1/createTariff", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity createTariff(@RequestBody CreateTariffRequest createTariffRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.CREATE_TARIFF_SERVICE);
		 BusinessDelegate delegate = null;
		 CreateTariffResponse createTariffResponse =  null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<Serializable> createTariffList = null;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 List<String> createTariffResList=null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
			{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsTariffMasterServiceHelper.createTariffFieldValidations(createTariffRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			 formatMsgList = HPPClinicsTariffMasterServiceHelper.createTariffBusinessValidations(createTariffRequest);
	   		 if(formatMsgList.size()==0)
	   		 {	
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(createTariffRequest.getUserLoginName(),createTariffRequest.getToken(),Long.parseLong(createTariffRequest.getTariff().getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
						Tariff tariff = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoTariffEntity(createTariffRequest);
						BusinessComponentMap bcMap = populateBCMForCreateTariff(tariff,createTariffRequest.getTariff().getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						createTariffList = delegate.execute(bcMap);
						 businessProcessEndTime = System.currentTimeMillis();
                         totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						createTariffResponse = new CreateTariffResponse();
						
						if(createTariffList!=null && createTariffList.size()>0)
						{
							createTariffResList=(List<String>)createTariffList.get(0);
							createTariffResponse.setTariffId(createTariffResList.get(0)); 
							createTariffResponse.setSuccessMessage(HPPClinicAdminConstants.CREATE_TARIFF_SUCCESS_MSG.replace("<Service Name>", createTariffRequest.getTariff().getServiceName()));
						
						}
						
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
					}
				}
	   		  }
  			  else
  				  //Fail in Business validations check...
  			  {
  				 if(formatMsgList.toString().contains(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD))
  				 {	 
  					 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"))).build();
  					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"),HttpStatus.INTERNAL_SERVER_ERROR);
  				 } 
  				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
  				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
  			  }
				
			  }
			  else
				  //Fail in mandatory fields check...
		      {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			  }
			 	serviceTimeEnd = System.currentTimeMillis();
				if(createTariffRequest.getTariff().getOrganisationId()!=null && createTariffRequest.getTariff().getOrganisationId()!="" && !createTariffRequest.getTariff().getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(createTariffRequest.getTariff().getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CREATE_TARIFF_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			}
			catch (HPPApplicationException e) 
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				
			}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CREATE_TARIFF_SERVICE);
		// return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(createTariffResponse)).build();
		 return new ResponseEntity(createTariffResponse,HttpStatus.OK);
	}
	
  /**
   * The below method is restful service for updateTariff service... 
   * @param updateTariffRequest
   * @return updateTariffResponse
   */

	@RequestMapping(value = "1.1/updateTariff", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity updateTariff(@RequestBody UpdateTariffRequest updateTariffRequest)
	{
    	 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.UPDATE_TARIFF_SERVICE);
		 BusinessDelegate delegate = null;
		 UpdateTariffResponse updateTariffResponse=null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
		 try
		 {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			  errorMsgList = HPPClinicsTariffMasterServiceHelper.updateTariffFieldValidations(updateTariffRequest);
				
			  if(errorMsgList.size()==0)
				  //Mandatory fields valid check...
			  {
			   formatMsgList = HPPClinicsTariffMasterServiceHelper.updateTariffBusinessValidations(updateTariffRequest);
			   if(formatMsgList.size()==0)
			   {
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(updateTariffRequest.getUserLoginName(),updateTariffRequest.getToken(),Long.parseLong(updateTariffRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
			    if(isValidTokenList!=null && isValidTokenList.size()>0)
			    {
				
				  isValidToken = (Boolean) isValidTokenList.get(0);
				  if(isValidToken)
					  //Valid token condition...
				  {
					
					oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
					Tariff tariff = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUpdateTariffEntity(updateTariffRequest);
					BusinessComponentMap bcMap = populateBCMForUpdateTariff(tariff,updateTariffRequest.getOrganisationId(),updateTariffRequest.getTariff().getServiceName());
					businessProcessStartTime = System.currentTimeMillis();
					delegate.execute(bcMap);
					 businessProcessEndTime = System.currentTimeMillis();
                     totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
					updateTariffResponse = new UpdateTariffResponse();
					updateTariffResponse.setSuccessMessage(HPPClinicAdminConstants.UPDATE_TARIFF_SUCCESS_MSG.replace("<Service Name>", updateTariffRequest.getTariff().getServiceName()));
				  }
				  else
					  //Invalid token
				  {
					
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					
				  }
				
			   }
			   }
	  			  else
	  				  //Fail in Business validations check...
	  			  {
	  				if(formatMsgList.toString().contains(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD))
	  				 {	 
	  					// return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"))).build();
	  					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,"Invalid Tariff amount - enter only numeric values"),HttpStatus.INTERNAL_SERVER_ERROR);
	  				 }
	  				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
	  				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
	  			  }
			  
			  }
			  else
				  //Fail in mandatory fields check...
		      {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			  }
			  serviceTimeEnd = System.currentTimeMillis();
				if(updateTariffRequest.getOrganisationId()!=null && updateTariffRequest.getOrganisationId()!="" && !updateTariffRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(updateTariffRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.UPDATE_TARIFF_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			
		}
		catch (HPPApplicationException e) 
		{
			if(e.getErrorCode()==HPPClinicAdminConstants.INVALID_PARAMETERS_ERR_CODE)
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			else
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			}
							
		}
		 				
		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(updateTariffResponse)).build();
		 return new ResponseEntity(updateTariffResponse,HttpStatus.OK);

	}
   
  
    /**
     * The below method is restful service for retrieveTariffs service...
     * @param getTariffRequest
     * @return getTariffResponse
     */

	@RequestMapping(value = "1.1/retrieveTariffs", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
   	public ResponseEntity retrieveTariff(@RequestBody GetTariffRequest getTariffRequest)
   	{
    	 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.RETRIEVE_TARIFF_ERR_RESPONSE);
   		 BusinessDelegate delegate = null;
   		 GetTariffResponse getTariffResponse =  null;
   		 GetTariffResponse.Tariff tariff = null;
   		 ArrayList<Serializable> isValidTokenList = null;
   		 List<Serializable> tariffList=null;
   		 boolean isValidToken = false;
   		 List<String> errorMsgList = null;
   		List<String> formatMsgList = null;
   		String count =null;
   		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
   		 try
   		  {
   			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
   			errorMsgList = HPPClinicsTariffMasterServiceHelper.retrieveTariffFieldValidations(getTariffRequest);
   			
   			if(errorMsgList.size()==0)
   				//Mandatory fields valid check...
   			{
   			  formatMsgList = HPPClinicsTariffMasterServiceHelper.retrieveTariffBusinessValidations(getTariffRequest);
   			  if(formatMsgList.size()==0)
   			  {
   	
   				/*
   				 * Validating the token...
   				 */
   			    BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(getTariffRequest.getUserLoginName(),getTariffRequest.getToken(),Long.parseLong(getTariffRequest.getOrganisationId()));
   				
   				delegate = new BusinessDelegateImpl();
   				businessProcessStartTime = System.currentTimeMillis();
   				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
   				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
   				if(isValidTokenList!=null && isValidTokenList.size()>0)
   				{
   					isValidToken = (Boolean) isValidTokenList.get(0);
   					if(isValidToken)
   						//Valid token condition...
   					{
   						
   						BusinessComponentMap bcMap = populateBCMForRetrieveTariff(getTariffRequest);
   					
   						businessProcessStartTime = System.currentTimeMillis();
   						tariffList = (ArrayList<Serializable>) delegate.execute(bcMap);
   					 businessProcessEndTime = System.currentTimeMillis();
                     totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
   						getTariffResponse=new GetTariffResponse();	
   						if(tariffList!=null && tariffList.size()>0)
   						{
   						  List searchTariffCountList = (List)tariffList.get(0);
   						  if(searchTariffCountList.size()>0)
   						  {
   						   count = searchTariffCountList.get(0).toString();
   						   List<TariffDTO> tariffDTOs = (List<TariffDTO>)searchTariffCountList.get(1);          
                           List<GetTariffResponse.Tariff> tariffs = new ArrayList<GetTariffResponse.Tariff>();
  						   for(TariffDTO tariffDTO : tariffDTOs)
  						   {
  							tariff = new GetTariffResponse.Tariff();
  							tariff.setTariffId(tariffDTO.getTariffId());
  							tariff.setTariffAmount(tariffDTO.getTariffAmount());
  					        tariff.setServiceId(tariffDTO.getServiceId());
  					        tariff.setServiceName(tariffDTO.getServiceName());
  					        tariff.setSiteId(tariffDTO.getSiteId());
  					        tariff.setLocationId(tariffDTO.getLocationId());
  					        tariff.setOrganisationId(tariffDTO.getOrganizationId());
  							
  							tariffs.add(tariff);
  							}
  						   getTariffResponse.setTariff(tariffs);
  						   getTariffResponse.setCount(count);
   						  }
   						
   						}
   						
   					}
   					else
   	   					//Invalid token
   	   				{
   	   					
   	   						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
   						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
   	   				
   	   				}
   				}
   				
   			  }
   			  else
   				  //Fail in Business validations check...
   			  {
   				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
   				  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
   			  }
   		    }
   			else
   				//Fail in mandatory fields check...
   			{
   				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
   				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
   			}
   			serviceTimeEnd = System.currentTimeMillis();
			if(getTariffRequest.getOrganisationId()!=null && getTariffRequest.getOrganisationId()!="" && !getTariffRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(getTariffRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RETRIEVE_TARIFFS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
   		 }
   		 catch(HPPApplicationException e)
   		 {
   				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
   			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
   		 }
   		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.RETRIEVE_TARIFF_ERR_RESPONSE);
   		 //return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getTariffResponse)).build();
   		 return new ResponseEntity(getTariffResponse,HttpStatus.OK);
   	}
   
   
	/**
	 * This method is used to prepare the BCM map for SiteMaster service...
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForSite(String orgId) {
		
	    logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_FOR_SITE);
    	ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getSite",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_FOR_SITE);
		return bcMap;
	}
	
	/**
	 * This method is used to prepare the BCM map for Create Tariff service...
	 * @param tariff
	 * @param orgId
	 * @return BusinessComponentMap
	 */
  private BusinessComponentMap populateBCMForCreateTariff(Tariff tariff,String orgId) {
	   
	    logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_CREATE_TARIFF);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(tariff);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"createTariff",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_CREATE_TARIFF);
		return bcMap;
	}
	
   /**
    * This method is used to prepare the BCM map for Update Tariff service...
    * @param tariff
    * @param orgId
    * @param serviceName
    * @return BusinessComponentMap
    */
  private BusinessComponentMap populateBCMForUpdateTariff(Tariff tariff,String orgId, String serviceName) 
	 {
    	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_UPDATE_TARIFF);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(serviceName);
		paramList.add(tariff);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"updateTariff",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_UPDATE_TARIFF);
		return bcMap;
	 }
	
    /**
     * This method is used to prepare the BCM map for Retrieve Tariff service...
     * @param getTariffRequest
     * @return BusinessComponentMap
     */
    private BusinessComponentMap populateBCMForRetrieveTariff(GetTariffRequest getTariffRequest) {
    	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_RETRIEVE_TARIFF);
       	ArrayList<Serializable> paramList = new ArrayList<Serializable>();
   		paramList.add(Long.parseLong(getTariffRequest.getOrganisationId()));
   		paramList.add(Long.parseLong(getTariffRequest.getLocationId()));
   		paramList.add(getTariffRequest.getServiceName());
   		paramList.add(getTariffRequest.getStartRow());
   		paramList.add(getTariffRequest.getInterval());
   		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"retrieveTariff",paramList);		
   		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_RETRIEVE_TARIFF);
   		return bcMap;
   	}
    
/*    private ResponseBuilder setHeaders(ResponseBuilder entity)
	{

		entity.header("Strict-Transport-Security", "max-age=60");
		entity.header("X-Content-Type-Options", "nosniff");
		entity.header("X-Frame-Options", "DENY");
		entity.header("X-WebKit-CSP", "default-src *");
		entity.header("X-XSS-Protection", "1 mode=block");
		return entity;

	}*/   
}
