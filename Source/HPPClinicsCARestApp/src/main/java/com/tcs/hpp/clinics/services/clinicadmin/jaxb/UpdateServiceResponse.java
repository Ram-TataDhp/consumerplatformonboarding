
package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Java class for UpdateServiceResponse element declaration.
* 
* <p>The following schema fragment specifies the expected content contained within this class.
* 
* <pre>
* &lt;element name="UpdateServiceResponse">
*   &lt;complexType>
*     &lt;complexContent>
*       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*         &lt;sequence>
*           &lt;element name="SuccessMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
*         &lt;/sequence>
*       &lt;/restriction>
*     &lt;/complexContent>
*   &lt;/complexType>
* &lt;/element>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 "tariffId",
 "successMessage"
})
@XmlRootElement(name = "UpdateServiceResponse")
public class UpdateServiceResponse {

 @XmlElement(name = "TariffId", required = true)
 private String tariffId;
 @XmlElement(name = "SuccessMessage", required = true)
 private String successMessage;
 
 
/**
 * @return the tariffId
 */
public String getTariffId() {
	return tariffId;
}

/**
 * @param tariffId the tariffId to set
 */
public void setTariffId(String tariffId) {
	this.tariffId = tariffId;
}

/**
 * Gets the value of the successMessage property.
 * @return successMessage
 */
public String getSuccessMessage() {
		return successMessage;
	}

	/**
	 * Sets the value of the successMessage property.
	 * @param successMessage
	 */
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

}
