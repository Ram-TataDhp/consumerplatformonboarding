package com.tcs.hpp.clinics.filter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class Version implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		String handler = "";
		String condition = "";
		String version = "";
		String mapping="";
		Properties prop = new Properties();
		InputStream input = null;
		HashMap<String, String> latestServiceMapping = new HashMap<String, String>();
		HashMap<String, List<VersionedBean>> versionedServiceMapping = new HashMap<String, List<VersionedBean>>();
		List<String> latestVersionWebServices = new ArrayList<String>();
		List<String> latestVersionWebService = new ArrayList<String>();
		List<String> versionedWebServices = new ArrayList<String>();
		List<String> versionedWebService = new ArrayList<String>();
		try {
			input = new FileInputStream("clinicAdminVersion.properties");
			prop.load(input);
			//String latestVersion = prop.getProperty("latestVersion");
		
			String latestVersionedWS = prop.getProperty("latestVersionWebService");
			if (latestVersionedWS != null) {
				String[] latestVersionedWSArray = latestVersionedWS.split(";");
				if (latestVersionedWSArray != null) {
					latestVersionWebServices = Arrays.<String> asList(latestVersionedWSArray);

				}
			}

			if (latestVersionWebServices != null) {
				for (int i = 1; i < latestVersionWebServices.size(); i++) {
					String[] latestVersionWebServicesArray = latestVersionWebServices.get(i).split(":");
					if (latestVersionWebServicesArray != null) {
						latestVersionWebService = Arrays.<String> asList(latestVersionWebServicesArray);
//						System.out.println(latestVersionWebService.get(0)+"   "+latestVersionWebService.get(1));
						latestServiceMapping.put(latestVersionWebService.get(0), latestVersionWebService.get(1));

					}

				}
			}
			List<String> supportedVersion = new ArrayList<String>();
			if (prop.getProperty("supportedVersions") != null) {
				
				String[] supportedVersionArray = prop.getProperty("supportedVersions").split(",");
				if (supportedVersionArray != null) {
					supportedVersion = Arrays.<String> asList(supportedVersionArray);
				}

			}
			List<String> versionedWS = new ArrayList<String>();
			if (supportedVersion != null) {
				for (int i = 0; i < supportedVersion.size(); i++) {
					
					versionedWS.add(prop.getProperty("versionMapping" + supportedVersion.get(i)));
				}

			}

			for (int i = 0; i < versionedWS.size(); i++) {
				List<VersionedBean> versionedBeans = new ArrayList<VersionedBean>();
				

				String[] versionArray = versionedWS.get(i).split(";");
				
				String versionNum = versionArray[0];
				
				for (int j = 1; j < versionArray.length; j++) {
					VersionedBean versionedBean = new VersionedBean();
					
					if (versionArray[j] != null) {
						
						String[] versionArray1 = versionArray[j].split(":");
						

						
						versionedBean.setVersion(versionArray1[1]);
						versionedBean.setWsHandler(versionArray1[0]);
						versionedBeans.add(versionedBean);
						
					}
				}
				
				versionedServiceMapping.put(versionNum, versionedBeans);
				
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
		if (request instanceof HttpServletRequest) {
			String url = ((HttpServletRequest) request).getRequestURL().toString();
//			System.out.println("trace1111111111111     URI    "+url+"   URI   "+((HttpServletRequest) request).getRequestURI().toString());
			//http://localhost:8080/ws-1.0/rest/countrylist/2/cities  
			String[] handlers = url.split("/");

			if (handlers != null) {
				condition = handlers[handlers.length - 3];
if(condition.equals("rest")){
	handler = handlers[handlers.length - 1];
	
	
	mapping=handlers[handlers.length - 2];
	handler=mapping+"/"+handler;
	
}
else{
	version = handlers[handlers.length - 2];
	mapping=handlers[handlers.length - 3];
	handler = handlers[handlers.length - 1];
	handler=mapping+"/"+handler;

}
				
				
			}

		}
	//	System.out.println("mapping    "+mapping);
		RequestDispatcher rd = null;
		if (condition.equals("rest")) {

			
			Iterator it = latestServiceMapping.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
//				System.out.println(handler+"      "+pair.getKey().toString());
				if (handler.equalsIgnoreCase(pair.getKey().toString())) {
//					System.out.println("mapping    "+mapping+"   pair.getValue().toString()   "+pair.getValue().toString()+"   pair.getKey().toString()   "+pair.getKey().toString());
					String endPoint[]=pair.getKey().toString().split("/");
					rd = request.getRequestDispatcher(
							"/rest/"+mapping+"/" + pair.getValue().toString() + "/" +endPoint[1] );
					rd.forward(request, response);

				}

			}

		} else {

//			System.out.println("trace1  ***********");

			Iterator it = versionedServiceMapping.entrySet().iterator();
			while (it.hasNext()) {

				Map.Entry pair = (Map.Entry) it.next();
				
				if (version.equalsIgnoreCase(pair.getKey().toString())) {
					
					List<VersionedBean> versionedBeans = versionedServiceMapping.get(pair.getKey());

					for (VersionedBean versionedBean : versionedBeans) {
						
						if (versionedBean.getWsHandler().equalsIgnoreCase(handler)) {

//							System.out.println("/rest/"+mapping+"/" +versionedBean.getVersion() + "/" + handler);
							String endPoint[]=handler.split("/");
							rd = request.getRequestDispatcher(
									"/rest/"+mapping+"/" +versionedBean.getVersion() + "/" + endPoint[1]);
							rd.forward(request, response);
						}

					}

				}

			}

		}

		

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
