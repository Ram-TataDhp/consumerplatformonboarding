package com.tcs.hpp.clinics.resource;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.AuthenticateUserDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.CommunicationConfigDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.ConfigParamDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.CreateUserReqDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.CreateUserResponseDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.FetchAppUpdateDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.GetUserDetailsDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.LocationDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.RSSFeedDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.SearchUserDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.SubscriptionDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UpdateCommunicationDto;
import com.tcs.hpp.clinics.clinicadmin.dto.UpdateUserRequestDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UserBasicDetailsDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UserDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.CommonOrganization;
import com.tcs.hpp.clinics.clinicadmin.persistence.Role;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.convertor.HPPClinicAdminJAXBConvertor;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AuthenticateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AuthenticateUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AuthenticateUserResponse.HTTPProperty;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AuthenticateUserResponse.Organisation;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ChangePasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ChangePasswordResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CommunicationConfigResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ConfigParamMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserAuthenticationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserAuthenticationResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ForgotPasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ForgotPasswordResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserBasicDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserBasicDetailsResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserConsultationTabDetailsResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserDetailsResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.HomePageServiceResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LogoutRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LogoutResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OtpVerificationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OtpVerificationResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResendSMSRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResendSMSResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResetPasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResetPasswordResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.TermAndConditionRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.TermAndConditionResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateCommunicationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateCommunicationResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserConsultationTabDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserAssocToClinicRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserAssocToClinicResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserSignUpValidateRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserSignUpValidateResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserStatusUpdateRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserStatusUpdateResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.VerifyUserLoginNameRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.VerifyUserLoginNameResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisher;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;
import com.tcs.hpp.platform.properties.PropertiesUtil;


/**
 * This class provides rest services for User Admin module...
 */

@RestController
@RequestMapping("/clinicAdministration")
public class HPPClinicsUserAdminService 
{
	public static final String CLASSNAME = "HPPClinicsUserAdminService";
	private static Logger logger=Logger.getLogger(HPPClinicsUserAdminService.class);
	
	public HPPClinicsUserAdminService()
	{
		 String trackId=null;
		 UUID idOne = UUID.randomUUID();
		 trackId=idOne.toString();
		 MDC.put(HPPClinicAdminConstants.TRACKING_ID_KEY, trackId);
	}
	
	/**
	 * The below method is getHomePagaData rest service..to get the RSSfeed data and subscription plans...
	 * @return Response
	 */	
	
	@RequestMapping(value = "1.1/getHomePageData", method = RequestMethod.GET,produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getHomePageData()
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_HOMEPAGE_DATA);		
		 BusinessDelegate delegate = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 serviceTimeStart = System.currentTimeMillis();
		 List<Serializable> getHomePageList = null;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 HomePageServiceResponse homePageServiceResponse = new HomePageServiceResponse();
		
		try
		{
			 delegate = new BusinessDelegateImpl();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMapWithHomePageDataFeedMap();
			 businessProcessStartTime = System.currentTimeMillis();
			 getHomePageList = delegate.execute(bcMap);
			 businessProcessEndTime = System.currentTimeMillis();
			 if(getHomePageList!=null && getHomePageList.size()>0)
			 {
				 ArrayList<Serializable> homePageDataList = (ArrayList<Serializable>) getHomePageList.get(0);
				 if(homePageDataList!=null && homePageDataList.size()>0)
				 {
					 ArrayList<RSSFeedDTO> rssfeedDTOList = (ArrayList<RSSFeedDTO>) homePageDataList.get(0);
					 if(rssfeedDTOList!=null && rssfeedDTOList.size()>0)
					 {
						 RSSFeedDTO rssfeedDTO =  rssfeedDTOList.get(0);
						 logger.info(HPPClinicAdminConstants.RSS_TEXT+rssfeedDTO.getRssText());
						 logger.info(HPPClinicAdminConstants.RSS_URL_LINK+rssfeedDTO.getUrlLink());
						 homePageServiceResponse.setRssText(rssfeedDTO.getRssText());
						 homePageServiceResponse.setUrlLink(rssfeedDTO.getUrlLink());
					 }
					 
					 
					 ArrayList<SubscriptionDTO> subscriptionplanDTOList = (ArrayList<SubscriptionDTO>) homePageDataList.get(1);
					 if(subscriptionplanDTOList!=null && subscriptionplanDTOList.size()>0)
					 {						 
						 homePageServiceResponse.getPlanType().clear();
						 for(int subPlanCnt=0;subPlanCnt<subscriptionplanDTOList.size();subPlanCnt++)
						 {
							 HomePageServiceResponse.PlanType planType = new HomePageServiceResponse.PlanType();
							 SubscriptionDTO subscriptionplanDTO = subscriptionplanDTOList.get(subPlanCnt);
							 planType.setPlanImageURL(subscriptionplanDTO.getPlanImageUrl());
							 planType.setPlanText(subscriptionplanDTO.getPlanname());
							 homePageServiceResponse.getPlanType().add(planType);
							 
						 }
						
					 }
					 
				 }
			 }	
			 
			 	serviceTimeEnd = System.currentTimeMillis();
			 	monitoringEvent.setOrgId(0L);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.HOME_PAGE_SERVICE_BA);
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime((businessProcessEndTime-businessProcessStartTime));
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_HOMEPAGE_DATA);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(homePageServiceResponse)).build();
		return new ResponseEntity(homePageServiceResponse, HttpStatus.OK);
		
	}
	
	/**
	 * The below method is verifyUserLoginName rest service..to check the availability of login name entered by the user...
	 * @param verifyUserLoginNameRequest
	 * @return verifyUserLoginNameResponse
	 */
	@RequestMapping(value = "1.1/verifyUserLoginName", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity verifyUserLoginName(@RequestBody VerifyUserLoginNameRequest verifyUserLoginNameRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<String> errorMsgList = null;
		 VerifyUserLoginNameResponse verifyUserLoginNameResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.verifyUserLoginNameFieldValidations(verifyUserLoginNameRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForVerifyUserLoginName(verifyUserLoginNameRequest.getUserLoginName());
						
						businessProcessStartTime = System.currentTimeMillis();
						delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						verifyUserLoginNameResponse = new VerifyUserLoginNameResponse();
						verifyUserLoginNameResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_VERIFY_USER_LOGIN_NAME_RESPONSE_MSG);
					
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
					
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)), HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(verifyUserLoginNameResponse)).build();
			return new ResponseEntity(verifyUserLoginNameResponse, HttpStatus.OK);
			
	}
	
	/**
	 * The below method is userSignUpValidate rest service..to validate the user and sends otp to user mobile number for sign in.
	 * @param verifyUserLoginNameRequest
	 * @return verifyUserLoginNameResponse
	 */

	@RequestMapping(value = "1.1/userSignUpValidate", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity userSignUpValidate(@RequestBody UserSignUpValidateRequest userSignUpValidateRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<String> errorMsgList = null;
		 UserSignUpValidateResponse userSignUpValidateResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List<String> formatMsgList = null;
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.userSignUpValidateFieldValidations(userSignUpValidateRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					formatMsgList = HPPClinicsUserAdminServiceHelper.userSignUpValidateBusinessValidations(userSignUpValidateRequest);
					 if(formatMsgList.size()==0)
				     {
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForUserSignUpValidate(userSignUpValidateRequest.getClinicUserId(),userSignUpValidateRequest.getOrganisationId());						
						businessProcessStartTime = System.currentTimeMillis();
						List<Serializable> resultList=delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						List<String> response=(List<String>) resultList.get(0);
						userSignUpValidateResponse = new UserSignUpValidateResponse();
						userSignUpValidateResponse.setOtpCode(response.get(0));
						userSignUpValidateResponse.setMobileNumber(response.get(1));
				     }
				
			     else
			    	 //Fail in Business validation
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
			    	    return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)), HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(userSignUpValidateResponse)).build();
			return new ResponseEntity(userSignUpValidateResponse, HttpStatus.OK);
			
	}
	
	/**
	 * The below method is otpVerification rest service..to verify the otp sent to user mobile.
	 * @param otpVerificationRequest
	 * @return otpVerificationResponse
	 */
	
	@RequestMapping(value = "1.1/otpVerification", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity otpVerification(@RequestBody OtpVerificationRequest otpVerificationRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.OTP_VERIFICATION);		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<String> errorMsgList = null;
		 OtpVerificationResponse otpVerificationResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List<String> formatMsgList = null;
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.otpVerificationFieldValidations(otpVerificationRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					formatMsgList = HPPClinicsUserAdminServiceHelper.otpVerificationBusinessValidations(otpVerificationRequest);
					 if(formatMsgList.size()==0)
				     {
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForOtpVerification(otpVerificationRequest.getClinicUserId(),otpVerificationRequest.getOrganisationId(),otpVerificationRequest.getOtpCode());
						
						businessProcessStartTime = System.currentTimeMillis();
						delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						otpVerificationResponse = new OtpVerificationResponse();
						otpVerificationResponse.setSuccessMessage(HPPClinicAdminConstants.OTP_VERIFICATION_SUCCESS_MESSAGE);
				     }
					 else
					 {
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					 }
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.OTP_VERIFICATION_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return  new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.OTP_VERIFICATION);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(otpVerificationResponse)).build();
			return new ResponseEntity(otpVerificationResponse, HttpStatus.OK);
			
	}
	/**
	 * The below method is resendSms rest service..to resend the otp to user...
	 * @param resendSmsRequest
	 * @return resendSMSResponse
	 */
	@RequestMapping(value = "1.1/resendSms", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity resendSms(@RequestBody ResendSMSRequest resendSmsRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.RESEND_SMS);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<String> errorMsgList = null;
		 ResendSMSResponse resendSMSResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List<String> formatMsgList = null;
			try
			{
				serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				errorMsgList=HPPClinicsUserAdminServiceHelper.resendSmsFieldValidations(resendSmsRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					formatMsgList = HPPClinicsUserAdminServiceHelper.resendSmsBusinessValidations(resendSmsRequest);
					 if(formatMsgList.size()==0)
				     {
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForResendSms(resendSmsRequest.getClinicUserId(), resendSmsRequest.getOrganisationId(), resendSmsRequest.getMobileNumber());
						
						businessProcessStartTime = System.currentTimeMillis();
						//Object returnObj=delegate.execute(bcMap);
						List<Serializable> returnObj=delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						String otpCode=returnObj.get(0).toString();
						resendSMSResponse = new ResendSMSResponse();
						resendSMSResponse.setOtpCode(otpCode);
						//System.out.println("HPPClinicsUserAdminService.resendSms(): "+otpCode+" --"+returnObj.toString());
				     }
					 else
					 {
						// return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					 }
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RESEND_SMS_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)), HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.RESEND_SMS);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(resendSMSResponse)).build();
			return new ResponseEntity(resendSMSResponse, HttpStatus.OK);
			
	}
	/**
	 * The below method is rest service for AppLogin...for clinic user...
	 * @param authenticateUserRequest
	 * @return AuthenticateUserResponse
	 */ 
	
	@RequestMapping(value = "1.1/appLogin", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity authenticateUser(@RequestBody AuthenticateUserRequest authenticateUserRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.AUTHENTICATE_USER);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<Serializable> authenticateUserStatusList = null;		 
		 AuthenticateUserResponse oAuthenticateUserResponse = null;
		 List<CommonOrganization> orgList=null;
		 List<AuthenticateUserResponse.Organisation> resOrgList=new ArrayList<AuthenticateUserResponse.Organisation>();
		 AuthenticateUserResponse.Organisation  resOrg = null;
		 List<String> errorMsgList = null;
		 PropertiesUtil oPropertiesUtil = null;		
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.authenticatUserFieldValidations(authenticateUserRequest);
			if(errorMsgList.size()==0)	//Mandatory fields valid check...
			{
				
				/* * User Authentication Logic starts */
				
				BusinessComponentMap authUserBCMap = HPPClinicsUserAdminMasterServiceHelper.populateBusinessComponentForAuthenticateUser(authenticateUserRequest.getUserLoginName().toUpperCase(),authenticateUserRequest.getPassword());
				businessProcessStartTime = System.currentTimeMillis();
				authenticateUserStatusList = delegate.execute(authUserBCMap);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				oAuthenticateUserResponse = new AuthenticateUserResponse();
				AuthenticateUserDTO oAuthenticateUserDTO = (AuthenticateUserDTO) authenticateUserStatusList.get(0);
				
					BeanUtils.copyProperties(oAuthenticateUserResponse, oAuthenticateUserDTO);
					/**
					 * Setting HTTP properties in the AuthenticationResponse....
					 */
					oPropertiesUtil = new PropertiesUtil(HPPClinicAdminConstants.HTTP_PROPERTY_FILE);
					HTTPProperty httpProperty = new HTTPProperty();
					httpProperty.setUploadURL(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_UPLOAD_URL_KEY));
					httpProperty.setDownloadURL(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_DOWNLOAD_URL_KEY));
					httpProperty.setUsername(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_USER_NAME_KEY));
					httpProperty.setPassword(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_PWD_KEY));
					
					oAuthenticateUserResponse.setHTTPProperty(httpProperty);
					orgList=oAuthenticateUserDTO.getOrgList();
					for(CommonOrganization cOrg:orgList)
					{
						resOrg= new Organisation();
						resOrg.setOrganisationId(cOrg.getOrganizationId().toString());
						resOrg.setOrganisationLogoUrl(cOrg.getLogo());
						resOrg.setOrganizationName(cOrg.getOrgName());
						resOrg.setIsVirtualClinic(cOrg.getVirtualClinic());
						resOrgList.add(resOrg);
					}
					oAuthenticateUserResponse.setOrganisation(resOrgList);
					oAuthenticateUserResponse.setIsAuthenticated(((Boolean)oAuthenticateUserDTO.isAuthenticatedFlag()).toString());
					oAuthenticateUserResponse.setClinicUserId(oAuthenticateUserDTO.getClinicUserId());
			
			}
			else	//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			serviceTimeEnd = System.currentTimeMillis();
			monitoringEvent.setOrgId(0L);
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.APP_LOGIN_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_CREDS_ERR_CODE)
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_CREDENTIALS_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_CREDENTIALS_MSG))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_CREDENTIALS_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_CREDENTIALS_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			else
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,"An exception occured while converting DTO to JAXB"), HttpStatus.INTERNAL_SERVER_ERROR);			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.AUTHENTICATE_USER);
		return new ResponseEntity(oAuthenticateUserResponse, HttpStatus.OK);
		
	}
	
	/**
	 * The below method is rest service for change Password...
	 * @param changePasswordRequest
	 * @return ChangePasswordResponse
	 */
	
	@RequestMapping(value = "1.1/changePassword", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity changePassword(@RequestBody ChangePasswordRequest changePasswordRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.CHANGE_PASSWORD);
		 List<Serializable> changePasswordList = null;
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 Boolean isPasswordUpdated = false;
		 ChangePasswordResponse oChangePasswordResponse = null;
		 List<String> errorMsgList = null;
		 
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;

			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList = HPPClinicsUserAdminServiceHelper.changePasswordFieldValidations(changePasswordRequest);
				
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					/*
					 * Validating the token...
					 */
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(changePasswordRequest.getUserLoginName().toUpperCase(),changePasswordRequest.getToken(),0L);
					
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							
							oChangePasswordResponse = new ChangePasswordResponse();
							BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForChangePassword(changePasswordRequest.getUserLoginName(),changePasswordRequest.getPassword(),changePasswordRequest.getOldPassword());
							
							businessProcessStartTime = System.currentTimeMillis();
							changePasswordList = delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							
							if(changePasswordList!=null && changePasswordList.size()>0)
							{
								isPasswordUpdated =(Boolean) changePasswordList.get(0); 
								if(!isPasswordUpdated)
								{
									//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_CHANGE_PASSWORD_FAIL_CODE,HPPClinicAdminConstants.CLINICADMIN_CHANGE_PASSWORD_FAIL_MSG))).build();
									return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_CHANGE_PASSWORD_FAIL_CODE,HPPClinicAdminConstants.CLINICADMIN_CHANGE_PASSWORD_FAIL_MSG), HttpStatus.INTERNAL_SERVER_ERROR); 
								}
								else
								{
									oChangePasswordResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_CHANGE_PASSWORD_RESPONSE_MSG);
								}
							}
							
						}
						else
							//Invalid token
						{
							
						 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity((HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG)),HttpStatus.INTERNAL_SERVER_ERROR);
							
						}
					
					}
					}
					else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
					
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CHANGE_PASSWORD_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			}
			catch (HPPApplicationException e) 
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CHANGE_PASSWORD);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(oChangePasswordResponse)).build();
			return new ResponseEntity(oChangePasswordResponse,HttpStatus.OK);
			
		}
	/**
	 * The below method is rest service for forgot Password...
	 * @param forgotPasswordRequest
	 * @return ForgotPasswordResponse
	 */
	
	@RequestMapping(value = "1.1/forgotPassword", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity forgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.FORGOT_PASSWORD);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<String> errorMsgList = null;
		 ForgotPasswordResponse forgotPasswordResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.forgotPasswordFieldValidations(forgotPasswordRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForForgotPassword(forgotPasswordRequest.getUserLoginName());
						
						businessProcessStartTime = System.currentTimeMillis();
						List<Serializable> resultList=delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						List<String> response=(List<String>) resultList.get(0);
					    forgotPasswordResponse = new ForgotPasswordResponse();
					    forgotPasswordResponse.setOtpCode(response.get(0));
					    forgotPasswordResponse.setMobileNumber(response.get(1));
					
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.FORGOT_PASSWORD_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.FORGOT_PASSWORD);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(forgotPasswordResponse)).build();
			return new ResponseEntity(forgotPasswordResponse,HttpStatus.OK);
			
	}
	/**
	 * The below method is rest service to reset the Password...
	 * @param resetPasswordRequest
	 * @return resetPasswordResponse
	 */
	
	@RequestMapping(value = "1.1/resetPassword", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity resetPassword(@RequestBody ResetPasswordRequest resetPasswordRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.RESET_PASSWORD);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 ResetPasswordResponse resetPasswordResponse = null;
		 List<String> errorMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
			try
			{
				serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				errorMsgList=HPPClinicsUserAdminServiceHelper.resetPasswordFieldValidations(resetPasswordRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForResetPassword(resetPasswordRequest.getUserLoginName(), resetPasswordRequest.getPassword());
						
						businessProcessStartTime = System.currentTimeMillis();
						delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						resetPasswordResponse = new ResetPasswordResponse();
						resetPasswordResponse.setSuccessMessage("Password has been reset successfully.");
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RESET_PASSWORD_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.RESET_PASSWORD);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(resetPasswordResponse)).build();
			return new ResponseEntity(resetPasswordResponse,HttpStatus.OK);
			
	}
	/**
	 * The below method is rest service to create user login name and password...
	 * @param createUserAuthenticationRequest
	 * @return createUserAuthenticationResponse
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	
	@RequestMapping(value = "1.1/createUserAuthentication", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity createUserAuthentication(@RequestBody CreateUserAuthenticationRequest createUserAuthenticationRequest) throws IllegalAccessException, InvocationTargetException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<Serializable> authenticateUserStatusList = null;
		 
		 CreateUserAuthenticationResponse createUserAuthenticationResponse = null;
		 List<CommonOrganization> orgList=null;
		 List<CreateUserAuthenticationResponse.Organisation> resOrgList=new ArrayList<CreateUserAuthenticationResponse.Organisation>();
		 CreateUserAuthenticationResponse.Organisation  resOrg = null;
		 PropertiesUtil oPropertiesUtil = null;
		 List<String> errorMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List<String> formatMsgList = null;
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.createUserAuthenticationFieldValidations(createUserAuthenticationRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					formatMsgList = HPPClinicsUserAdminServiceHelper.createUserAuthenticationBusinessValidations(createUserAuthenticationRequest);
					 if(formatMsgList.size()==0)
				     {
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForCreateUserAuthentication(createUserAuthenticationRequest.getUserLoginName(), createUserAuthenticationRequest.getPassword(), createUserAuthenticationRequest.getClinicUserId(), createUserAuthenticationRequest.getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						authenticateUserStatusList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
						createUserAuthenticationResponse = new CreateUserAuthenticationResponse();
						AuthenticateUserDTO oAuthenticateUserDTO = (AuthenticateUserDTO) authenticateUserStatusList.get(0);
						/**
						 * Setting HTTP properties in the AuthenticationResponse....
						 */
						oPropertiesUtil = new PropertiesUtil(HPPClinicAdminConstants.HTTP_PROPERTY_FILE);
						CreateUserAuthenticationResponse.HTTPProperty httpProperty = new CreateUserAuthenticationResponse.HTTPProperty();
						httpProperty.setUploadURL(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_UPLOAD_URL_KEY));
						httpProperty.setDownloadURL(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_DOWNLOAD_URL_KEY));
						httpProperty.setUsername(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_USER_NAME_KEY));
						httpProperty.setPassword(oPropertiesUtil.getPropertyValue(HPPClinicAdminConstants.HTTP_PWD_KEY));
						
						createUserAuthenticationResponse.setHTTPProperty(httpProperty);
						orgList=oAuthenticateUserDTO.getOrgList();
						for(CommonOrganization cOrg:orgList)
						{
							resOrg= new CreateUserAuthenticationResponse.Organisation();
							resOrg.setOrganisationId(cOrg.getOrganizationId().toString());
							resOrg.setOrganisationLogoUrl(cOrg.getLogo());
							resOrg.setOrganizationName(cOrg.getOrgName());
							resOrgList.add(resOrg);
						}
						createUserAuthenticationResponse.setOrganisation(resOrgList);
						createUserAuthenticationResponse.setIsAuthenticated(((Boolean)oAuthenticateUserDTO.isAuthenticatedFlag()).toString());
						createUserAuthenticationResponse.setToken(oAuthenticateUserDTO.getToken());
						createUserAuthenticationResponse.setClinicUserId(oAuthenticateUserDTO.getClinicUserId());
						createUserAuthenticationResponse.setDigitalSignaturePath(oAuthenticateUserDTO.getDigitalSignaturePath());
						createUserAuthenticationResponse.setDigitalSignaturePassword(oAuthenticateUserDTO.getDigitalSignaturePassword());
				     }
					 else	//fail in business validation
					 {
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					 }
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				}
							
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(createUserAuthenticationResponse)).build();
			return new ResponseEntity(createUserAuthenticationResponse,HttpStatus.OK);
			
	}
	/**
	 * The below method is rest service for getting user basic details...
	 * @param getUserBasicDetailsRequest
	 * @return getUserBasicDetailsResponse
	 */
	
	@RequestMapping(value = "1.1/getUserBasicDetails", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getUserBasicDetails(@RequestBody GetUserBasicDetailsRequest getUserBasicDetailsRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.GET_USER_BASIC_DETAILS);
		 List<Serializable> userBasicDetails = null;
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 GetUserBasicDetailsResponse getUserBasicDetailsResponse = null;
		 List<String> errorMsgList = null;

		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List responseList=null;
		 List<String> formatMsgList = null;
		 UserBasicDetailsDTO detailsDto=null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.getUserBasicDetailsFieldValidations(getUserBasicDetailsRequest);
			
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				formatMsgList = HPPClinicsUserAdminServiceHelper.getUserBasicDetailsBusinessValidations(getUserBasicDetailsRequest);
				 if(formatMsgList.size()==0)
			     {
					// * Validating the token...
					 
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(getUserBasicDetailsRequest.getUserLoginName(),getUserBasicDetailsRequest.getToken(),0L);
					
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							
							getUserBasicDetailsResponse = new GetUserBasicDetailsResponse();
							BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForGetUserBasicDetails(getUserBasicDetailsRequest.getUserLoginName(), getUserBasicDetailsRequest.getClinicUserId(), getUserBasicDetailsRequest.getOrganisationId());
							
							businessProcessStartTime = System.currentTimeMillis();
							userBasicDetails =  (List<Serializable>)delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							
							if(userBasicDetails!=null)
							{
								
								responseList=(List) userBasicDetails.get(0);
								detailsDto=(UserBasicDetailsDTO) responseList.get(0);
								System.out.println(responseList.get(0));
								getUserBasicDetailsResponse.setRoleType(detailsDto.getRoleType());
								getUserBasicDetailsResponse.setSpeciality(detailsDto.getSpeciality());
								getUserBasicDetailsResponse.setUserImageUrl(detailsDto.getUserImageUrl());
								getUserBasicDetailsResponse.setUserName(detailsDto.getUserName());
								getUserBasicDetailsResponse.setLocation(detailsDto.getLocations());
								getUserBasicDetailsResponse.setRole(detailsDto.getRoles());
								getUserBasicDetailsResponse.setDoctor_list(detailsDto.getDoctor_list());
								getUserBasicDetailsResponse.setSubscriptionId(detailsDto.getSubscriptionId());
								getUserBasicDetailsResponse.setMobileNo(detailsDto.getMobileNo());
								getUserBasicDetailsResponse.setEmailId(detailsDto.getEmailId());
								getUserBasicDetailsResponse.setSignature(detailsDto.getSignature());
								getUserBasicDetailsResponse.setIsVirtualClinic(detailsDto.getVirtualClinic());
								getUserBasicDetailsResponse.setDefaultLocationDetails(detailsDto.getDefaultLocationDetails());
								
										
							}
							else
							{
								//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_GET_USER_BASIC_DETAILS_EXCEPTION_MSG))).build();
								return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_GET_USER_BASIC_DETAILS_EXCEPTION_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
							}
						}
					}
					else
							//Invalid token
					{
							
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
			     }
				 else
				 {
					 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
					
				}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
				
				
					
				serviceTimeEnd = System.currentTimeMillis();
				if(getUserBasicDetailsRequest.getOrganisationId()!=null && getUserBasicDetailsRequest.getOrganisationId()!="" && !getUserBasicDetailsRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(getUserBasicDetailsRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CHANGE_PASSWORD_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			}
			catch (HPPApplicationException e) 
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.GET_USER_BASIC_DETAILS);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getUserBasicDetailsResponse)).build();
			return new ResponseEntity(getUserBasicDetailsResponse,HttpStatus.OK);
		}
	/**
	 * The below method is rest service for Create Clinic User...
	 * @param createUserRequest
	 * @return CreateUserResponse
	 */
	
	@RequestMapping(value = "1.1/createUser", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity createClinicUser(@RequestBody CreateUserRequest createUserRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.CREATE_CLINIC_USER);
		 List<Serializable> createUserList = null;
		 List<Serializable> resultList = null;
		 BusinessDelegate delegate = null;
		 String userIdPrimKey = null;
		 CreateUserResponse createUserResponse =  null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			errorMsgList = HPPClinicsUserAdminServiceHelper.createUserFieldValidations(createUserRequest);
			
			
			if(errorMsgList.size()==0)//Mandatory fields valid check...
			{
		     formatMsgList = HPPClinicsUserAdminServiceHelper.createUserBusinessValidations(createUserRequest);
		     if(formatMsgList.size()==0)
		     {
		    	 delegate = new BusinessDelegateImpl();
		    	 
		  		/*
				 * Validating the token...
				 */
		    	 
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(createUserRequest.getUserLoginName(),createUserRequest.getToken(),Long.parseLong(createUserRequest.getUser().getOrganisationId()));
				
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)//Valid token condition...
					{
						BusinessComponentMap bcMapLink = HPPClinicsUserAdminMasterServiceHelper.populateBCMForApkLink();
						businessProcessStartTime = System.currentTimeMillis();
						resultList = delegate.execute(bcMapLink);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						FetchAppUpdateDTO fetchAppUpdateDTO = new FetchAppUpdateDTO();
						String apkFileLink = null;
						if(resultList != null && resultList.size() > 0)
						{
							fetchAppUpdateDTO = (FetchAppUpdateDTO)resultList.get(0);
						}
						if(fetchAppUpdateDTO != null) {
							apkFileLink = fetchAppUpdateDTO.getApkFileLink();
						}
						
						oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
					
						CreateUserReqDTO createUserReqDTO = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUserEntity(createUserRequest);
						
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForCreateUser(createUserReqDTO,apkFileLink);
						
						businessProcessStartTime = System.currentTimeMillis();
						createUserList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(createUserList!=null && createUserList.size()>0)
						{
							CreateUserResponseDTO oCreateUserResponseDTO = (CreateUserResponseDTO) createUserList.get(0); 
							userIdPrimKey =oCreateUserResponseDTO.getSuccessMessage(); 
							createUserResponse = new CreateUserResponse();
							if(oCreateUserResponseDTO.getIsDuplicateFound().equalsIgnoreCase(HPPClinicAdminConstants.YES))
							{
						
								CreateUserResponse.User userJaxb = null;
								DateFormat dateFormat = new SimpleDateFormat(HPPClinicAdminConstants.DATE_FORMAT);								
								for(UserDTO userDTO:oCreateUserResponseDTO.getUserDTO())
								{
									userJaxb = new CreateUserResponse.User();
									String dob = dateFormat.format(userDTO.getUserDob());
									userJaxb.setAddress(userDTO.getAddress());
									userJaxb.setBloodGroup(userDTO.getBloodGroup());
									userJaxb.setDob(dob);
									userJaxb.setEmail(userDTO.getEmailId());
									userJaxb.setGender(userDTO.getGender());
									userJaxb.setMedicalRegNo(userDTO.getMedicalRegNo());
									userJaxb.setMobileNumber(userDTO.getMobileNo());
									userJaxb.setRoleType(userDTO.getRoleType());
									userJaxb.setSignature(userDTO.getSignature());
									userJaxb.setSpeciality(userDTO.getSpecialty());
									userJaxb.setTitle(userDTO.getTitle());
									userJaxb.setUserId(userDTO.getClinicUserId().toString());
									userJaxb.setUserName(userDTO.getUserName());
									userJaxb.setUserPhoto(userDTO.getUserPhoto());
									createUserResponse.getUser().add(userJaxb);
									
								}
							}
							else
							{
						if(userIdPrimKey.contains("@"))
						{
							StringTokenizer st = new StringTokenizer(userIdPrimKey,"@");
							createUserResponse.setSuccessMessage(st.nextToken());
							userIdPrimKey=st.nextToken();
									createUserResponse.setUserId(userIdPrimKey);
							
						}
						else
						{
									createUserResponse.setUserId(userIdPrimKey);
									createUserResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_CREATE_USER_SUCCESS_MESSAGE_MSG.replace("<User Name>", createUserRequest.getUser().getUserName()).replace("<User ID>", createUserResponse.getUserId()));
						}
							}
						
							createUserResponse.setIsDuplicateFound(oCreateUserResponseDTO.getIsDuplicateFound());
							
						
						}
					}
					else//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
		     }
		     else
		    	 //Fail in Business validation
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
		    	 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(createUserRequest.getUser().getOrganisationId()!=null && createUserRequest.getUser().getOrganisationId()!="" && !createUserRequest.getUser().getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(createUserRequest.getUser().getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.CREATE_USER_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CREATE_CLINIC_USER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(createUserResponse)).build();
		return new ResponseEntity(createUserResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service for Update Clinic User....
	 * @param updateUserRequest
	 * @return UpdateUserResponse
	 */
	
	@RequestMapping(value = "1.1/updateUser", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity updateClinicUser(@RequestBody UpdateUserRequest updateUserRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.UPDATE_CLINIC_USER);
		 
		 BusinessDelegate delegate = null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 UpdateUserResponse oUpdateUserResponse = null;
		 List<String> formatMsgList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			oUpdateUserResponse = new UpdateUserResponse();
			errorMsgList = HPPClinicsUserAdminServiceHelper.updateUserFiledValidations(updateUserRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				
				formatMsgList = HPPClinicsUserAdminServiceHelper.updteUserBusinessValidations(updateUserRequest);
				if(formatMsgList.size()==0)
				{
					
					/* * Validating the token...*/
					 
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(updateUserRequest.getAdminUserLoginName(),updateUserRequest.getToken(),Long.parseLong(updateUserRequest.getOrganisationId()));
					
					delegate = new BusinessDelegateImpl();
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)//Valid token condition...
						{
							oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
							UpdateUserRequestDTO updateUserRequestDTO = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUpdateUserReqDTO(updateUserRequest);
							BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForUpdateUser(updateUserRequestDTO);
							businessProcessStartTime = System.currentTimeMillis();
							 delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							oUpdateUserResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_UPDATE_USER_SUCCESS_MESSAGE_MSG.replace("<User Name>", updateUserRequest.getUserName()));

						
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
					}
				}
				 else
					 //Fail in Business validation
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					 	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(updateUserRequest.getOrganisationId()!=null && updateUserRequest.getOrganisationId()!="" && !updateUserRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(updateUserRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.UPDATE_USER_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.CREATE_CLINIC_USER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(oUpdateUserResponse)).build();
		return new ResponseEntity(oUpdateUserResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service for Clinic user Logout...
	 * @param logoutRequest
	 * @return LogoutResponse
	 */
	
	@RequestMapping(value = "1.1/logout", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity logoutService(@RequestBody LogoutRequest logoutRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.LOGOUT_SERVICE);
		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 LogoutResponse logoutResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			errorMsgList = HPPClinicsUserAdminMasterServiceHelper.logoutServiceFieldValidations(logoutRequest);
			if(errorMsgList.size()==0)//Mandatory fields valid check...
			{
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(logoutRequest.getUserLoginName().toUpperCase(),logoutRequest.getToken(),0L);
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)//Valid token condition...
					{
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForLogout(logoutRequest.getUserLoginName().toUpperCase());
						logoutResponse = new LogoutResponse();
						businessProcessStartTime = System.currentTimeMillis();
						delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);

						logoutResponse.setSuccessMessage(HPPClinicAdminConstants.LOGOUT_SUCCESS_MSG);
					}
					else//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			}
			else//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			
			monitoringEvent.setOrgId(0L);
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.LOGOUT_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.LOGOUT_SERVICE);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(logoutResponse)).build();
		return new ResponseEntity(logoutResponse,HttpStatus.OK);
		
	}
	
	/**
	 * This method is rest service to get Auto suggested user by like searchText.....
	 * @param autoSearchUserRequest
	 * @return autoSearchUserResponse
	 */
	
	@RequestMapping(value = "1.1/autoSuggestionUsers", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getAutoSuggestedUsers(@RequestBody AutoSearchUserRequest autoSearchUserRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_USERS);
				
		 List<Serializable> getAutoSuggUsers = null;		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 AutoSearchUserResponse autoSearchUserResponse = null;
         long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminMasterServiceHelper.getAutoSuggestedUsersFiledValidations(autoSearchUserRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				
				formatMsgList = HPPClinicsUserAdminMasterServiceHelper.getAutoSuggestedUsersBusinessValidations(autoSearchUserRequest);
				if(formatMsgList.size()==0)
				{
					/*
					 * Validating the token...
					 */
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(autoSearchUserRequest.getUserLoginName(),autoSearchUserRequest.getToken(),Long.parseLong(autoSearchUserRequest.getOrganisationId()));
					
					delegate = new BusinessDelegateImpl();
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							autoSearchUserResponse = new AutoSearchUserResponse();
							
							/*
							 * Getting Auto suggested users...
							 */
							BusinessComponentMap bcMapAutoSuggUsers = HPPClinicsUserAdminMasterServiceHelper.populateBCMForAutoSuggestionUsers(autoSearchUserRequest.getOrganisationId(),autoSearchUserRequest.getSearchText());
							
							businessProcessStartTime = System.currentTimeMillis();
							getAutoSuggUsers = delegate.execute(bcMapAutoSuggUsers);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(getAutoSuggUsers!=null && getAutoSuggUsers.size()>0)
							{
								 List<SearchUserDTO> searchUserDTOList = (List<SearchUserDTO>) getAutoSuggUsers.get(0);
								 AutoSearchUserResponse.User jaxbUser = null;
								 if(searchUserDTOList!=null && searchUserDTOList.size()>0)
								 {
									 for(SearchUserDTO searchUserDTO:searchUserDTOList)
										{
											jaxbUser = new AutoSearchUserResponse.User();
											jaxbUser.setClinicUserId(searchUserDTO.getUserId().toString());
											jaxbUser.setUserName(searchUserDTO.getUserName());
											autoSearchUserResponse.getUser().add(jaxbUser);
										}
								 }
								
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
					}
				}
				 else 
					 //Fail in Business validation
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					}
			   
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(autoSearchUserRequest.getOrganisationId()!=null && autoSearchUserRequest.getOrganisationId()!="" && !autoSearchUserRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(autoSearchUserRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.AUTO_SUGGESTION_USERS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_USERS);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(autoSearchUserResponse)).build();
		return new ResponseEntity(autoSearchUserResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service for retrieving users based on search Criteria..
	 * @param searchUserRequest
	 * @return searchUserResponse
	 */
	
	@RequestMapping(value = "1.1/retrieveUsers", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity retrieveUsers(@RequestBody SearchUserRequest searchUserRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_USERS);
		
		 List<Serializable> searchUserList = null;
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 SearchUserResponse searchUserResponse = null;
		 HPPClinicAdminJAXBConvertor hppClinicAdminJAXBConvertor  = null;
		 String count = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.retrieveUsersServiceFiledValidations(searchUserRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				formatMsgList = HPPClinicsUserAdminServiceHelper.retrieveUsersServiceBusinessValidations(searchUserRequest);
				if(formatMsgList.size()==0)
				{
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(searchUserRequest.getUserLoginName(),searchUserRequest.getToken(),Long.parseLong(searchUserRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);

				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForRetrieveUsers(searchUserRequest.getSearchText(),searchUserRequest.getStartRow(),searchUserRequest.getInterval(),searchUserRequest.getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						searchUserList = delegate.execute(bcMap);

						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);

						searchUserResponse = new SearchUserResponse();
						if(searchUserList!=null && searchUserList.size()>0)
						{
							List searchUserCountList = (List)searchUserList.get(0);
							if(searchUserCountList.size()>0)
							{
								 count=searchUserCountList.get(0).toString();
								 List searchUserDTOList = (List)searchUserCountList.get(1);
								 if(searchUserDTOList.size()>0)
								 {
									 hppClinicAdminJAXBConvertor  = new HPPClinicAdminJAXBConvertor();
									 searchUserResponse = hppClinicAdminJAXBConvertor.convertSearchUserDTOtoJAXB(searchUserDTOList);
									
								 }
								 searchUserResponse.setCount(count);
							}
							
						}
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				}
				else
					//Fail in Business validations...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(searchUserRequest.getOrganisationId()!=null && searchUserRequest.getOrganisationId()!="" && !searchUserRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(searchUserRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RETRIEVE_USERS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_USERS);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(searchUserResponse)).build();
		return new ResponseEntity(searchUserResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service for user status update...
	 * @param userStatusUpdateRequest
	 * @return UserStatusUpdateResponse
	 */
	
	@RequestMapping(value = "1.1/userStatusUpdate", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity userStatusUpdate(@RequestBody UserStatusUpdateRequest userStatusUpdateRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_STATUS_UPADTE);		
		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 UserStatusUpdateResponse oUserStatusUpdateResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.userStatusUpdateFiledValidations(userStatusUpdateRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			  formatMsgList = HPPClinicsUserAdminServiceHelper.userStatusUpdateBusinessValidations(userStatusUpdateRequest);
			  if(formatMsgList.size()==0)
			  {
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(userStatusUpdateRequest.getAdminUserLoginName(),userStatusUpdateRequest.getToken(),Long.parseLong(userStatusUpdateRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForUserStatusUpdate(userStatusUpdateRequest.getUserId(),userStatusUpdateRequest.getIsActive(),userStatusUpdateRequest.getRoleType(),userStatusUpdateRequest.getOrganisationId(),userStatusUpdateRequest.getAdminUserId());
						
						businessProcessStartTime = System.currentTimeMillis();
						delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						oUserStatusUpdateResponse = new UserStatusUpdateResponse();
						oUserStatusUpdateResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_UPDATE_USER_STATUS_SUCCESS_MESSAGE);
					
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			  }
			  else
				  //Fail in Business Validations
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(userStatusUpdateRequest.getOrganisationId()!=null && userStatusUpdateRequest.getOrganisationId()!="" && !userStatusUpdateRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(userStatusUpdateRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.USER_STATUS_UPDATE_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_STATUS_UPADTE);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(oUserStatusUpdateResponse)).build();
		return new ResponseEntity(oUserStatusUpdateResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service to get User details based on clinic user id and org id...
	 * @param getUserDetailsRequest
	 * @return getUserDetailsResponse
	 */
	
	@RequestMapping(value = "1.1/getUserDetails", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getUserDetails(@RequestBody GetUserDetailsRequest getUserDetailsRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_DETAILS);
		
		List<Serializable> getUserDtlsList = null;
		 BusinessDelegate delegate = null;		 
		List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		GetUserDetailsResponse getUserDetailsResponse = null;
		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 GetUserDetailsDTO userDetailsDto=null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.getUserDetailsFiledValidations(getUserDetailsRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList = HPPClinicsUserAdminServiceHelper.getUserDetailsBusinessValidations(getUserDetailsRequest);
			 if(formatMsgList.size()==0)
			 {
				 
				 //* Validating token...
				 
				 
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(getUserDetailsRequest.getAdminUserLoginName(),getUserDetailsRequest.getToken(),Long.parseLong(getUserDetailsRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForGetUserDtls(getUserDetailsRequest.getClinicUserId(),getUserDetailsRequest.getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						getUserDtlsList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(getUserDtlsList!=null && getUserDtlsList.size()>0)
						{
							getUserDetailsResponse = new GetUserDetailsResponse();
							 userDetailsDto = (GetUserDetailsDTO) getUserDtlsList.get(0);
							if(userDetailsDto!=null)
							{
								BeanUtils.copyProperties(getUserDetailsResponse, userDetailsDto);
							}
							else
							{
								//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DETAILS_NOT_FOUND_MSG))).build();
								return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DETAILS_NOT_FOUND_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
							}
						}
								
						}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			 }
			 else
				 //Fail in Business Validations
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				    return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(getUserDetailsRequest.getOrganisationId()!=null && getUserDetailsRequest.getOrganisationId()!="" && !getUserDetailsRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(getUserDetailsRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.GET_USER_DETAILS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			logger.error(e.getMessage());
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		catch (Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_CONVERT_ENTITY_TO_JAXB_EXCEPTION_MSG+"::"+e.getMessage());
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_ENTITY_TO_JAXB_EXCEPTION_MSG))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_ENTITY_TO_JAXB_EXCEPTION_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_DETAILS);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getUserDetailsResponse)).build();
		return new ResponseEntity(getUserDetailsResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service to get User Master details..which are pre-requisites to create /update the clinic user...
	 * @param userMasterRequest
	 * @return userMasterResponse
	 */
	
	@RequestMapping(value = "1.1/userMaster", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getUserMaster(@RequestBody UserMasterRequest userMasterRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_MASTER);
		
		 List<Serializable> getUserMasterList = null;		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 UserMasterResponse userMasterResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminMasterServiceHelper.getUserMasterFiledValidations(userMasterRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			  formatMsgList = HPPClinicsUserAdminMasterServiceHelper.getUserMasterBusinessValidations(userMasterRequest);
			  if(formatMsgList.size()==0)
			  {	
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(userMasterRequest.getUserLoginName(),userMasterRequest.getToken(),Long.parseLong(userMasterRequest.getOrganisationId()));				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						userMasterResponse = new UserMasterResponse();
						/*
						 * Getting User Master details...
						 */
						BusinessComponentMap bcMapTitle = HPPClinicsUserAdminMasterServiceHelper.populateBCMForGetUserMaster(userMasterRequest.getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						getUserMasterList = delegate.execute(bcMapTitle);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(getUserMasterList!=null && getUserMasterList.size()>0)
						{
							Map<String,Object> userMasterMap =(Map<String,Object>) getUserMasterList.get(0);
							if(userMasterMap!=null)
							{
								List<String> titleList = (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_TITLE);
								for(String title:titleList)
								{
									userMasterResponse.getTitle().add(title);
								}
								List<String> bloodGroupList = (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_BLOOD_GROUP);
								for(String bloodGrp:bloodGroupList)
								{
									userMasterResponse.getBloodGroup().add(bloodGrp);
								}
								List<String> roleTypeList = (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_ROLE_TYPE);
								for(String roleType:roleTypeList)
								{
									userMasterResponse.getRoleType().add(roleType);
								}
								List<String> specialtyList = (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_SPECIALITY);
								for(String specialty:specialtyList)
								{
									userMasterResponse.getSpeciality().add(specialty);
								}
								List<String> religion = (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_RELIGION);
								for(String religion_val:religion)
								{
									userMasterResponse.getReligion().add(religion_val);
								}
								List<String> governmentIdType= (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_GOVERNMENTIDTYPE);
								for(String goverment_id_type: governmentIdType)
								{
									userMasterResponse.getGovernmentIdType().add(goverment_id_type);
								}
								List<String> employment= (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_EMPLOYMENT);
								for(String employment_val: employment)
								{
									userMasterResponse.getEmployment().add(employment_val);
								}
								List<String> icdCode= (List<String>) userMasterMap.get(HPPClinicAdminConstants.OPTION_TYPE_ICD_CODE);
								for(String icdCode_val: icdCode)
								{
									userMasterResponse.getIcdCode().add(icdCode_val);
								}
								
								Map<String,List<String>> consultation_details=(Map<String,List<String>>) userMasterMap.get(HPPClinicAdminConstants.CONSULTATION_DETAILS);
								if(consultation_details!=null && consultation_details.size()>0)
								{
									
								  userMasterResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRE_EXAMINATION, consultation_details.get(HPPClinicAdminConstants.PRE_EXAMINATION));
								  userMasterResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.EXAMINATION, consultation_details.get(HPPClinicAdminConstants.EXAMINATION));
								  userMasterResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.DIAGNOSIS, consultation_details.get(HPPClinicAdminConstants.DIAGNOSIS));
								  userMasterResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRESCRIPTION, consultation_details.get(HPPClinicAdminConstants.PRESCRIPTION));
														
								}
								
								/*
								 * Getting Operational Role Master Data...
								 */
								
									List<Role> roleList = (List<Role>) userMasterMap.get(HPPClinicAdminConstants.ROLE_DATA);
									for(Role role:roleList)
									{
										UserMasterResponse.Role jaxbRole = new UserMasterResponse.Role();
										jaxbRole.setId(role.getRoleId().toString());
										jaxbRole.setName(role.getRoleName());
										userMasterResponse.getRole().add(jaxbRole);
									}
								
									/*
									 * Getting Location Master Data...
									 */
								
									List<LocationDTO> locationList = (List<LocationDTO>) userMasterMap.get(HPPClinicAdminConstants.LOCATION_DATA);
									for(LocationDTO locationDTO:locationList)
									{
										UserMasterResponse.Location jaxbLocation = new UserMasterResponse.Location();
										jaxbLocation.setLocationId(locationDTO.getLocationId().toString());
										jaxbLocation.setName(locationDTO.getName());
										jaxbLocation.setSiteId(locationDTO.getSiteId().toString());
										userMasterResponse.getLocation().add(jaxbLocation);
									}
								
							}
							
						}
											
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			  }
			  else
				  //Fail in business validation
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				    return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(userMasterRequest.getOrganisationId()!=null && userMasterRequest.getOrganisationId()!="" && !userMasterRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(userMasterRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.USER_MASTER_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_MASTER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(userMasterResponse)).build();
		return new ResponseEntity(userMasterResponse,HttpStatus.OK);
		
	}
	/**
	 * The below method is rest service for associating the user to a clinic...
	 * @param forgotPasswordRequest
	 * @return ForgotPasswordResponse
	 */
	@RequestMapping(value = "1.1/userAssocToClinic", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity userAssocToClinic(@RequestBody UserAssocToClinicRequest userAssocToClinicRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC);
		
		 BusinessDelegate delegate = new BusinessDelegateImpl();
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 UserAssocToClinicResponse userAssocToClinicResponse= null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 List<String> formatMsgList = null;
			try
			{
				 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList=HPPClinicsUserAdminServiceHelper.userAssocToClinicFieldValidations(userAssocToClinicRequest);
				if(errorMsgList.size()==0)
					//Mandatory fields valid check...
				{
					 formatMsgList = HPPClinicsUserAdminServiceHelper.userAssocToClinicBusinessValidations(userAssocToClinicRequest);
					  if(formatMsgList.size()==0)
					  {	
						  /*
							 * Validating the token...
							 */
							BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(userAssocToClinicRequest.getUserLoginName(),userAssocToClinicRequest.getToken(),Long.parseLong(userAssocToClinicRequest.getOrganisationId()));
							
							delegate = new BusinessDelegateImpl();
							businessProcessStartTime = System.currentTimeMillis();
							isValidTokenList = delegate.execute(isValidTokenBCM);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
							if(isValidTokenList!=null && isValidTokenList.size()>0)
							{
								isValidToken = (Boolean) isValidTokenList.get(0);
								if(isValidToken)
									//Valid token condition...
								{
								BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForUserAssocToClinic(userAssocToClinicRequest.getClinicUserId(),userAssocToClinicRequest.getOrganisationId());
								
								businessProcessStartTime = System.currentTimeMillis();
								List<Serializable> resultList=delegate.execute(bcMap);
								businessProcessEndTime = System.currentTimeMillis();
								totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
								CommonOrganization response=(CommonOrganization) resultList.get(0);
								userAssocToClinicResponse = new UserAssocToClinicResponse();
								userAssocToClinicResponse.setOrganisationId(response.getOrganizationId().toString());
								userAssocToClinicResponse.setOrganisationName(response.getOrgName());
								userAssocToClinicResponse.setOrganisationLogoUrl(response.getLogo());
								userAssocToClinicResponse.setSuccessMessage(HPPClinicAdminConstants.CLINICADMIN_USER_ASSOC_TO_CLINIC_SUCCESS_MESSAGE.replace("<Organisation Name>", response.getOrgName()));
}
							}
							else
								//Invalid token
							{
								//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
								return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
							}
						}
					  else
					  {
						  //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
						  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					  }
				}
				else
					//Fail in mandatory fields check...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				serviceTimeEnd = System.currentTimeMillis();
				monitoringEvent.setOrgId(0L);
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_SERVICE_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
					
			}
			catch (HPPApplicationException e) 
			{
				if(e.getErrorCode()==HPPClinicAdminConstants.HPPAPPLICATION_EXCEPTION_INVALID_USER_ERR_CODE)
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_CODE,HPPClinicAdminConstants.CLINICADMIN_USER_DOESNOT_EXIST_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(userAssocToClinicResponse)).build();
			return new ResponseEntity(userAssocToClinicResponse,HttpStatus.OK);
			
	}
	  /* private ResponseBuilder setHeaders(ResponseBuilder entity)
	   {
			entity.header("Strict-Transport-Security", "max-age=60");
			entity.header("X-Content-Type-Options", "nosniff");
			entity.header("X-Frame-Options", "DENY");
			entity.header("X-WebKit-CSP", "default-src *");
			entity.header("X-XSS-Protection", "1 mode=block");
			return entity;

		}*/
	   

@RequestMapping(value = "1.1/getUserConsultationTabDetails", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
public ResponseEntity getUserConsultationTabDetails(@RequestBody GetUserDetailsRequest getUserDetailsRequest)
{
	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" getUserConsultationTabDetails");	
	List<Serializable> getUserConsultationDtlsList = null;
	BusinessDelegate delegate = null;		 
	List<Serializable> isValidTokenList = null;
	boolean isValidToken = false;
	List<String> errorMsgList = null;
	List<String> formatMsgList = null;
	GetUserConsultationTabDetailsResponse getUserConsultationTabDetailsResponse = null;
	long serviceTimeStart = 0L;
	long serviceTimeEnd = 0L;
	long businessProcessStartTime=0L;
	long businessProcessEndTime=0L;
	long totalBusinessProcessingTime = 0L;
	MonitoringMessagePublisher oMonitoringMessagePublisher = null;
	MonitoringEvent monitoringEvent = null;
	//GetUserDetailsDTO userDetailsDto=null;
	try
	{
		 serviceTimeStart = System.currentTimeMillis();
		 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
		 monitoringEvent =new  MonitoringEvent();
		 errorMsgList = HPPClinicsUserAdminServiceHelper.getUserDetailsFiledValidations(getUserDetailsRequest);
		 if(errorMsgList.size()==0)
		 {
			 formatMsgList = HPPClinicsUserAdminServiceHelper.getUserDetailsBusinessValidations(getUserDetailsRequest);
			 if(formatMsgList.size()==0)
			 {
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(getUserDetailsRequest.getAdminUserLoginName(),getUserDetailsRequest.getToken(),Long.parseLong(getUserDetailsRequest.getOrganisationId()));
				 delegate = new BusinessDelegateImpl();
				 businessProcessStartTime = System.currentTimeMillis();
				 isValidTokenList = delegate.execute(isValidTokenBCM);
				 businessProcessEndTime = System.currentTimeMillis();
				 totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				 if(isValidTokenList!=null && isValidTokenList.size()>0)
				 {
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)						
						{
							BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForGetUserConsultationTabDetails(getUserDetailsRequest.getClinicUserId(),getUserDetailsRequest.getOrganisationId());
							businessProcessStartTime = System.currentTimeMillis();
							getUserConsultationDtlsList = delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
							totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(getUserConsultationDtlsList!=null && getUserConsultationDtlsList.size()>0)
							{
								getUserConsultationTabDetailsResponse = new GetUserConsultationTabDetailsResponse();
								Map<String,List<String>> consultationTabDetails=(Map<String,List<String>>)getUserConsultationDtlsList.get(0);
								if(consultationTabDetails!=null && consultationTabDetails.size()>0)
								{
										getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRE_EXAMINATION, consultationTabDetails.get(HPPClinicAdminConstants.PRE_EXAMINATION));
										getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.EXAMINATION, consultationTabDetails.get(HPPClinicAdminConstants.EXAMINATION));
										getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.DIAGNOSIS, consultationTabDetails.get(HPPClinicAdminConstants.DIAGNOSIS));
										getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRESCRIPTION, consultationTabDetails.get(HPPClinicAdminConstants.PRESCRIPTION));
										getUserConsultationTabDetailsResponse.setSuccess("success");
														
								}
								else
								{
									getUserConsultationTabDetailsResponse.setSuccess("Not Found User Consultation Details");
								}	
							}
							
							
							
						}
				}	
				 else
				 {
					//Invalid token
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
			 }
			 else
			 {
				 //Fail in Business Validations
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
		 }
		 else
		 {
			//Fail in mandatory fields check...
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
		 	
		 	serviceTimeEnd = System.currentTimeMillis();
			if(getUserDetailsRequest.getOrganisationId()!=null && getUserDetailsRequest.getOrganisationId()!="" && !getUserDetailsRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(getUserDetailsRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity("GET USER "+HPPClinicAdminConstants.CONSULTATION_DETAILS);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		
		
	}
	catch (HPPApplicationException e) 
	{
		logger.error(e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	catch (Exception e)
	{
		logger.error("getUserConsultationTabDetails::"+e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" getUserConsultationTabDetails");
	//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getUserConsultationTabDetailsResponse)).build();
	return new ResponseEntity(getUserConsultationTabDetailsResponse,HttpStatus.OK);
	
}


@RequestMapping(value = "1.1/updateUserConsultationTabDetails", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
public ResponseEntity updateUserConsultationTabDetails(@RequestBody UpdateUserConsultationTabDetailsRequest updateUserConsultationTabDetailsRequest)
{
	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" getUserConsultationTabDetails");
	
	List<Serializable> updateUserConsultationDtlsList = null;
	BusinessDelegate delegate = null;		 
	List<Serializable> isValidTokenList = null;
	boolean isValidToken = false;
	List<String> errorMsgList = null;
	List<String> formatMsgList = null;
	List<String> isConsultationValidMsgList = null;
	GetUserConsultationTabDetailsResponse getUserConsultationTabDetailsResponse = null;
	long serviceTimeStart = 0L;
	long serviceTimeEnd = 0L;
	long businessProcessStartTime=0L;
	long businessProcessEndTime=0L;
	long totalBusinessProcessingTime = 0L;
	MonitoringMessagePublisher oMonitoringMessagePublisher = null;
	MonitoringEvent monitoringEvent = null;
	//GetUserDetailsDTO userDetailsDto=null;
	try
	{
		 serviceTimeStart = System.currentTimeMillis();
		 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
		 monitoringEvent =new  MonitoringEvent();
		 errorMsgList = HPPClinicsUserAdminServiceHelper.getUserConsultationTabDetailsValidations(updateUserConsultationTabDetailsRequest);
		 if(errorMsgList.size()==0)
		 {
			 formatMsgList = HPPClinicsUserAdminServiceHelper.getUserConsultationTabDetailsBusinessValidations(updateUserConsultationTabDetailsRequest);
			 if(formatMsgList.size()==0)
			 {
				 isConsultationValidMsgList=HPPClinicsUserAdminServiceHelper.isValidUserConsultationTabDetails(updateUserConsultationTabDetailsRequest);
				 if(isConsultationValidMsgList.size()>0)
				 {
					// return setHeaders(Response.status(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,isConsultationValidMsgList.toString()))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,isConsultationValidMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				 }	 
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(updateUserConsultationTabDetailsRequest.getAdminUserLoginName(),updateUserConsultationTabDetailsRequest.getToken(),Long.parseLong(updateUserConsultationTabDetailsRequest.getOrganisationId()));
				 delegate = new BusinessDelegateImpl();
				 businessProcessStartTime = System.currentTimeMillis();
				 isValidTokenList = delegate.execute(isValidTokenBCM);
				 businessProcessEndTime = System.currentTimeMillis();
				 totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				 if(isValidTokenList!=null && isValidTokenList.size()>0)
				 {
						 isValidToken = (Boolean) isValidTokenList.get(0);
						 if(isValidToken)						
						 {
							 	BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForUpdateUserConsultationTabDetails(updateUserConsultationTabDetailsRequest.getClinicUserId(),updateUserConsultationTabDetailsRequest.getOrganisationId(),(ArrayList<String>)updateUserConsultationTabDetailsRequest.getConsultationTabDetails());
								businessProcessStartTime = System.currentTimeMillis();
								updateUserConsultationDtlsList = delegate.execute(bcMap);
								businessProcessEndTime = System.currentTimeMillis();
								totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
								if(updateUserConsultationDtlsList!=null && updateUserConsultationDtlsList.size()>0)
								{
									getUserConsultationTabDetailsResponse = new GetUserConsultationTabDetailsResponse();
									Map<String,List<String>> consultationTabDetails=(Map<String,List<String>>)updateUserConsultationDtlsList.get(0);
									if(consultationTabDetails!=null && consultationTabDetails.size()>0)
									{
											getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRE_EXAMINATION, consultationTabDetails.get(HPPClinicAdminConstants.PRE_EXAMINATION));
											getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.EXAMINATION, consultationTabDetails.get(HPPClinicAdminConstants.EXAMINATION));
											getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.DIAGNOSIS, consultationTabDetails.get(HPPClinicAdminConstants.DIAGNOSIS));
											getUserConsultationTabDetailsResponse.getConsultationTabDetails().put(HPPClinicAdminConstants.PRESCRIPTION, consultationTabDetails.get(HPPClinicAdminConstants.PRESCRIPTION));
																										
									}
									getUserConsultationTabDetailsResponse.setSuccess("success");
										
								}
						 }
				 }
				 else
				 {
					//Invalid token
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
			 }
			 else
			 {
				 //Fail in Business Validations
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
			
		 }
	 else
	 {
		//Fail in mandatory fields check...
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
		 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
	 }
		
		 	serviceTimeEnd = System.currentTimeMillis();
			if(updateUserConsultationTabDetailsRequest.getOrganisationId()!=null && updateUserConsultationTabDetailsRequest.getOrganisationId()!="" && !updateUserConsultationTabDetailsRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(updateUserConsultationTabDetailsRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity("UPDATE USER "+HPPClinicAdminConstants.CONSULTATION_DETAILS);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);	 
		 
	}
	catch (HPPApplicationException e) 
	{
		logger.error(e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	catch (Exception e)
	{
		logger.error("updateUserConsultationTabDetails::"+e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" updateUserConsultationTabDetails");
	//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getUserConsultationTabDetailsResponse)).build();
	return new ResponseEntity(getUserConsultationTabDetailsResponse,HttpStatus.OK);
	
}


@RequestMapping(value = "1.1/acceptTermCondition", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
public ResponseEntity acceptTermCondition(@RequestBody TermAndConditionRequest termAndConditionRequest)
{
	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" acceptTermCondition");
	BusinessDelegate delegate = null;		 
	List<Serializable> isValidTokenList = null;
	boolean isValidToken = false;
	List<String> errorMsgList = null;
	List<Serializable> resultList = null;
	String message=null;
	TermAndConditionResponse termConditionResponse=new TermAndConditionResponse();
	
	try
	{
		 errorMsgList = HPPClinicsUserAdminServiceHelper.acceptTermAndConditinValidations(termAndConditionRequest);
		 if(errorMsgList.size()==0)
		 {
			 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(termAndConditionRequest.getUserLoginName(),termAndConditionRequest.getToken(),Long.parseLong("0"));
			 delegate = new BusinessDelegateImpl();
			 isValidTokenList = delegate.execute(isValidTokenBCM);
			 if(isValidTokenList!=null && isValidTokenList.size()>0)
			 {
				 isValidToken = (Boolean) isValidTokenList.get(0);
				 if(isValidToken)						
				 {
						BusinessComponentMap bcMap = HPPClinicsUserAdminMasterServiceHelper.populateBCMForAcceptTermAndCondition(termAndConditionRequest.getClinicUserId(), termAndConditionRequest.getAcceptTermAndCondition(),termAndConditionRequest.getIpAddress());
						resultList = delegate.execute(bcMap);
						if(resultList!=null && resultList.size()>0){
							message=(String)resultList.get(0);
							termConditionResponse.setSuccessMessage(message);
						}
						else
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity("unable to update the term and condition")).build();
							return new ResponseEntity("unable to update the term and condition",HttpStatus.INTERNAL_SERVER_ERROR);
						}
						
					
				 }
				 else
				 {
					//Invalid token
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
			 }
			 else
			 {
				//Invalid token
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
		 }
		 else
		 {
			//Fail in mandatory fields check...
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
			 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	}
	catch (HPPApplicationException e) 
	{
		logger.error(e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	catch (Exception e)
	{
		logger.error("updateUserConsultationTabDetails::"+e.getMessage());
		//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"))).build();
		return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"An exception occured while fetching the user consultation details"),HttpStatus.INTERNAL_SERVER_ERROR);
	}
	logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" acceptTermCondition");
	//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(termConditionResponse)).build();
	return new ResponseEntity(termConditionResponse,HttpStatus.OK);
			
}

	@RequestMapping(value = "1.1/getCommunicationParam", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getCommunicationConfig(@RequestBody ConfigParamMasterRequest request)
	{
			 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" getCommunicationConfig ");
			 BusinessDelegate delegate = null;
			 CommunicationConfigResponse response=null;
			 ArrayList<Serializable> isValidTokenList = null;
			 boolean isValidToken = false;
			 List<String> errorMsgList = null;
			 List<String> formatMsgList = null;
			 List<Serializable> resultList = null;
			 long serviceTimeStart = 0L;
			 long serviceTimeEnd = 0L;
			 long businessProcessStartTime=0L;
			 long businessProcessEndTime=0L;
			 long totalBusinessProcessingTime = 0L;
			 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
			 MonitoringEvent monitoringEvent = null;
		     try
			 {
		    	 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList = HPPClinicsConfigParamMasterServiceHelper.getParametersMasterFieldValidations(request);
				 if(errorMsgList.size()==0)
				 {
				    formatMsgList = HPPClinicsConfigParamMasterServiceHelper.getParametersMasterBusinessValidations(request);
				    if(formatMsgList.size()==0)
				    {
				    	BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganisationId()));
				    	delegate = new BusinessDelegateImpl();
						businessProcessStartTime = System.currentTimeMillis();
						isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
						businessProcessEndTime = System.currentTimeMillis();
						totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

						if(isValidTokenList!=null && isValidTokenList.size()>0)
						{
							isValidToken = (Boolean) isValidTokenList.get(0);
							if(isValidToken)
							{
							 	String orgId = request.getOrganisationId();
							 	BusinessComponentMap bcMap =HPPClinicsUserAdminMasterServiceHelper.populateBCMForCommunicationConfig(orgId);
								businessProcessStartTime = System.currentTimeMillis();
								resultList = delegate.execute(bcMap);
								businessProcessEndTime = System.currentTimeMillis();
			                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
			                    if(resultList!=null && resultList.size()>0)
			                    {
			                    	CommunicationConfigDTO configParamDTOs = (CommunicationConfigDTO)resultList.get(0);
			                    	response=new CommunicationConfigResponse();
			                    	BeanUtils.copyProperties(response, configParamDTOs);			                    	
			                    }
			                    
							}
							else{
							  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						
							}
						}
				    }
				    else{
				    	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				    }		
				    
				 }
				 else
				 {
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				 }
				 	serviceTimeEnd = System.currentTimeMillis();
					if(request.getOrganisationId()!=null && request.getOrganisationId()!="" && !request.getOrganisationId().isEmpty()){
						monitoringEvent.setOrgId(Long.parseLong(request.getOrganisationId()));
					}
					else{
						monitoringEvent.setOrgId(0L);
					}
					monitoringEvent.setBusinessActivity("GET COMMUNICATION PARAMETER");
					monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
					monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
					monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
					oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			 }
		     catch (HPPApplicationException e) 
			 {
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
		     catch (Exception ex) 
			 {
		    	 ex.printStackTrace();
		    	 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,"An exception occured while converting DTO to JAXB : getCommunicationConfig() "), HttpStatus.INTERNAL_SERVER_ERROR);
			 }
		     
			logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+"  getCommunicationConfig");
			return new ResponseEntity(response,HttpStatus.OK);	
	}
	   
	@RequestMapping(value = "1.1/updateCommunicationParam", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity updateCommunicationParam(@RequestBody UpdateCommunicationRequest request)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" updateCommunicationParam ");
		 BusinessDelegate delegate = null;
		 ArrayList<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 List<Serializable> resultList = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 UpdateCommunicationResponse  response=new UpdateCommunicationResponse();
		 try
		 {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsUserAdminServiceHelper.updateCommunicationParamValidations(request);
			 if(errorMsgList.size()==0)
			 {
				 	BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganizationId()));
			    	delegate = new BusinessDelegateImpl();
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
						{
							UpdateCommunicationDto updateCommunicationDto=new UpdateCommunicationDto();							
							BeanUtils.copyProperties(updateCommunicationDto, request);
							BusinessComponentMap bcMap=HPPClinicsUserAdminMasterServiceHelper.populateBCMForUpdateCommunicationConfig(updateCommunicationDto);
							businessProcessStartTime = System.currentTimeMillis();
							resultList = delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
		                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
		                    String message=(String)resultList.get(0);
		                    if(message!=null)
		                    {
		                    	response.setMessage(message);
		                    }
		                    else
		                    {
		                    	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE,"unable to update the communication config"),HttpStatus.INTERNAL_SERVER_ERROR);
		                    }
		                	                    
						}
						else{
						  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					
						}
					}
			 }
			 else
			 {
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
			 	serviceTimeEnd = System.currentTimeMillis();
				if(request.getOrganizationId()!=null && request.getOrganizationId()!="" && !request.getOrganizationId().isEmpty()){
					monitoringEvent.setOrgId(Long.parseLong(request.getOrganizationId()));
				}
				else{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity("UPDATE COMMUNICATION PARAMETER");
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		 }
	     catch (HPPApplicationException e) 
		 {
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	     catch (Exception ex) 
		 {
	    	 ex.printStackTrace();
	    	 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,"An exception occured while converting DTO to JAXB : getCommunicationConfig() "), HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	     
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+"  getCommunicationConfig");
		return new ResponseEntity(response,HttpStatus.OK);	
		
	}
	
}
