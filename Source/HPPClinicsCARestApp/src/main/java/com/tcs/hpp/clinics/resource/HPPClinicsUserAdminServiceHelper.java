/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AuthenticateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ChangePasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserAuthenticationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ForgotPasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserBasicDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetUserDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OtpVerificationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResendSMSRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ResetPasswordRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.TermAndConditionRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateCommunicationRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserConsultationTabDetailsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserAssocToClinicRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserSignUpValidateRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserStatusUpdateRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.VerifyUserLoginNameRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Description: This Class is used to handle the mandatory field validations and business validations for all the User Admin services.
 */
public final class HPPClinicsUserAdminServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsUserAdminServiceHelper.class);
	private HPPClinicsUserAdminServiceHelper()
	{
		
	}
	/**
	 * This method is used to perform appLogin service field validations
	 * @param authenticateUserRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> authenticatUserFieldValidations(AuthenticateUserRequest authenticateUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.AUTHENTICATE_USER_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();

		if(authenticateUserRequest.getUserLoginName()==null || "".equals(authenticateUserRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(authenticateUserRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(authenticateUserRequest.getPassword()==null || "".equals(authenticateUserRequest.getPassword()))
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
		else if(authenticateUserRequest.getPassword().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.AUTHENTICATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.AUTHENTICATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.AUTHENTICATE_USER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform forgotPassword service field validations
	 * @param forgotPasswordRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> forgotPasswordFieldValidations(ForgotPasswordRequest forgotPasswordRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(forgotPasswordRequest.getUserLoginName()==null || "".equals(forgotPasswordRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
    	else if(forgotPasswordRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform resetPassword service field validations
	 * @param resetPasswordRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> resetPasswordFieldValidations(ResetPasswordRequest resetPasswordRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RESET_PASSWORD_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(resetPasswordRequest.getUserLoginName()==null || "".equals(resetPasswordRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
    	else if(resetPasswordRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
    	}
    	if(resetPasswordRequest.getPassword()==null || "".equals(resetPasswordRequest.getPassword()))
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
    	else if(resetPasswordRequest.getPassword().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.FORGOT_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RESET_PASSWORD_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform createUserAuthentication service field validations
	 * @param createUserAuthenticationRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createUserAuthenticationFieldValidations(CreateUserAuthenticationRequest createUserAuthenticationRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(createUserAuthenticationRequest.getUserLoginName()==null || "".equals(createUserAuthenticationRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
    	else if(createUserAuthenticationRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
    	}
    	if(createUserAuthenticationRequest.getPassword()==null || "".equals(createUserAuthenticationRequest.getPassword()))
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
    	else if(createUserAuthenticationRequest.getPassword().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
    	}
    	if(createUserAuthenticationRequest.getClinicUserId()==null || "".equals(createUserAuthenticationRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
		}
    	else if(createUserAuthenticationRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
    	}
    	if(createUserAuthenticationRequest.getOrganisationId()==null || "".equals(createUserAuthenticationRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(createUserAuthenticationRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform createUserAuthentication BusinessValidations.
	 * @param createUserAuthenticationRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createUserAuthenticationBusinessValidations(CreateUserAuthenticationRequest createUserAuthenticationRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(createUserAuthenticationRequest.getClinicUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(createUserAuthenticationRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_USER_AUTHENTICATION_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform createUser service FieldValidations.... 
	 * @param createUserRequest
	 * @return
	 * @throws HPPApplicationException
	 */
	public static List<String> createUserFieldValidations(CreateUserRequest createUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_USER_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
		if(createUserRequest.getUserLoginName()==null || "".equals(createUserRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(createUserRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(createUserRequest.getToken()==null || "".equals(createUserRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(createUserRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(createUserRequest.getSiteId()==null || "".equals(createUserRequest.getSiteId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
		}
		else if(createUserRequest.getSiteId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
		}
		if(createUserRequest.getAdminUserId()==null || "".equals(createUserRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(createUserRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(createUserRequest.getRoleId().size()==0)
		{
			errorMsgList.add(HPPClinicAdminConstants.ROLE_ID_LIST_FIELD);
		}
		if(createUserRequest.getLocationId().size()==0)
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATION_ID_LIST_FIELD);
		}
		if(createUserRequest.getIsDuplicateCheckRequired()==null || "".equals(createUserRequest.getIsDuplicateCheckRequired()))
		{
			errorMsgList.add(HPPClinicAdminConstants.IS_DUPLICATE_CHECK_REQUIRED_FIELD);
		}
		else if(createUserRequest.getIsDuplicateCheckRequired().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.IS_DUPLICATE_CHECK_REQUIRED_FIELD);
		}
		if(createUserRequest.getIsUserMappingRequired()==null || "".equals(createUserRequest.getIsUserMappingRequired()))
		{
			errorMsgList.add(HPPClinicAdminConstants.IS_USER_MAPPING_REQUIRED_FIELD);
		}
		else if(createUserRequest.getIsUserMappingRequired().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.IS_USER_MAPPING_REQUIRED_FIELD);
		}
		if(createUserRequest.getUser()==null || "".equals(createUserRequest.getUser().toString()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USER_OBJECT_FIELD);
		}
		else
		{
			if(HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()))
			{
				if(createUserRequest.getUser().getUserId()==null || "".equals(createUserRequest.getUser().getUserId()))
				{
					errorMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
				}
				else if(createUserRequest.getUser().getUserId().trim().isEmpty())
				{
					errorMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
				}
			}
			if(createUserRequest.getUser().getAddress()==null || "".equals(createUserRequest.getUser().getAddress()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ADDRESS_FIELD);
			}
			else if(createUserRequest.getUser().getAddress().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ADDRESS_FIELD);
			}
			if(createUserRequest.getUser().getBloodGroup()==null || "".equals(createUserRequest.getUser().getBloodGroup()))
			{
				errorMsgList.add(HPPClinicAdminConstants.BLOODGROUP_FIELD);
			}
			else if(createUserRequest.getUser().getBloodGroup().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.BLOODGROUP_FIELD);
			}
			if(createUserRequest.getUser().getDOB()==null || "".equals(createUserRequest.getUser().getDOB()))
			{
				errorMsgList.add(HPPClinicAdminConstants.DOB_FIELD);
			}
			else if(createUserRequest.getUser().getDOB().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.DOB_FIELD);
			}
			if(createUserRequest.getUser().getEmail()==null || "".equals(createUserRequest.getUser().getEmail()))
			{
				errorMsgList.add(HPPClinicAdminConstants.EMAIL_FIELD);
			}
			else if(createUserRequest.getUser().getEmail().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.EMAIL_FIELD);
			}
			if(createUserRequest.getUser().getGender()==null || "".equals(createUserRequest.getUser().getGender()))
			{
				errorMsgList.add(HPPClinicAdminConstants.GENDER_FIELD);
			}
			else if(createUserRequest.getUser().getGender().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.GENDER_FIELD);
			}
			if(createUserRequest.getUser().getIsActive()==null || "".equals(createUserRequest.getUser().getIsActive()))
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			else if(createUserRequest.getUser().getIsActive().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.STATUS_FIELD);
			}
			if(createUserRequest.getUser().getMobileNumber()==null || "".equals(createUserRequest.getUser().getMobileNumber()))
			{
				errorMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
			}
			else if(createUserRequest.getUser().getMobileNumber().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
			}
			if(createUserRequest.getUser().getOrganisationId()==null || "".equals(createUserRequest.getUser().getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(createUserRequest.getUser().getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(createUserRequest.getUser().getOrganisationName()==null || "".equals(createUserRequest.getUser().getOrganisationName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_NAME_FIELD);
			}
			else if(createUserRequest.getUser().getOrganisationName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_NAME_FIELD);
			}
			if(createUserRequest.getUser().getRoleType()==null || "".equals(createUserRequest.getUser().getRoleType()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
			}
			else if(createUserRequest.getUser().getRoleType().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
			}
			else if(HPPClinicAdminConstants.DOCTOR.equalsIgnoreCase(createUserRequest.getUser().getRoleType()))
			{
				if(createUserRequest.getUser().getSpeciality()==null || "".equals(createUserRequest.getUser().getSpeciality()))
				{
					errorMsgList.add(HPPClinicAdminConstants.SPECIALITY_FIELD);
				}
				else if(createUserRequest.getUser().getSpeciality().trim().isEmpty())
				{
					errorMsgList.add(HPPClinicAdminConstants.SPECIALITY_FIELD);
				}
			}
			if(createUserRequest.getUser().getTitle()==null || "".equals(createUserRequest.getUser().getTitle()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TITLE_FIELD);
			}
			else if(createUserRequest.getUser().getTitle().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TITLE_FIELD);
			}
			if(createUserRequest.getUser().getUserName()==null || "".equals(createUserRequest.getUser().getUserName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERNAME_FIELD);
			}
			else if(createUserRequest.getUser().getUserName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERNAME_FIELD);
			}
			if(createUserRequest.getUser().getIsActive()==null || "".equals(createUserRequest.getUser().getIsActive()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ISACTIVE_FIELD);
			}
			else if(createUserRequest.getUser().getIsActive().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ISACTIVE_FIELD);
			}
			if(createUserRequest.getUser().getIsVirtualClinic()==null || "".equals(createUserRequest.getUser().getIsVirtualClinic()))
			{
				errorMsgList.add(HPPClinicAdminConstants.VIRTUALCLINIC);
			}
			else if(createUserRequest.getUser().getIsVirtualClinic().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.VIRTUALCLINIC);
			}
	     }
		if(errorMsgList.size()==0)
		{
			if(!HPPClinicAdminConstants.USER_STATUS_ACTIVE.equalsIgnoreCase(createUserRequest.getUser().getIsActive()))
			{
					logger.error(HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_MSG);
					throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_ISACTIVE_VALUE_MSG);
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsDuplicateCheckRequired()) &&  !HPPClinicAdminConstants.NO.equalsIgnoreCase(createUserRequest.getIsDuplicateCheckRequired()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_IS_DUPLICATE_CHK_REQD_VALUE_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_IS_DUPLICATE_CHK_REQD_VALUE_MSG);
				
			}
			if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()) &&  !HPPClinicAdminConstants.NO.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_IS_USER_MAPPING_REQD_VALUE_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_IS_USER_MAPPING_REQD_VALUE_MSG);
				
			}
			if(HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsDuplicateCheckRequired()) && HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_IS_USER_MAPPING_AND_IS_DUPLICATE_CHECK_REQD_VALUE_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_IS_USER_MAPPING_AND_IS_DUPLICATE_CHECK_REQD_VALUE_MSG);
			}
		}
		
		
	    }
        catch(HPPApplicationException hae)
        {
    		logger.error(hae.getMessage());
    		throw hae;
		}
        catch(Exception e)
		{
	    	logger.error(HPPClinicAdminConstants.CREATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
	    	throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_USER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform createUser service BusinessValidations.
	 * @param createUserRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createUserBusinessValidations(CreateUserRequest createUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_USER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   
	   try
	   {
		   Long.parseLong(createUserRequest.getSiteId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
	   }
	   if(HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()))
	   {
		   try
		   {
			   Long.parseLong(createUserRequest.getUser().getUserId());
		   }
		   catch(NumberFormatException nfe)
		   {
			   formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
		   }
	   }
	   try
	   {
		   Long.parseLong(createUserRequest.getUser().getOrganisationId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
	   }
	   try
	   {
		   Long.parseLong(createUserRequest.getAdminUserId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
	   }
	   try
	   {
		   Long.parseLong(createUserRequest.getUser().getMobileNumber());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
		   
	   }
	   try
	   {
		   for(String roleId:createUserRequest.getRoleId())
		   {
			   if(roleId!=null && !("".equals(roleId)) && !roleId.trim().isEmpty())
			   {
				   Long.parseLong(roleId);
			   }
			  
		   }
	   }catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ROLEID_FIELD);
		   
	   }
	   try
	   {
		   for(String locId:createUserRequest.getLocationId())
		   {
			   if(locId!=null && !("".equals(locId)) && !locId.trim().isEmpty())
			   {
				   Long.parseLong(locId);
			   }
			  
		   }
	   }catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		   
	   }
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_USER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform updateUser FieldValidations.
	 * @param updateUserRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateUserFiledValidations(UpdateUserRequest updateUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_USER_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
	    errorMsgList = new ArrayList<String>();
		
		if(updateUserRequest.getToken()==null || "".equals(updateUserRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(updateUserRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(updateUserRequest.getUserName()==null || "".equals(updateUserRequest.getUserName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERNAME_FIELD);
		}
		else if(updateUserRequest.getUserName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERNAME_FIELD);
		}
		if(updateUserRequest.getRoleType()==null || "".equals(updateUserRequest.getRoleType()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
		}
		else if(updateUserRequest.getRoleType().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
		}
		if(updateUserRequest.getAdminUserLoginName()==null || "".equals(updateUserRequest.getAdminUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
		}
		else if(updateUserRequest.getAdminUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
		}
		if(updateUserRequest.getAdminUserId()==null || "".equals(updateUserRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(updateUserRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(updateUserRequest.getClinicUserId()==null || "".equals(updateUserRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
		}
		else if(updateUserRequest.getClinicUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
		}
		if(updateUserRequest.getSiteId()==null || "".equals(updateUserRequest.getSiteId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
		}
		else if(updateUserRequest.getSiteId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
		}
		if(updateUserRequest.getLocationId()==null || "".equals(updateUserRequest.getLocationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
		else if(updateUserRequest.getLocationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
		if(updateUserRequest.getOrganisationId()==null || "".equals(updateUserRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORGANIZATION_ID);
		}
		else if(updateUserRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORGANIZATION_ID);
		}
		if(updateUserRequest.getRoleId().size()==0)
		{
			errorMsgList.add(HPPClinicAdminConstants.ROLE_ID_LIST_FIELD);
		}
		
		if(errorMsgList.size()==0)
		{
			if(!HPPClinicAdminConstants.DOCTOR.equalsIgnoreCase(updateUserRequest.getRoleType()) && !HPPClinicAdminConstants.STAFF.equalsIgnoreCase(updateUserRequest.getRoleType()) && !HPPClinicAdminConstants.DIGITIZATION.equalsIgnoreCase(updateUserRequest.getRoleType()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_ROLE_TYPE_VALUE_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_ROLE_TYPE_VALUE_MSG);
			}
		}
			
			
		
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.UPDATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_USER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_USER_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform updateUser BusinessValidations.
	 * @param updateUserRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updteUserBusinessValidations(UpdateUserRequest updateUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_USER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   
	   try
	   {
		   Long.parseLong(updateUserRequest.getSiteId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
	   }
	   try
	   {
		   Long.parseLong(updateUserRequest.getOrganisationId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ORGANIZATION_ID);
	   }
	   try
	   {
		   Long.parseLong(updateUserRequest.getLocationId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
	   }
	   try
	   {
		   Long.parseLong(updateUserRequest.getAdminUserId());
	   }
	   catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
	   }
	   
	   try
	   {
		   for(String roleId:updateUserRequest.getRoleId())
		   {
			   if(roleId!=null && !("".equals(roleId)) && !roleId.trim().isEmpty())
			   {
				   Long.parseLong(roleId);
			   }
			  
		   }
	   }catch(NumberFormatException nfe)
	   {
		   formatMsgList.add(HPPClinicAdminConstants.ROLEID_FIELD);
		   
	   }
	  
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_USER_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform retrieveUsers FieldValidations.
	 * @param searchUserRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveUsersServiceFiledValidations(SearchUserRequest searchUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(searchUserRequest.getToken()==null || "".equals(searchUserRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(searchUserRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(searchUserRequest.getUserLoginName()==null || "".equals(searchUserRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(searchUserRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(searchUserRequest.getOrganisationId()==null || "".equals(searchUserRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(searchUserRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform retrieveUsers BusinessValidations.
	 * @param searchUserRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveUsersServiceBusinessValidations(SearchUserRequest searchUserRequest)throws HPPApplicationException
	{
		   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION);
		
		   List<String> formatMsgList = new ArrayList<String>();
		   try
		   {
			   
				try
				{
					Integer.parseInt(searchUserRequest.getOrganisationId());
				}
				catch(NumberFormatException nfe)
				{
					formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				}
				try
				{
					if(searchUserRequest.getStartRow()!=null && !("".equals(searchUserRequest.getStartRow())) && !searchUserRequest.getStartRow().trim().isEmpty())
					{
					Integer.parseInt(searchUserRequest.getStartRow());
					}
				}
				catch(NumberFormatException nfe)
				{
					formatMsgList.add(HPPClinicAdminConstants.STARTROW_FIELD);
				}
				try
				{
					if(searchUserRequest.getInterval()!=null && !("".equals(searchUserRequest.getInterval())) && !searchUserRequest.getInterval().trim().isEmpty())
					{
						Integer.parseInt(searchUserRequest.getInterval());
					}
				}
				catch(NumberFormatException nfe)
				{
					formatMsgList.add(HPPClinicAdminConstants.INTERVAL_FIELD);
					
				}
		   }catch(Exception e)
		   {
			   logger.error(HPPClinicAdminConstants.RETRIEVE_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			   throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		   }
		
		   
		   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_USERS_FIELD_VALIDATION);
			return formatMsgList;
	}
	
	/**
	 * This method is used to perform userStatusUpdate FieldValidations.
	 * @param UserStatusUpdateRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userStatusUpdateFiledValidations(UserStatusUpdateRequest userStatusUpdateRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_STATUS_UPDATE_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(userStatusUpdateRequest.getToken()==null || "".equals(userStatusUpdateRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(userStatusUpdateRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(userStatusUpdateRequest.getAdminUserLoginName()==null || "".equals(userStatusUpdateRequest.getAdminUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			else if(userStatusUpdateRequest.getAdminUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			if(userStatusUpdateRequest.getOrganisationId()==null || "".equals(userStatusUpdateRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(userStatusUpdateRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(userStatusUpdateRequest.getIsActive()==null|| "".equals(userStatusUpdateRequest.getIsActive()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ISACTIVE_FIELD);
			}
			else if(userStatusUpdateRequest.getIsActive().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ISACTIVE_FIELD);
			}
			if(userStatusUpdateRequest.getUserId()==null || "".equals(userStatusUpdateRequest.getUserId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			else if(userStatusUpdateRequest.getUserId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			if(userStatusUpdateRequest.getRoleType()==null || "".equals(userStatusUpdateRequest.getRoleType()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
			}
			else if(userStatusUpdateRequest.getRoleType().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ROLE_TYPE_FIELD);
			}
			
			if(errorMsgList.size()==0)
			{
				if(!HPPClinicAdminConstants.USER_STATUS_ACTIVE.equalsIgnoreCase(userStatusUpdateRequest.getIsActive()) && !HPPClinicAdminConstants.USER_STATUS_INACTIVE.equalsIgnoreCase(userStatusUpdateRequest.getIsActive()))
				{
					logger.error(HPPClinicAdminConstants.CLINICADMIN_USER_ISACTIVE_VALUE_INVALID_MSG);
					throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_USER_ISACTIVE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_USER_ISACTIVE_VALUE_INVALID_MSG);
				}
				if(!(userStatusUpdateRequest.getRoleType().equalsIgnoreCase(HPPClinicAdminConstants.DOCTOR)) && !(userStatusUpdateRequest.getRoleType().equalsIgnoreCase(HPPClinicAdminConstants.STAFF)) && !(userStatusUpdateRequest.getRoleType().equalsIgnoreCase(HPPClinicAdminConstants.DIGITIZATION)))
				{
					logger.error(HPPClinicAdminConstants.CLINICADMIN_ROLETYPE_VALUE_MSG);
					throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE, HPPClinicAdminConstants.CLINICADMIN_ROLETYPE_VALUE_MSG);
				}
			}
		}
		catch(HPPApplicationException hae)
		{
   			logger.error(hae.getMessage());
   			throw hae;
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_USER_STATUS_UPDATE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_USER_STATUS_UPDATE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_STATUS_UPDATE_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform userStatusUpdate BusinessValidations.
	 * @param userStatusUpdateRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userStatusUpdateBusinessValidations(UserStatusUpdateRequest userStatusUpdateRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_STATUS_UPDATE_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(userStatusUpdateRequest.getUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(userStatusUpdateRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			try
			{
				Long.parseLong(userStatusUpdateRequest.getAdminUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
				
			}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.USER_STATUS_UPDATE_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.USER_STATUS_UPDATE_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_STATUS_UPDATE_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform getUserDetails FieldValidations.
	 * @param getUserDetailsRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserDetailsFiledValidations(GetUserDetailsRequest getUserDetailsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(getUserDetailsRequest.getToken()==null || "".equals(getUserDetailsRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(getUserDetailsRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(getUserDetailsRequest.getClinicUserId()==null || "".equals(getUserDetailsRequest.getClinicUserId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
			}
			else if(getUserDetailsRequest.getClinicUserId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
			}
			if(getUserDetailsRequest.getAdminUserLoginName()==null || "".equals(getUserDetailsRequest.getAdminUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			else if(getUserDetailsRequest.getAdminUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			if(getUserDetailsRequest.getOrganisationId()==null || "".equals(getUserDetailsRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(getUserDetailsRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getUserDetails BusinessValidations.
	 * @param getUserDetailsRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserDetailsBusinessValidations(GetUserDetailsRequest getUserDetailsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_BUSS_VALIDATION);
		
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			Long.parseLong(getUserDetailsRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform changePassword FieldValidations.
	 * @param changePasswordRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> changePasswordFieldValidations(ChangePasswordRequest changePasswordRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CHANGE_PASSWORD_FIELD_VALIDATION);
	   List<String> errorMsgList = null;
		try
		{
		errorMsgList = new ArrayList<String>();
		if(changePasswordRequest.getUserLoginName()==null || "".equals(changePasswordRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(changePasswordRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(changePasswordRequest.getPassword()==null || "".equals(changePasswordRequest.getPassword()))
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
		else if(changePasswordRequest.getPassword().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.PASSWORD_FIELD);
		}
		if(changePasswordRequest.getOldPassword()==null || "".equals(changePasswordRequest.getOldPassword()))
		{
			errorMsgList.add(HPPClinicAdminConstants.OLD_PASSWORD_FIELD);
		}
		else if(changePasswordRequest.getOldPassword().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.OLD_PASSWORD_FIELD);
		}
		if(changePasswordRequest.getToken()==null || "".equals(changePasswordRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(changePasswordRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CHANGE_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CHANGE_PASSWORD_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CHANGE_PASSWORD_FIELD_VALIDATION);
		return errorMsgList;
	}

	
	/**
	 * This method is used to perform verifyUserLoginName service field validations
	 * @param verifyUserLoginName
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> verifyUserLoginNameFieldValidations(VerifyUserLoginNameRequest verifyUserLoginNameRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(verifyUserLoginNameRequest.getUserLoginName()==null || "".equals(verifyUserLoginNameRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
    	else if(verifyUserLoginNameRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.VERIFY_USER_LOGIN_NAME_FIELD_VALIDATION);
		return errorMsgList;
	}

	
	/**
	 * This method is used to perform userSignUpValidate service field validations
	 * @param userSignUpValidateRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userSignUpValidateFieldValidations(UserSignUpValidateRequest userSignUpValidateRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(userSignUpValidateRequest.getClinicUserId()==null || "".equals(userSignUpValidateRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
		}
    	else if(userSignUpValidateRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
    	}
    	if(userSignUpValidateRequest.getOrganisationId()==null || "".equals(userSignUpValidateRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(userSignUpValidateRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform userSignUpValidate BusinessValidations.
	 * @param userSignUpValidateRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userSignUpValidateBusinessValidations(UserSignUpValidateRequest userSignUpValidateRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(userSignUpValidateRequest.getClinicUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(userSignUpValidateRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_SIGN_UP_VALIDATE_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform otpVerification BusinessValidations.
	 * @param otpVerificationRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> otpVerificationBusinessValidations(OtpVerificationRequest otpVerificationRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.OTP_VERIFICATION_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(otpVerificationRequest.getClinicUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(otpVerificationRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			try
			{
				Long.parseLong(otpVerificationRequest.getOtpCode());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.OTP_CODE_FIELD);
			}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.OTP_VERIFICATION_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.OTP_VERIFICATION_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.OTP_VERIFICATION_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform userSignUpValidate service field validations
	 * @param userSignUpValidateRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> otpVerificationFieldValidations(OtpVerificationRequest otpVerificationRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.OTP_VERIFICATION_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(otpVerificationRequest.getClinicUserId()==null || "".equals(otpVerificationRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
		}
    	else if(otpVerificationRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
    	}
    	if(otpVerificationRequest.getOrganisationId()==null || "".equals(otpVerificationRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(otpVerificationRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
    	if(otpVerificationRequest.getOtpCode()==null || "".equals(otpVerificationRequest.getOtpCode()))
		{
			errorMsgList.add(HPPClinicAdminConstants.OTP_CODE_FIELD);
		}
    	else if(otpVerificationRequest.getOtpCode().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.OTP_CODE_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.OTP_VERIFICATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.OTP_VERIFICATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.OTP_VERIFICATION_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform verifyUserLoginName service field validations
	 * @param verifyUserLoginName
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> resendSmsFieldValidations(ResendSMSRequest resendSMSRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RESEND_SMS_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(resendSMSRequest.getMobileNumber()==null || "".equals(resendSMSRequest.getMobileNumber()))
		{
			errorMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
		}
    	else if(resendSMSRequest.getMobileNumber().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
    	}
    	if(resendSMSRequest.getClinicUserId()==null || "".equals(resendSMSRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
		}
    	else if(resendSMSRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
    	}
    	if(resendSMSRequest.getOrganisationId()==null || "".equals(resendSMSRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(resendSMSRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.RESEND_SMS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RESEND_SMS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RESEND_SMS_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform resendSms BusinessValidations.
	 * @param resendSMSRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> resendSmsBusinessValidations(ResendSMSRequest resendSMSRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RESEND_SMS_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(resendSMSRequest.getClinicUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(resendSMSRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			try
			{
				Long.parseLong(resendSMSRequest.getMobileNumber());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.MOBILE_NUM_FIELD);
				
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.RESEND_SMS_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RESEND_SMS_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RESEND_SMS_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform verifyUserLoginName service field validations
	 * @param verifyUserLoginName
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserBasicDetailsFieldValidations(GetUserBasicDetailsRequest getUserBasicDetailsRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_BASIC_DETAILS_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(getUserBasicDetailsRequest.getToken()==null || "".equals(getUserBasicDetailsRequest.getToken()))
    	{
    		errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
    	}
    	else if(getUserBasicDetailsRequest.getToken().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
    	}
    	if(getUserBasicDetailsRequest.getUserLoginName()==null || "".equals(getUserBasicDetailsRequest.getUserLoginName()))
    	{
    		errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
    	}
    	else if(getUserBasicDetailsRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
    	}
    	if(getUserBasicDetailsRequest.getClinicUserId()==null || "".equals(getUserBasicDetailsRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
		}
    	else if(getUserBasicDetailsRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID);
    	}
    	if(getUserBasicDetailsRequest.getOrganisationId()==null || "".equals(getUserBasicDetailsRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(getUserBasicDetailsRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.RESEND_SMS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.GET_USER_BASIC_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_BASIC_DETAILS_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform verifyUserLoginName service field validations
	 * @param verifyUserLoginName
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserBasicDetailsBusinessValidations(GetUserBasicDetailsRequest getUserBasicDetailsRequest)throws HPPApplicationException
	{

		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_BUSS_VALIDATION);
		
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			Long.parseLong(getUserBasicDetailsRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			
		}
		try
		{
			
			Long.parseLong(getUserBasicDetailsRequest.getClinicUserId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
			
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_DETAILS_BUSS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform userAssocToClinic service field validations
	 * @param forgotPasswordRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userAssocToClinicFieldValidations(UserAssocToClinicRequest userAssocToClinicRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
       try
	   {
    	errorMsgList = new ArrayList<String>();
    	if(userAssocToClinicRequest.getUserLoginName()==null || "".equals(userAssocToClinicRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
    	else if(userAssocToClinicRequest.getUserLoginName().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
    	}
    	if(userAssocToClinicRequest.getToken()==null || "".equals(userAssocToClinicRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
    	else if(userAssocToClinicRequest.getToken().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
    	}
    	if(userAssocToClinicRequest.getClinicUserId()==null || "".equals(userAssocToClinicRequest.getClinicUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
		}
    	else if(userAssocToClinicRequest.getClinicUserId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
    	}
    	if(userAssocToClinicRequest.getOrganisationId()==null || "".equals(userAssocToClinicRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
    	else if(userAssocToClinicRequest.getOrganisationId().trim().isEmpty())
    	{
    		errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
    	}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform userAssocToClinic BusinessValidations.
	 * @param userAssocToClinicRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> userAssocToClinicBusinessValidations(UserAssocToClinicRequest userAssocToClinicRequest)throws HPPApplicationException
	{
        logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_BUSS_VALIDATION);
		
		List<String> formatMsgList = null;
		try
		{
			formatMsgList = new ArrayList<String>();
			try
			{
				Long.parseLong(userAssocToClinicRequest.getClinicUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.USER_ID_FIELD);
			}
			try
			{
				Long.parseLong(userAssocToClinicRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
				
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.USER_ASSOC_TO_CLINIC_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform getUserConsultationTabDetailsValidations FieldValidations.
	 * @param getUserDetailsRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserConsultationTabDetailsValidations(UpdateUserConsultationTabDetailsRequest updateUserConsultationTabDetailsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" getUserConsultationTabDetailsValidations");
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(updateUserConsultationTabDetailsRequest.getToken()==null || "".equals(updateUserConsultationTabDetailsRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(updateUserConsultationTabDetailsRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(updateUserConsultationTabDetailsRequest.getClinicUserId()==null || "".equals(updateUserConsultationTabDetailsRequest.getClinicUserId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
			}
			else if(updateUserConsultationTabDetailsRequest.getClinicUserId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.CLINIC_USER_ID_FIELD);
			}
			if(updateUserConsultationTabDetailsRequest.getAdminUserLoginName()==null || "".equals(updateUserConsultationTabDetailsRequest.getAdminUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			else if(updateUserConsultationTabDetailsRequest.getAdminUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ADMIN_USR_LOGIN_FIELD);
			}
			if(updateUserConsultationTabDetailsRequest.getOrganisationId()==null || "".equals(updateUserConsultationTabDetailsRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(updateUserConsultationTabDetailsRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			List cons_details_list=updateUserConsultationTabDetailsRequest.getConsultationTabDetails();
			if(cons_details_list==null)
			{
				errorMsgList.add("Consultation Tab Details");
			}
			
			
		}catch(Exception e)
		{
			logger.error("getUserConsultationTabDetailsValidations ::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, "An error occured while validating input fields of getUserConsultationTabDetailsValidations service."+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" getUserConsultationTabDetailsValidations ");
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getUserConsultationTabDetailsBusinessValidations BusinessValidations.
	 * @param getUserDetailsRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserConsultationTabDetailsBusinessValidations(UpdateUserConsultationTabDetailsRequest updateUserConsultationTabDetailsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" getUserConsultationTabDetailsBusinessValidations");
		
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			Long.parseLong(updateUserConsultationTabDetailsRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			
		}
		catch(Exception e)
		{
			logger.error("getUserConsultationTabDetailsBusinessValidations ::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, " An error occured while validating input fields of getUserConsultationTabDetailsValidations service."+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" getUserConsultationTabDetailsBusinessValidations");
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform getUserConsultationTabDetailsBusinessValidations BusinessValidations.
	 * @param getUserDetailsRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> isValidUserConsultationTabDetails(UpdateUserConsultationTabDetailsRequest updateUserConsultationTabDetailsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" isValidUserConsultationTabDetails");
		
		List<String> isConsultationValidMsgList = new ArrayList<String>();
		try
		{
			
			 List<String> consultationTabDetails=updateUserConsultationTabDetailsRequest.getConsultationTabDetails();
			 if(consultationTabDetails!=null && consultationTabDetails.size()>0)
			 {
				 List list=new ArrayList<String>();
				 list.add(HPPClinicAdminConstants.PRE_EXAMINATION);
				 list.add(HPPClinicAdminConstants.EXAMINATION);
				 list.add(HPPClinicAdminConstants.DIAGNOSIS);
				 list.add(HPPClinicAdminConstants.PRESCRIPTION);
				 list.add(HPPClinicAdminConstants.FOLLOW_UP_VISIT);
				 list.add(HPPClinicAdminConstants.REFERRAL);
				 list.add(HPPClinicAdminConstants.SPECIFY_TEST_SCAN);
				 list.add(HPPClinicAdminConstants.RECOMMEND_HOME_MONITORING);
				 list.add(HPPClinicAdminConstants.VITALS_SYMPTOMS);
				 list.add(HPPClinicAdminConstants.IN_HOUSE_TESTING_AND_LAB_RESULTS);
				 
				 int actual_size=consultationTabDetails.size();
				 
				 
				 consultationTabDetails.retainAll(list);
				 int new_size=consultationTabDetails.size();
				 System.out.println("HPPClinicsUserAdminServiceHelper.isValidUserConsultationTabDetails() -->"+consultationTabDetails);
				 System.out.println("HPPClinicsUserAdminServiceHelper.isValidUserConsultationTabDetails() actual_size -->"+actual_size+" new_size"+new_size);
				 if(actual_size!=new_size)
				 {
					 isConsultationValidMsgList.add("Not a valid Consultation Tab Name");
				 }
				 
			 }	 
		}
		
		catch(Exception e)
		{
			logger.error("isValidUserConsultationTabDetails ::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, " An error occured while validating input fields of isValidUserConsultationTabDetails service."+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" isValidUserConsultationTabDetails");
		return isConsultationValidMsgList;
	}
	
	public static List<String> acceptTermAndConditinValidations(TermAndConditionRequest request)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" acceptTermAndConditinValidations");
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(request.getToken()==null || "".equals(request.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(request.getToken().trim().equals(""))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(request.getClinicUserId()==null || "".equals(request.getClinicUserId()))
			{
				errorMsgList.add("clinicUserId");
			}
			else if(request.getClinicUserId().trim().equals(""))
			{
				errorMsgList.add("clinicUserId");
			}
			if(request.getAcceptTermAndCondition()==null || "".equals(request.getAcceptTermAndCondition()))
			{
				errorMsgList.add("acceptTermAndCondition");
			}
			else if(request.getAcceptTermAndCondition().trim().equals(""))
			{
				errorMsgList.add("acceptTermAndCondition");
			}
			else if(!"Y".equals(request.getAcceptTermAndCondition()) && !"N".equals(request.getAcceptTermAndCondition()))
			{
				throw new HPPApplicationException(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE, "Please provide valid input for acceptTermAndCondition (Y/N) ");
			}
			if(request.getUserLoginName()==null || "".equals(request.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(request.getUserLoginName().trim().equals(""))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			
			
		}
		catch(HPPApplicationException hae)
		{
			logger.error("acceptTermAndConditinValidations ::"+hae.getMessage());
			throw hae;
		}
		catch(Exception e)
		{
			logger.error("acceptTermAndConditinValidations ::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, "An error occured while validating input fields of acceptTermAndConditinValidations service."+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" acceptTermAndConditinValidations ");
		return errorMsgList;
	}
	
	public static List<String> updateCommunicationParamValidations(UpdateCommunicationRequest request)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" updateCommunicationParamValidations");
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(request.getToken()==null || "".equals(request.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(request.getToken().trim().equals(""))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(request.getUserLoginName()==null || "".equals(request.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(request.getUserLoginName().trim().equals(""))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(request.getOrganizationId()==null || "".equals(request.getOrganizationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORGANIZATION_ID);
			}
			else if(request.getOrganizationId().trim().equals(""))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORGANIZATION_ID);
			}
			else
			{
				try{
				Long.parseLong(request.getOrganizationId());
				}
				catch(NumberFormatException nfe)
				{
						throw new HPPApplicationException(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE, "Organization ID must be numerical..");		
				}
				
			}
			if(request.getClinicUserId()==null || "".equals(request.getClinicUserId()))
			{
				errorMsgList.add("clinicUserId");
			}
			else if(request.getClinicUserId().trim().equals(""))
			{
				errorMsgList.add("clinicUserId");
			}
			if(request.getReceiptUser()==null || "".equals(request.getReceiptUser()))
			{
				errorMsgList.add("RECEIPT USER");
			}	
			else if(request.getReceiptUser().trim().equals(""))
			{
				errorMsgList.add("RECEIPT USER");
			}
			else
			{
				if(!"PATIENT".equals(request.getReceiptUser()) && !"DOCTOR".equals(request.getReceiptUser()))
				{
					throw new HPPApplicationException(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE, "Please provide a valid input for RECEIPT USER ");
				}
			}
			if(request.getWorkflow()==null || "".equals(request.getWorkflow()))
			{
				errorMsgList.add("WORKFLOW");
			}
			else if(request.getWorkflow().trim().equals(""))
			{
				errorMsgList.add("WORKFLOW");
			}
				
			if(request.getCommunicationType()==null || "".equals(request.getCommunicationType()))
			{
				errorMsgList.add("COMMUNICATION TYPE");
			}
			else if(request.getCommunicationType().trim().equals(""))
			{
				errorMsgList.add("COMMUNICATION TYPE");
			}
			else
			{
				if(!"SMS".equals(request.getCommunicationType()) && !"EMAIL".equals(request.getCommunicationType()))
				{
					throw new HPPApplicationException(HPPClinicAdminConstants.CUSTOM_EXCEPTION_CODE, "Please provide a valid input for Communication Type");
				}
			}	
			if("SMS".equalsIgnoreCase(request.getCommunicationType()))
			{
					if(request.getSmsMessage()==null || "".equals(request.getSmsMessage()))
					{
						errorMsgList.add("SMS MESSAGE");
					}
					else if(request.getSmsMessage().trim().equals(""))
					{
						errorMsgList.add("SMS MESSAGE");
					}	
			}
			if("EMAIL".equalsIgnoreCase(request.getCommunicationType()))
			{
				if(request.getMailSubject()==null || "".equals(request.getMailSubject()))
				{
					errorMsgList.add("MAIL SUBJECT");
				}
				else if(request.getMailSubject().trim().equals(""))
				{
					errorMsgList.add("MAIL SUBJECT");
				}	
				if(request.getMailBody()==null || "".equals(request.getMailBody()))
				{
					errorMsgList.add("MAIL BODY");
				}
				else if(request.getMailBody().trim().equals(""))
				{
					errorMsgList.add("MAIL BODY");
				}			
				
			}		
			
		}
		
		catch(HPPApplicationException hae)
		{
			logger.error("updateCommunicationParamValidations ::"+hae.getMessage());
			throw hae;
		}
		catch(Exception e)
		{
			logger.error("updateCommunicationParamValidations ::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, "An error occured while validating input fields of updateCommunicationParamValidations service."+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" updateCommunicationParamValidations ");
		return errorMsgList;
	}
}
