/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LocationMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SiteMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateTariffRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * This class is used to handle the Tariff Master Module field and Business validations.
 */
public final class HPPClinicsTariffMasterServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsTariffMasterServiceHelper.class);
	private HPPClinicsTariffMasterServiceHelper()
	{
		
	}

	
	/**
	 * This method is used to perform locationMaster FieldValidations.
	 * @param locationMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> locationMasterFieldValidations(LocationMasterRequest locationMasterRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.LOCATION_MASTER_FIELD_VALIDATION);
		List<String> errorMsgList = null;
		try
		{
	    errorMsgList = new ArrayList<String>();
		if(locationMasterRequest.getToken()==null || "".equals(locationMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(locationMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(locationMasterRequest.getUserLoginName()==null || "".equals(locationMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(locationMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		} 
		if(locationMasterRequest.getOrganisationId()==null || "".equals(locationMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(locationMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.LOCATION_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.LOCATION_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.LOCATION_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform locationMaster BusinessValidations.
	 * @param locationMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> locationMasterBusinessValidations(LocationMasterRequest locationMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.LOCATION_MASTER_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		   
			
				Long.parseLong(locationMasterRequest.getOrganisationId());
	   }	
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}  
	   catch(Exception e)
	   {
		   logger.error(HPPClinicAdminConstants.LOCATION_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		   throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.LOCATION_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
	   }
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.LOCATION_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform siteMaster FieldValidations.
	 * @param siteMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> siteMasterFieldValidations(SiteMasterRequest siteMasterRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SITE_MASTER_FIELD_VALIDATION);
		List<String> errorMsgList = null;
		try
		{
	    errorMsgList = new ArrayList<String>();
		if(siteMasterRequest.getToken()==null || "".equals(siteMasterRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(siteMasterRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(siteMasterRequest.getUserLoginName()==null || "".equals(siteMasterRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(siteMasterRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		} 
		if(siteMasterRequest.getOrganisationId()==null || "".equals(siteMasterRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(siteMasterRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.SITE_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SITE_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SITE_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform siteMaster BusinessValidations.
	 * @param siteMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> siteMasterBusinessValidations(SiteMasterRequest siteMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SITE_MASTER_BUSS_VALIDATION);
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		  
			
				Long.parseLong(siteMasterRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}   
	   catch(Exception e)
	   {
		   logger.error(HPPClinicAdminConstants.SITE_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		   throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SITE_MASTER_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
	   }
	   logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SITE_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform createTariff FieldValidations.
	 * @param createTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createTariffFieldValidations(CreateTariffRequest createTariffRequest)throws HPPApplicationException
	{
	  logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_TARIFF_FIELD_VALIDATION);
	  List<String> errorMsgList = null;
	  try
	  {
		errorMsgList = new ArrayList<String>();
		if(createTariffRequest.getToken()==null || "".equals(createTariffRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(createTariffRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(createTariffRequest.getUserLoginName()==null || "".equals(createTariffRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(createTariffRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(createTariffRequest.getAdminUserId()==null || "".equals(createTariffRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(createTariffRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(createTariffRequest.getTariff()==null || "".equals(createTariffRequest.getTariff().toString()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TARIFF_OBJECT_FIELD);
		}
		else
		{
			if(createTariffRequest.getTariff().getTariffAmount()==null || "".equals(createTariffRequest.getTariff().getTariffAmount()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			else if(createTariffRequest.getTariff().getTariffAmount().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			if(createTariffRequest.getTariff().getServiceId()==null || "".equals(createTariffRequest.getTariff().getServiceId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			else if(createTariffRequest.getTariff().getServiceId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			if(createTariffRequest.getTariff().getServiceName()==null || "".equals(createTariffRequest.getTariff().getServiceName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			else if(createTariffRequest.getTariff().getServiceName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			if(createTariffRequest.getTariff().getSiteId()==null || "".equals(createTariffRequest.getTariff().getSiteId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			else if(createTariffRequest.getTariff().getSiteId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			if(createTariffRequest.getTariff().getLocationId()==null || "".equals(createTariffRequest.getTariff().getLocationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			else if(createTariffRequest.getTariff().getLocationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			if(createTariffRequest.getTariff().getOrganisationId()==null || "".equals(createTariffRequest.getTariff().getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(createTariffRequest.getTariff().getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}
	  }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.CREATE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
	  
		
	  logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_TARIFF_FIELD_VALIDATION);
	  return errorMsgList;
	}
	
	/**
	 * This method is used to perform createTariff BusinessValidations.
	 * @param createTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> createTariffBusinessValidations(CreateTariffRequest createTariffRequest)throws HPPApplicationException
	{

		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.CREATE_TARIFF_BUSS_VALIDATION);
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			try
			{
				Long.parseLong(createTariffRequest.getAdminUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
			}
			try
			{
				new BigDecimal(createTariffRequest.getTariff().getTariffAmount());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			try
			{
				Long.parseLong(createTariffRequest.getTariff().getServiceId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SERVICE_ID_FIELD);
			}
			try
			{
				Long.parseLong(createTariffRequest.getTariff().getSiteId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.SITE_ID_FIELD);
			}
			try
			{
				Long.parseLong(createTariffRequest.getTariff().getLocationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			try
			{
				Long.parseLong(createTariffRequest.getTariff().getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CREATE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CREATE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.CREATE_TARIFF_BUSS_VALIDATION);
		   return formatMsgList;
	}
	
	/**
	 * This method is used to perform updateTariff FieldValidations.
	 * @param updateTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateTariffFieldValidations(UpdateTariffRequest updateTariffRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_TARIFF_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(updateTariffRequest.getToken()==null || "".equals(updateTariffRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(updateTariffRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(updateTariffRequest.getUserLoginName()==null || "".equals(updateTariffRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(updateTariffRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(updateTariffRequest.getAdminUserId()==null || "".equals(updateTariffRequest.getAdminUserId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		else if(updateTariffRequest.getAdminUserId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
		}
		if(updateTariffRequest.getOrganisationId()==null || "".equals(updateTariffRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(updateTariffRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(updateTariffRequest.getTariff()==null || "".equals(updateTariffRequest.getTariff().toString()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TARIFF_OBJECT_FIELD);
		}
		else
		{
			if(updateTariffRequest.getTariff().getTariffId()==null || "".equals(updateTariffRequest.getTariff().getTariffId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_ID_FIELD);
			}
			else if(updateTariffRequest.getTariff().getTariffId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_ID_FIELD);
			}
			if(updateTariffRequest.getTariff().getTariffAmount()==null || "".equals(updateTariffRequest.getTariff().getTariffAmount()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			else if(updateTariffRequest.getTariff().getTariffAmount().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			if(updateTariffRequest.getTariff().getServiceName()==null || "".equals(updateTariffRequest.getTariff().getServiceName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
			else if(updateTariffRequest.getTariff().getServiceName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SERVICE_NAME_FIELD);
			}
		}
	   }catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.UPDATE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
	   
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_TARIFF_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform updateTariff BusinessValidations.
	 * @param updateTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> updateTariffBusinessValidations(UpdateTariffRequest updateTariffRequest)throws HPPApplicationException
	{

		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.UPDATE_TARIFF_BUSS_VALIDATION);
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			try
			{
				Long.parseLong(updateTariffRequest.getAdminUserId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ADMINUSERID_FIELD);
			}
			try
			{
				new BigDecimal(updateTariffRequest.getTariff().getTariffAmount());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_AMOUNT_FIELD);
			}
			try
			{
				Long.parseLong(updateTariffRequest.getTariff().getTariffId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.TARIFF_ID_FIELD);
			}
			try
			{
				Long.parseLong(updateTariffRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.UPDATE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.UPDATE_TARIFF_BUSS_VALIDATION);
		   return formatMsgList;
	}
	
	/**
	 * This method is used to perform retrieveTariff FieldValidations.
	 * @param getTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveTariffFieldValidations(GetTariffRequest getTariffRequest)throws HPPApplicationException
	{
	  logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_TARIFF_FIELD_VALIDATION);
	  List<String> errorMsgList = null;
	  
	  try
	  {
		errorMsgList = new ArrayList<String>();
		if(getTariffRequest.getToken()==null || "".equals(getTariffRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(getTariffRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(getTariffRequest.getUserLoginName()==null || "".equals(getTariffRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(getTariffRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(getTariffRequest.getLocationId()==null || "".equals(getTariffRequest.getLocationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
		else if(getTariffRequest.getLocationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
		}
		if(getTariffRequest.getOrganisationId()==null || "".equals(getTariffRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(getTariffRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(getTariffRequest.getOrganisationId()==null || "".equals(getTariffRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(getTariffRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	  }
	  catch(Exception e)
		{
		    logger.error(HPPClinicAdminConstants.RETRIEVE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_TARIFF_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
	   
	  logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_TARIFF_FIELD_VALIDATION);
	   return errorMsgList;
	}
	
	/**
	 * This method is used to perform retrieveTariff BusinessValidations.
	 * @param getTariffRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> retrieveTariffBusinessValidations(GetTariffRequest getTariffRequest)throws HPPApplicationException
	{
		List<String> formatMsgList = new ArrayList<String>();
		
		try
		{
			try
			{
				Integer.parseInt(getTariffRequest.getLocationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.LOCATIONID_FIELD);
			}
			try
			{
				Integer.parseInt(getTariffRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			try
			{
				if(getTariffRequest.getStartRow()!=null && !("".equals(getTariffRequest.getStartRow())) && !getTariffRequest.getStartRow().trim().isEmpty())
				{
					Integer.parseInt(getTariffRequest.getStartRow());
				}
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.STARTROW_FIELD);
			}
			try
			{
				if(getTariffRequest.getInterval()!=null && !("".equals(getTariffRequest.getInterval())) && !getTariffRequest.getInterval().trim().isEmpty())
				{
					Integer.parseInt(getTariffRequest.getInterval());
				}
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.INTERVAL_FIELD);
				
			}
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.RETRIEVE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.RETRIEVE_TARIFF_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		   return formatMsgList;
	}
	
}
