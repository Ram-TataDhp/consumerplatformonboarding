/**
 * 
 */
package com.tcs.hpp.clinics.services.clinicadmin.convertor;



import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.CreateUserReqDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.SearchUserDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UpdateUserRequestDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UserDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.Parameter;
import com.tcs.hpp.clinics.clinicadmin.persistence.Service;
import com.tcs.hpp.clinics.clinicadmin.persistence.Tariff;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.CreateUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.JaxbUser;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchUserResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateServiceRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateSystemParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateTariffRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateUserRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;


/**
 * @author 219331 (Nagesh Chintala)
 * Date:20/02/2015
 * This class is used for convert JAXB to corresponding JPA entities and vice versa..
 */
public class HPPClinicAdminJAXBConvertor {
	private static Logger logger = Logger.getLogger(HPPClinicAdminJAXBConvertor.class);
	
	public HPPClinicAdminJAXBConvertor()
	{
		
	}
	/**
	 * This method is used to convert CreateUserRequest JAXB object to ClinicUser entity...
	 * @param createUserRequest
	 * @return ClinicUser
	 */
	 public CreateUserReqDTO convertFromJAXBtoUserEntity(CreateUserRequest createUserRequest)throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.CONVERT_JAXB_USER_ENTITY);
		
		 JaxbUser jaxbUser = new JaxbUser();
		 
		 CreateUserReqDTO userReq = new CreateUserReqDTO();
		 UserDTO user = userReq.getUserDTO();
		
		 try
		 {
			 
			 if(createUserRequest!=null)
			 {
				 CreateUserRequest.User createUser = createUserRequest.getUser();
				 
					BeanUtils.copyProperties(jaxbUser, createUser);
			
			 }
			 
			 userReq.setAdminUserId(Long.parseLong(createUserRequest.getAdminUserId()));
			 userReq.setOrganisationId(Long.parseLong(jaxbUser.getOrganisationId()));
			 userReq.setOrganisationName(jaxbUser.getOrganisationName());
			 userReq.setSiteId(Long.parseLong(createUserRequest.getSiteId()));
			 userReq.setIsDuplicateCheckReqd(createUserRequest.getIsDuplicateCheckRequired());
			 userReq.setIsUserMappingReqd(createUserRequest.getIsUserMappingRequired());
			 user.setAddress(jaxbUser.getAddress());
			 user.setIsactive(jaxbUser.getIsActive());
			 user.setEmailId(jaxbUser.getEmail());
			 user.setGender(jaxbUser.getGender());
			 user.setMedicalRegNo(jaxbUser.getMedicalRegNo());
			 user.setMobileNo(jaxbUser.getMobileNumber());
			 user.setBloodGroup(jaxbUser.getBloodGroup());
			 user.setIsVirtualClinic(jaxbUser.getIsVirtualClinic());			 
			 //logic to Capitalise first letter of UserName
			 String alphaNumPattern= "^[a-zA-Z0-9\\s\\.]*$";
			 String  userName = jaxbUser.getUserName();
			 userName = userName .trim();
			 if(!userName.equals("") && userName.matches(alphaNumPattern)) {
				 	String[] arr = userName.split("\\s+");
				 	StringBuffer sb = new StringBuffer();
				 	for (int i = 0; i < arr.length; i++){
				 		sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");				 		
				 	}          
				 	userName = sb.toString().trim();
			 }
			 	 
			 user.setUserName(userName);
			 user.setUserPhoto(jaxbUser.getUserPhoto());
			 user.setTitle(jaxbUser.getTitle());
			 user.setSignature(jaxbUser.getSignature());
			 
			 SimpleDateFormat ft = new SimpleDateFormat (HPPClinicAdminConstants.DATE_FORMAT); 
			 Date dob = null; 
			 dob = ft.parse(jaxbUser.getDOB());
			      
			 user.setUserDob(dob);
			 
			 
			 if(HPPClinicAdminConstants.YES.equalsIgnoreCase(createUserRequest.getIsUserMappingRequired()))//In case of update user,need to set UserId value...
			 {
				 user.setClinicUserId(Long.parseLong(jaxbUser.getUserId()));
			 }
			
			 user.setRoleType(jaxbUser.getRoleType());
			 user.setSpecialty(jaxbUser.getSpeciality());
			 
			 userReq.setUserDTO(user);
			
			
			 Iterator<String> roleTagItr = null;
				Iterator<String> locationTagItr =null;
				
					roleTagItr = createUserRequest.getRoleId().iterator();
					locationTagItr =  createUserRequest.getLocationId().iterator();
					List<Long> roleIdList = new ArrayList<Long>();
					List<Long> locationIdList = new ArrayList<Long>();
				while(roleTagItr.hasNext())
				{
					long roleId = Long.parseLong(roleTagItr.next());
					roleIdList.add(roleId);
				}
				while(locationTagItr.hasNext())
				{
					long locationId = Long.parseLong(locationTagItr.next());
					locationIdList.add(locationId);
				} 
				userReq.setRoleIdList(roleIdList);
				userReq.setLocationIdList(locationIdList);
		 }catch(ParseException e)
		 {
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_JAXB_TO_ENTITY_CONVERSION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		 catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.CLINICADMIN_JAXB_TO_ENTITY_CONVERSION_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_JAXB_TO_ENTITY_CONVERSION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		
				 
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.CONVERT_JAXB_USER_ENTITY);
		return userReq;
		 
	 }
	 /**
		 * This method is used to convert CreateUserRequest JAXB object to ClinicUser entity...
		 * @param createUserRequest
		 * @return ClinicUser
		 */
		 public UpdateUserRequestDTO convertFromJAXBtoUpdateUserReqDTO(UpdateUserRequest updateUserRequest)throws HPPApplicationException
		 {
			 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.CONVERT_JAXB_CLINIC_USER_ENTITY);
			 UpdateUserRequestDTO updateUserRequestDTO = null;
			 try
			 {
				 updateUserRequestDTO = new UpdateUserRequestDTO();
				 BeanUtils.copyProperties(updateUserRequestDTO, updateUserRequest);
				 Iterator<String> roleIdItr = updateUserRequest.getRoleId().listIterator();
				 List<Long> roleIdList = new ArrayList<Long>();
				 while(roleIdItr.hasNext())
				 {
					 roleIdList.add(Long.parseLong(roleIdItr.next()));
				 }
				 updateUserRequestDTO.getRoleId().clear();
				 updateUserRequestDTO.getRoleId().addAll(roleIdList);
			 }
			 catch(Exception e)
			 {
				 logger.error(HPPClinicAdminConstants.CLINICADMIN_JAXB_TO_ENTITY_CONVERSION_EXCEPTION_MSG+"::"+e.getMessage());
				 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_JAXB_TO_ENTITY_CONVERSION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
			 }
			 
			 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.CONVERT_JAXB_CLINIC_USER_ENTITY);
			 return updateUserRequestDTO;
		 }
	/**
	 * This method is used to convert CreateServiceRequest JAXB object to Service entity...
	 * @param createServiceRequest
	 * @return service
	 * @throws HPPApplicationException
	 */
	public Service convertFromJAXBtoServiceEntity(CreateServiceRequest createServiceRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_CREATE_SERVICE_ENTITY);
		 Service service = null;
		 
		 CreateServiceRequest.Service jaxbService= null;
         try
         {
        	 service =new Service();
        	 jaxbService=createServiceRequest.getService();
			 service.setServiceName(jaxbService.getServiceName());
			 service.setServiceClassificationId(Long.parseLong(jaxbService.getClassificationId()));
			 service.setServiceType(jaxbService.getServiceType());
			 service.setIsactive(jaxbService.getStatus());
			 service.setIschargable(jaxbService.getChargable());
			 service.setOrganizationId(Long.parseLong(jaxbService.getOrganisationId()));
			 service.setCreatedUserId(Long.parseLong(createServiceRequest.getAdminUserId()));
			 service.setIsresultApplicable(jaxbService.getResultApplicable());
			 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
			 service.setCreatedTimestamp(currentTimestamp);
			 service.setEffectiveFromDate(currentTimestamp);
         }catch(Exception e)
		 {
        	 logger.error(HPPClinicAdminConstants.JAXB_CREATE_SERVICE_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.JAXB_CREATE_SERVICE_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
         
		 
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_CREATE_SERVICE_ENTITY);
		 return service;
		 
	 }

	/**
	 * This method is used to convert UpdateServiceRequest JAXB object to Service entity.
	 * @param updateServiceRequest
	 * @return service
	 * @throws HPPApplicationException
	 */
	public Service convertFromJAXBtoUpdateServiceEntity(UpdateServiceRequest updateServiceRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_ENTITY);
		 Service service =new Service();
		 
		 UpdateServiceRequest.Service jaxbService=updateServiceRequest.getService();
		  
		 try
		 {
		 	 service.setServiceId(Long.parseLong(jaxbService.getServiceId()));
			 service.setServiceName(jaxbService.getServiceName());
			 service.setServiceType(jaxbService.getServiceType());
			 service.setIsactive(jaxbService.getStatus());
			 service.setIschargable(jaxbService.getChargable());
			 service.setOrganizationId(Long.parseLong(jaxbService.getOrganisationId()));
			 service.setModifiedUserId(Long.parseLong(updateServiceRequest.getAdminUserId()));
			 service.setIsresultApplicable(jaxbService.getResultApplicable());
			 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
			 service.setModifiedTimestamp(currentTimestamp);
			 service.setEffectiveFromDate(currentTimestamp);
		 }catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_ENTITY);
		 return service;
	 }
	/**
	 * This method is used to convert UpdateServiceRequest JAXB object to Service entity.
	 * @param updateServiceRequest
	 * @return Tariff
	 * @throws HPPApplicationException
	 */
	public Tariff convertFromJAXBtoUpdateOrCreateTariffEntity(UpdateServiceRequest updateServiceRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_TARIFF_ENTITY);
		 Tariff tariff =new Tariff();
		 
		 UpdateServiceRequest.Service jaxbService=updateServiceRequest.getService();
		  
		 try
		 {
			 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
			 if(updateServiceRequest.getService().getTariffId()!=null && !updateServiceRequest.getService().getTariffId().trim().isEmpty())
			 {
				 tariff.setTariffId(Long.parseLong(jaxbService.getTariffId()));
				 tariff.setModifiedUserId(Long.parseLong(updateServiceRequest.getAdminUserId()));
				 tariff.setModifiedTimestamp(currentTimestamp);
			 }
			 else
			 {
				 tariff.setCreatedUserId(Long.parseLong(updateServiceRequest.getAdminUserId()));
				 tariff.setCreatedTimestamp(currentTimestamp);
			 }
			 tariff.setServiceId(Long.parseLong(updateServiceRequest.getService().getServiceId()));
			 tariff.setTariffAmount(new BigDecimal(updateServiceRequest.getService().getTariff()));
			 tariff.setIsactive(jaxbService.getStatus());
			 
			 tariff.setOrganizationId(Long.parseLong(jaxbService.getOrganisationId()));
			
			
		 }catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_TARIFF_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_TARIFF_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_SERVICE_TARIFF_ENTITY);
		 return tariff;
	 }
	 
	/**
	 * This method is used to convert CreateTariffRequest JAXB object to Tariff entity.
	 * @param createTariffRequest
	 * @return tariff
	 * @throws HPPApplicationException
	 */
	public Tariff convertFromJAXBtoTariffEntity(CreateTariffRequest createTariffRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_CREATE_TARIFF_ENTITY);
		 Tariff tariff =new Tariff();
		 
		 CreateTariffRequest.Tariff jaxbService=createTariffRequest.getTariff();
         try
         {
		     tariff.setCreatedUserId(Long.parseLong(createTariffRequest.getAdminUserId()));
		     tariff.setIsactive(HPPClinicAdminConstants.ACTIVE);
		     tariff.setLocationId(Long.parseLong(jaxbService.getLocationId()));
		     tariff.setOrganizationId(Long.parseLong(jaxbService.getOrganisationId()));
		     tariff.setServiceId(Long.parseLong(jaxbService.getServiceId()));
		     tariff.setSiteId(Long.parseLong(jaxbService.getSiteId()));
		     tariff.setTariffAmount(new BigDecimal(jaxbService.getTariffAmount()));
			 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
			 tariff.setCreatedTimestamp(currentTimestamp);
         }catch(Exception e)
		 {
        	 logger.error(HPPClinicAdminConstants.JAXB_TARIFF_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.JAXB_TARIFF_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
	     logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_CREATE_TARIFF_ENTITY);
		 return tariff;
		 
	 }

	/**
	 * This method is used to convert UpdateTariffRequest JAXB object to Tariff entity.
	 * @param updateTariffRequest
	 * @return tariff
	 * @throws HPPApplicationException
	 */
	public Tariff convertFromJAXBtoUpdateTariffEntity(UpdateTariffRequest updateTariffRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_TARIFF_ENTITY);
		 Tariff tariff =new Tariff();
		 
		 UpdateTariffRequest.Tariff jaxbService=updateTariffRequest.getTariff();
		 try
		 {
		 tariff.setTariffId(Long.parseLong(jaxbService.getTariffId()));
	     tariff.setModifiedUserId(Long.parseLong(updateTariffRequest.getAdminUserId()));
	     tariff.setOrganizationId(Long.parseLong(updateTariffRequest.getOrganisationId()));
	     tariff.setTariffAmount(new BigDecimal(jaxbService.getTariffAmount()));
		 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
		 tariff.setModifiedTimestamp(currentTimestamp);
		 }catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.JAXB_UPDATE_TARIFF_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.JAXB_UPDATE_TARIFF_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_UPDATE_TARIFF_ENTITY);
		 return tariff;
	 }
	 /**
	  * This method is used to convert List<SearchUserDTO> to SearchUserResponse JAXB object.
	  * @param searchUserDTOList
	  * @return SearchUserResponse
	  * @throws HPPApplicationException
	  */
	 public SearchUserResponse convertSearchUserDTOtoJAXB(List<SearchUserDTO> searchUserDTOList)throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.JAXB_SEARCH_USER_DTO);
		 SearchUserResponse oSearchUserResponse = new SearchUserResponse();
		 SearchUserResponse.User jaxbUser= null;
		 try
		 {
			
			
			 Iterator<SearchUserDTO> searchUserDTOItr = searchUserDTOList.iterator();
			 while(searchUserDTOItr.hasNext())
			 {
				 SearchUserDTO oSearchUserDTO = searchUserDTOItr.next();
				 jaxbUser= new SearchUserResponse.User();
				 BeanUtils.copyProperties(jaxbUser, oSearchUserDTO);
				 oSearchUserResponse.getUser().add(jaxbUser);
				 
			 }
			 
		 }catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.CLINICADMIN_SEARCHUSER_CONVERT_DTO_JAXB_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_SEARCHUSER_CONVERT_DTO_JAXB_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		 
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.JAXB_SEARCH_USER_DTO);
		 return oSearchUserResponse;
	 }
	 
	/**
	 * This method is used to convert UpdateSystemParamRequest JAXB object to Parameter entity.
	 * @param updateSystemParamRequest
	 * @return parameter
	 * @throws HPPApplicationException
	 */
	public Parameter convertFromJAXBtoUpdateSystemParamEntity(UpdateSystemParamRequest updateSystemParamRequest) throws HPPApplicationException
	 {
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" " +HPPClinicAdminConstants.JAXB_SYSTEM_PARAM_ENTITY);
		 Parameter parameter = null;
		 
		 UpdateSystemParamRequest.SystemParam jaxbSystemParam = null;
		 try
		 {
		 parameter =new Parameter();
		 jaxbSystemParam=updateSystemParamRequest.getSystemParam();
		 parameter.setParameterId(Long.parseLong(jaxbSystemParam.getParamId()));
		 parameter.setSystemParameterId(Long.parseLong(jaxbSystemParam.getSysParamNameId()));
		 parameter.setParameterValue(jaxbSystemParam.getParamValue());
		 if(jaxbSystemParam.getLevel().equalsIgnoreCase(HPPClinicAdminConstants.ORGANIZATION))
		 {
			 parameter.setOrganizationId(Long.parseLong(jaxbSystemParam.getLevelValueId()));
		 }
		 else if(jaxbSystemParam.getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LOCATION))
		 {
			 parameter.setLocationId(Long.parseLong(jaxbSystemParam.getLevelValueId()));
			 parameter.setSiteId(Long.parseLong(jaxbSystemParam.getSiteId()));
		 }
		 else if(jaxbSystemParam.getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LEVEL_SITE))
		 {
			 parameter.setSiteId(Long.parseLong(jaxbSystemParam.getSiteId()));
		 }
		 parameter.setOrganizationId(Long.parseLong(updateSystemParamRequest.getOrganisationId()));
		 parameter.setModifiedUserId(Long.parseLong(updateSystemParamRequest.getAdminUserId()));
		 java.sql.Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
		 parameter.setModifiedTimestamp(currentTimestamp);
		 }catch(Exception e)
		 {
			 logger.error(HPPClinicAdminConstants.UPDATESYSPARAM_CONVERT_JAXB_TO_ENTITY_EXCEPTION_MSG+"::"+e.getMessage());
			 throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.UPDATESYSPARAM_CONVERT_JAXB_TO_ENTITY_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		 }
		 
		 
		 logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" " +HPPClinicAdminConstants.JAXB_SYSTEM_PARAM_ENTITY);
		 return parameter;
	 }
	
}
