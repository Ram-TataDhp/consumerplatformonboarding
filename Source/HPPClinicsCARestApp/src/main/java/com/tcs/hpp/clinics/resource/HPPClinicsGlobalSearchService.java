/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;*/





import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.GlobalClinicServiceSearchDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.GlobalPatientSearchDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.GlobalUserSearchDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.Role;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSpecialityRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSpecialityResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSpecialityRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSpecialityResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalClinicServcieSearchRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalClinicServcieSearchResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalUserSearchRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalUserSearchResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OperationalRoleMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OperationalRoleMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchGlobalPatientsRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchGlobalPatientsResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisher;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Description: This class is used to handle all the services related to GlobalSearch Module.
 */

@RestController
@RequestMapping("/clinicAdministration")
public class HPPClinicsGlobalSearchService {
	public static final String CLASSNAME = "HPPClinicsGlobalSearchService";
	
	
	private static Logger logger=Logger.getLogger(HPPClinicsGlobalSearchService.class);
	public HPPClinicsGlobalSearchService()
	{
		 String trackId=null;
		 UUID idOne = UUID.randomUUID();
		 trackId=idOne.toString();
		 MDC.put(HPPClinicAdminConstants.TRACKING_ID_KEY, trackId);
	}
	
	/*
	 * Global Search Services starts here
	 *  
	 */
	/**
	 * This method is used to get all roles master data...
	 * @param operationalRoleMasterRequest
	 * @return operationalRoleMasterResponse
	 */

	@RequestMapping(value = "1.1/getOperationalRoleList", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getOperationalRoleList(@RequestBody OperationalRoleMasterRequest operationalRoleMasterRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_ROLES_MASTER);		
		List<Serializable> getMasterRoleList = null;
		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 OperationalRoleMasterResponse operationalRoleMasterResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			errorMsgList = HPPClinicsGlobalSearchServiceHelper.getOperationalRoleMasterFiledValidations(operationalRoleMasterRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList = HPPClinicsGlobalSearchServiceHelper.getOperationalRoleMasterBusinessValidations(operationalRoleMasterRequest);
			 if(formatMsgList.size()==0)
			 {
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(operationalRoleMasterRequest.getUserLoginName(),operationalRoleMasterRequest.getToken(),Long.parseLong(operationalRoleMasterRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						operationalRoleMasterResponse = new OperationalRoleMasterResponse();
						
						/*
						 * Getting Operational Role Master Data...
						 */
						BusinessComponentMap bcMapRoleMaster = HPPClinicAdminUtil.populateBCMForRolesMasterData(null,operationalRoleMasterRequest.getOrganisationId());
						
						businessProcessStartTime = System.currentTimeMillis();
						getMasterRoleList = delegate.execute(bcMapRoleMaster);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(getMasterRoleList!=null && getMasterRoleList.size()>0)
						{
							List<Role> roleList = (List<Role>) getMasterRoleList.get(0);
							for(Role role:roleList)
							{
								OperationalRoleMasterResponse.Role jaxbRole = new OperationalRoleMasterResponse.Role();
								jaxbRole.setRoleId(role.getRoleId().toString());
								jaxbRole.setRoleName(role.getRoleName());
								operationalRoleMasterResponse.getRole().add(jaxbRole);
							}
						}
						
					
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			   }
			   else
				  //Fail in business validation
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(operationalRoleMasterRequest.getOrganisationId()!=null && operationalRoleMasterRequest.getOrganisationId()!="" && !operationalRoleMasterRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(operationalRoleMasterRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.GET_OPERATIONAL_ROLE_LIST_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_ROLES_MASTER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(operationalRoleMasterResponse)).build();
		return new ResponseEntity(operationalRoleMasterResponse,HttpStatus.OK);
		
	}
	/**
	 * This method is used to get all specialties data...
	 * @param getSpecialityRequest
	 * @return getSpecialityResponse
	 */

	@RequestMapping(value = "1.1/getSpecialty", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getSpecialtyMaster(@RequestBody GetSpecialityRequest getSpecialityRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER);		
		
		 List<Serializable> getSpecialtyMasterList = null;		
		 BusinessDelegate delegate = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 GetSpecialityResponse getSpecialityResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsGlobalSearchServiceHelper.getSpecialtyMasterFiledValidations(getSpecialityRequest);
			 if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList = HPPClinicsGlobalSearchServiceHelper.getSpecialtyMasterBusinessValidations(getSpecialityRequest);
			 if(formatMsgList.size()==0)
			 {
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(getSpecialityRequest.getUserLoginName(),getSpecialityRequest.getToken(),Long.parseLong(getSpecialityRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						getSpecialityResponse = new GetSpecialityResponse();
						
						/*
						 * Getting Operational Role Master Data...
						 */
						BusinessComponentMap bcMapRoleMaster = populateBCMForSpecialtyMasterData(getSpecialityRequest.getOrganisationId(),getSpecialityRequest.getIsClinicAdminUser());
						
						businessProcessStartTime = System.currentTimeMillis();
						getSpecialtyMasterList = delegate.execute(bcMapRoleMaster);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						if(getSpecialtyMasterList!=null && getSpecialtyMasterList.size()>0)
						{
							List<String> specialtyList = (List<String>) getSpecialtyMasterList.get(0);
							if(specialtyList!=null && specialtyList.size()>0)
							{
								getSpecialityResponse.getSpeciality().addAll(specialtyList);
							}
							
						}
						
					
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
			 }
			   else
				  //Fail in business validation
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(getSpecialityRequest.getOrganisationId()!=null && getSpecialityRequest.getOrganisationId()!="" && !getSpecialityRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(getSpecialityRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.GET_SPECIALTY_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(getSpecialityResponse)).build();
		return new ResponseEntity(getSpecialityResponse,HttpStatus.OK);
		
	}
		
	
	/**
	 * This method is used to get the clinicservice details for global search.
	 * @param globalClinicServcieSearchRequest
	 * @return globalClinicServcieSearchResponse
	 */
	@RequestMapping(value = "1.1/searchClinicServiceGlobal", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity searchClinicServiceGlobal(@RequestBody GlobalClinicServcieSearchRequest globalClinicServcieSearchRequest)
	{
			 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_CLINIC_SEARCH_SERVICE);
			 
			 BusinessDelegate delegate = new BusinessDelegateImpl();
			 GlobalClinicServcieSearchResponse globalClinicServcieSearchResponse =  null;
			 ArrayList<Serializable> isValidTokenList = null;
			 boolean isValidToken = false;
	 		 List<Serializable> globalClinicServiceList=null;
	 		 List<String> errorMsgList = null;
	 		 List<String> formatMsgList = null;
	 		 List<GlobalClinicServiceSearchDTO> gblClinicServiceSearchDTOList = null;
	 		 long serviceTimeStart = 0L;
			 long serviceTimeEnd = 0L;
			 long businessProcessStartTime=0L;
			 long businessProcessEndTime=0L;
			 long totalBusinessProcessingTime = 0L;
			 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
			 MonitoringEvent monitoringEvent = null;
	 		
	 		try
			{
	 			 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList = HPPClinicsGlobalSearchServiceHelper.searchGlobalClinicServiceFieldValidations(globalClinicServcieSearchRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList = HPPClinicsGlobalSearchServiceHelper.searchGlobalClinicServiceBusinessValidations(globalClinicServcieSearchRequest);
			  if(formatMsgList.size()==0)
			  {	
				/*
				 * Validating the token...
				 */
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(globalClinicServcieSearchRequest.getUserLoginName(),globalClinicServcieSearchRequest.getToken(),Long.parseLong(globalClinicServcieSearchRequest.getOrganisationId()));
					
				    businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						
						isValidToken = (Boolean) isValidTokenList.get(0);
						logger.info("isValidToken==========>"+isValidToken);
						if(isValidToken)
							//Valid token condition...
						{
							BusinessComponentMap bcMap = populateBCMForGlobalClinicSearch(globalClinicServcieSearchRequest.getOrganisationId(),globalClinicServcieSearchRequest.getServiceCategory(),globalClinicServcieSearchRequest.getServiceClassificationId(),globalClinicServcieSearchRequest.getServiceName());
							
							businessProcessStartTime = System.currentTimeMillis();
							globalClinicServiceList = (ArrayList<Serializable>) delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(globalClinicServiceList!=null && globalClinicServiceList.size()>0)
							{
								globalClinicServcieSearchResponse = new GlobalClinicServcieSearchResponse();
								gblClinicServiceSearchDTOList = (List<GlobalClinicServiceSearchDTO>)globalClinicServiceList.get(0);
								 if(gblClinicServiceSearchDTOList!=null && gblClinicServiceSearchDTOList.size()>0)
								 {
									 for(GlobalClinicServiceSearchDTO globalClinicServiceSearchDTO : gblClinicServiceSearchDTOList)
									 {
										 GlobalClinicServcieSearchResponse.Service jaxbService = new GlobalClinicServcieSearchResponse.Service();
										 BeanUtils.copyProperties(jaxbService, globalClinicServiceSearchDTO);
										 globalClinicServcieSearchResponse.getService().add(jaxbService);
									 }
								 }
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
				 
					}
			     }
				else
					//Fail in Business validations...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			   }
			   else
				   //Fail in mandatory fields check...
		       {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			   }
			    serviceTimeEnd = System.currentTimeMillis();
				if(globalClinicServcieSearchRequest.getOrganisationId()!=null && globalClinicServcieSearchRequest.getOrganisationId()!="" && !globalClinicServcieSearchRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(globalClinicServcieSearchRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SEARCH_CLINIC_SERVICE_GLOBAL_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			 }
			 catch(HPPApplicationException e)
			 {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
	 		catch (Exception e)
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_SERVICE_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG))).build();
	 			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_SERVICE_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			}
	 		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_CLINIC_SEARCH_SERVICE);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(globalClinicServcieSearchResponse)).build();
	 		return new ResponseEntity(globalClinicServcieSearchResponse,HttpStatus.OK);
		}	
	/**
	 * This is the service to get user details for global search...
	 * @param GlobalUserSearchRequest
	 * @return globalUserSearchResponse
	 */

	@RequestMapping(value = "1.1/searchUserGlobal", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity searchUserGlobal(@RequestBody GlobalUserSearchRequest globalUserSearchRequest)
	{
			 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_USER_GLOBAL);
			 BusinessDelegate delegate = new BusinessDelegateImpl();
			 GlobalUserSearchResponse globalUserSearchResponse =  null;			
			 ArrayList<Serializable> isValidTokenList = null;
			 boolean isValidToken = false;	 		
	 		 List<Serializable> globalUserSearchList=null;
	 		 List<String> errorMsgList = null;
	 		 List<String> formatMsgList = null;
	 		 List<GlobalUserSearchDTO> globalUserSearchDTOList = null;
	 		 GlobalUserSearchResponse.User gblUserSearchJaxb = null;
	 		 long serviceTimeStart = 0L;
			 long serviceTimeEnd = 0L;
			 long businessProcessStartTime=0L;
			 long businessProcessEndTime=0L;
			 long totalBusinessProcessingTime = 0L;
			 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
			 MonitoringEvent monitoringEvent = null;
	 		try
			{
	 			 serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
				 errorMsgList = HPPClinicsGlobalSearchServiceHelper.searchUserGlobalFieldValidations(globalUserSearchRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList = HPPClinicsGlobalSearchServiceHelper.searchUserGlobalBusinessValidations(globalUserSearchRequest);
			  if(formatMsgList.size()==0)
			  {	
				/*
				 * Validating the token...
				 */
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(globalUserSearchRequest.getUserLoginName(),globalUserSearchRequest.getToken(),Long.parseLong(globalUserSearchRequest.getOrganisationId()));
					
				 	businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						
						isValidToken = (Boolean) isValidTokenList.get(0);
						logger.info("isValidToken==========>"+isValidToken);
						if(isValidToken)
							//Valid token condition...
						{
							BusinessComponentMap bcMap = populateBCMForGlobalUserSearch(globalUserSearchRequest.getOrganisationId(),globalUserSearchRequest.getUserName(),globalUserSearchRequest.getPhoneNumber(),globalUserSearchRequest.getRoleId(),globalUserSearchRequest.getSpeciality(),globalUserSearchRequest.getIsClinicAdminUser(),globalUserSearchRequest.getUserType());
							
							businessProcessStartTime = System.currentTimeMillis();
							globalUserSearchList = (ArrayList<Serializable>) delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(globalUserSearchList!=null && globalUserSearchList.size()>0)
							{
								globalUserSearchResponse = new GlobalUserSearchResponse();
								globalUserSearchDTOList = (List<GlobalUserSearchDTO>)globalUserSearchList.get(0);
								 if(globalUserSearchDTOList!=null && globalUserSearchDTOList.size()>0)
								 {
									 for(GlobalUserSearchDTO globalUserSearchDTO : globalUserSearchDTOList)
									 {
										 gblUserSearchJaxb = new GlobalUserSearchResponse.User();
										BeanUtils.copyProperties(gblUserSearchJaxb, globalUserSearchDTO);
										globalUserSearchResponse.getUser().add(gblUserSearchJaxb);
									 }
								 }
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
				 
					}
			     }
				else
					//Fail in Business validations...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			   }
			   else
				   //Fail in mandatory fields check...
		       {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			   }
			    serviceTimeEnd = System.currentTimeMillis();
				if(globalUserSearchRequest.getOrganisationId()!=null && globalUserSearchRequest.getOrganisationId()!="" && !globalUserSearchRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(globalUserSearchRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SEARCH_USER_GLOBAL_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			 }
			 catch(HPPApplicationException e)
			 {
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
	 		catch (Exception e)
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_USER_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG))).build();
	 			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_USER_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			}
	 		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_USER_GLOBAL);
			//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(globalUserSearchResponse)).build();
	 		return new ResponseEntity(globalUserSearchResponse,HttpStatus.OK);
		}	
	
	/*
	 * Global Search service ends here
	 */
	
	/**
	 * This is the service to get patient details for global search...
	 * @param SearchGlobalPatientsRequest
	 * @return searchGlobalPatientsResponse
	 */
     //NOT USED
	@RequestMapping(value = "1.1/searchGlobalPatients", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity searchGlobalPatient(@RequestBody SearchGlobalPatientsRequest searchGlobalPatientsRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_PATIENT_GLOBAL);
		BusinessDelegate delegate = new BusinessDelegateImpl();
		SearchGlobalPatientsResponse searchGlobalPatientsResponse =  null;
	    ArrayList<Serializable> isValidTokenList = null;
	    boolean isValidToken = false;
	 	
	    List<Serializable> globalPatientsSearchList=null;
	 	List<String> errorMsgList = null;
	 	List<String> formatMsgList = null;
	 	List<GlobalPatientSearchDTO> globalpatientsList = null;
	 	SearchGlobalPatientsResponse.Patient gblPatientSearchJaxb = null;
	 	long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
	 		try
			{
	 			serviceTimeStart = System.currentTimeMillis();
				 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
				 monitoringEvent =new  MonitoringEvent();
	 			
			 errorMsgList = HPPClinicsGlobalSearchServiceHelper.searchGlobalPatientsFieldValidations(searchGlobalPatientsRequest);
				
			 if(errorMsgList.size()==0)
				 //Mandatory fields valid check...
			 {
			  formatMsgList = HPPClinicsGlobalSearchServiceHelper.searchGlobalPatientsBusinessValidations(searchGlobalPatientsRequest);
			  if(formatMsgList.size()==0)
			  {	
				/*
				 * Validating the token...
				 */
				 BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(searchGlobalPatientsRequest.getUserLoginName(),searchGlobalPatientsRequest.getToken(),Long.parseLong(searchGlobalPatientsRequest.getOrganisationId()));
					
				 businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						
						isValidToken = (Boolean) isValidTokenList.get(0);
						logger.info("isValidToken==========>"+isValidToken);
						if(isValidToken)
							//Valid token condition...
						{
							BusinessComponentMap bcMap = populateBCMForGlobalPatientsSearch(searchGlobalPatientsRequest.getOrganisationId(),searchGlobalPatientsRequest.getPatientName(),searchGlobalPatientsRequest.getPhoneNumber(),searchGlobalPatientsRequest.getUHID());
							
							businessProcessStartTime = System.currentTimeMillis();
							globalPatientsSearchList = (ArrayList<Serializable>) delegate.execute(bcMap);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(globalPatientsSearchList!=null && globalPatientsSearchList.size()>0)
							{
								searchGlobalPatientsResponse = new SearchGlobalPatientsResponse();
								globalpatientsList = (List<GlobalPatientSearchDTO>)globalPatientsSearchList.get(0);
								 if(globalpatientsList!=null && globalpatientsList.size()>0)
								 {
									 for(GlobalPatientSearchDTO globalPatientSearchDTO : globalpatientsList)
									 {
										gblPatientSearchJaxb = new SearchGlobalPatientsResponse.Patient();
										BeanUtils.copyProperties(gblPatientSearchJaxb, globalPatientSearchDTO);
										searchGlobalPatientsResponse.getPatient().add(gblPatientSearchJaxb);
									 }
								 }
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
				 
					}
			     }
				else
					//Fail in Business validations...
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				}
			   }
			   else
				   //Fail in mandatory fields check...
		       {
				 //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			   }
			   serviceTimeEnd = System.currentTimeMillis();
				if(searchGlobalPatientsRequest.getOrganisationId()!=null && searchGlobalPatientsRequest.getOrganisationId()!="" && !searchGlobalPatientsRequest.getOrganisationId().isEmpty())
				{
					monitoringEvent.setOrgId(Long.parseLong(searchGlobalPatientsRequest.getOrganisationId()));
				}
				else
				{
					monitoringEvent.setOrgId(0L);
				}
				monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.SEARCH_GLOBAL_PATIENTS_BA);
				monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
				monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
				monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
				oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
			 }
			 catch(HPPApplicationException e)
			 {
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
	 		catch (Exception e)
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_USER_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG))).build();
	 			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE,HPPClinicAdminConstants.CLINICADMIN_CONVERT_GBL_USER_SEARCH_DTO_TO_JAXB_EXCEPTION_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			}
	 		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_PATIENT_GLOBAL);
	 		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(searchGlobalPatientsResponse)).build();
	 		return new ResponseEntity(searchGlobalPatientsResponse,HttpStatus.OK);
		}	
	

	/**
	 * This method is used to get all Specialty data...
	 * @param autoSearchSpecialityRequest
	 * @return autoSearchSpecialityResponse
	 */
	@RequestMapping(value = "1.1/autoSuggestionSpeciality", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getAutoSuggestedSpeciality(@RequestBody AutoSearchSpecialityRequest autoSearchSpecialityRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_SPECIALITY);
		
		List<Serializable> getAutoSuggSpeciality = null;
		BusinessDelegate delegate = null;
		List<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		AutoSearchSpecialityResponse autoSearchSpecialityResponse = null;
		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			errorMsgList = HPPClinicsGlobalSearchServiceHelper.getAutoSuggestSpecialityFieldValidations(autoSearchSpecialityRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				
				formatMsgList = HPPClinicsGlobalSearchServiceHelper.getAutoSuggestSpecialityBusinessValidations(autoSearchSpecialityRequest);
				if(formatMsgList.size()==0)
				{
					/*
					 * Validating the token...
					 */
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(autoSearchSpecialityRequest.getUserLoginName(),autoSearchSpecialityRequest.getToken(),Long.parseLong(autoSearchSpecialityRequest.getOrganisationId()));
					
					delegate = new BusinessDelegateImpl();
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;
					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							autoSearchSpecialityResponse = new AutoSearchSpecialityResponse();
							
							/*
							 * Getting Auto suggested users...
							 */
							BusinessComponentMap bcMapAutoSuggSpeciality = populateBCMForAutoSuggestionSpeciality(autoSearchSpecialityRequest.getOrganisationId(),autoSearchSpecialityRequest.getSearchText());
							
							businessProcessStartTime = System.currentTimeMillis();
							getAutoSuggSpeciality = delegate.execute(bcMapAutoSuggSpeciality);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(getAutoSuggSpeciality!=null && getAutoSuggSpeciality.size()>0)
							{
								 List<String> specialityList = (List<String>) getAutoSuggSpeciality.get(0);
                         		 if(specialityList!=null && specialityList.size()>0)
									{
									 autoSearchSpecialityResponse.getSpeciality().addAll(specialityList);
									}
								
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
					}
				 }
				 else 
					 //Fail in Business validation
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					}
			   
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(autoSearchSpecialityRequest.getOrganisationId()!=null && autoSearchSpecialityRequest.getOrganisationId()!="" && !autoSearchSpecialityRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(autoSearchSpecialityRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.AUTO_SUGGESTION_SPECIALITY_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_SPECIALITY);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(autoSearchSpecialityResponse)).build();
		return new ResponseEntity(autoSearchSpecialityResponse,HttpStatus.OK);
		
	}
	
	
	/**
	 * This method is used to populate the businessComponentMap for GlobalUserSearch service
	 * @param orgId
	 * @param userNameText
	 * @param mobileNoText
	 * @param roleId
	 * @param specialty
	 * @param isCAUser
	 * @param userType
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForGlobalUserSearch(String orgId,String userNameText,String mobileNoText,String roleId,String specialty,String isCAUser,String userType) {
		
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_USER_SEARCH);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(userNameText);
		paramList.add(mobileNoText);
		paramList.add(roleId);
		paramList.add(specialty);
		paramList.add(isCAUser);
		paramList.add(userType);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"searchUserGlobal",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_USER_SEARCH);
		return bcMap;
	}
	/**
	 * This method is used to populate the businessComponentMap for searchClinicServiceGlobal service
	 * @param orgId
	 * @param category
	 * @param serviceClassificationId
	 * @param searchText
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForGlobalClinicSearch(String orgId,String category,String serviceClassificationId,String searchText) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_CLINIC_SEARCH);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(category);
		paramList.add(serviceClassificationId);
		paramList.add(searchText);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"searchClinicServiceGlobal",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_CLINIC_SEARCH);
		return bcMap;
	}
	/**
	 * This method is used to populate the businessComponentMap for searchGlobalPatients service
	 * @param orgId
	 * @param patientName
	 * @param mobileNoText
	 * @param uhid
	 * @return BusinessComponentMap
	 */
    private BusinessComponentMap populateBCMForGlobalPatientsSearch(String orgId,String patientName,String mobileNoText,String uhid) {
		
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_PATIENT_SEARCH);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(patientName);
		paramList.add(mobileNoText);
		paramList.add(uhid);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"searchGlobalPatient",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_GLOBAL_PATIENT_SEARCH);
		return bcMap;
	}
    /**
     * This method is used to populate the businessComponentMap for specialtyMaster service
     * @param orgId
     * @param isCAUser
     * @return BusinessComponentMap
     */
    private BusinessComponentMap populateBCMForSpecialtyMasterData(String orgId,String isCAUser) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_SPECIALTY_MASTER_DATA);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(isCAUser);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getSpecialtyMaster",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_SPECIALTY_MASTER_DATA);
		return bcMap;
	}
   /**
    * This method is used to populate the businessComponentMap for autoSuggestionSpeciality service
    * @param orgId
    * @param searchText
    * @return BusinessComponentMap
    */
    private BusinessComponentMap populateBCMForAutoSuggestionSpeciality(String orgId,String searchText) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_SPECIALITY);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(searchText);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getAutoSuggestedSpeciality",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_SPECIALITY);
		return bcMap;
	}
    
/*    private ResponseBuilder setHeaders(ResponseBuilder entity)
	{

		entity.header("Strict-Transport-Security", "max-age=60");
		entity.header("X-Content-Type-Options", "nosniff");
		entity.header("X-Frame-Options", "DENY");
		entity.header("X-WebKit-CSP", "default-src *");
		entity.header("X-XSS-Protection", "1 mode=block");
		return entity;

	}*/
	
}
