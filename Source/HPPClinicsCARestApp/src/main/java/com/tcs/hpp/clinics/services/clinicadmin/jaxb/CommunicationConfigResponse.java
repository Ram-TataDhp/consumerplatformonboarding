package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import java.util.List;

public class CommunicationConfigResponse {
	
	private List<SmsParameter> smsParameter;
	private List<EmailParameter> emailParameter;
	
	
	/**
	 * @return the smsParameter
	 */
	public List<SmsParameter> getSmsParameter() {
		return smsParameter;
	}

	/**
	 * @param smsParameter the smsParameter to set
	 */
	public void setSmsParameter(List<SmsParameter> smsParameter) {
		this.smsParameter = smsParameter;
	}

	/**
	 * @return the emailParameter
	 */
	public List<EmailParameter> getEmailParameter() {
		return emailParameter;
	}

	/**
	 * @param emailParameter the emailParameter to set
	 */
	public void setEmailParameter(List<EmailParameter> emailParameter) {
		this.emailParameter = emailParameter;
	}

	/**
	 * This class is used to provide a sms parameter response
	 * @author vinunair
	 *
	 */
	public static class SmsParameter
	{
		String receiptUser;
		String workflow;
		String message;
		String communicationType;
		/**
		 * @return the receiptUser
		 */
		public String getReceiptUser() {
			return receiptUser;
		}
		/**
		 * @param receiptUser the receiptUser to set
		 */
		public void setReceiptUser(String receiptUser) {
			this.receiptUser = receiptUser;
		}
		/**
		 * @return the workflow
		 */
		public String getWorkflow() {
			return workflow;
		}
		/**
		 * @param workflow the workflow to set
		 */
		public void setWorkflow(String workflow) {
			this.workflow = workflow;
		}
		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}
		/**
		 * @param message the message to set
		 */
		public void setMessage(String message) {
			this.message = message;
		}
		/**
		 * @return the communicationType
		 */
		public String getCommunicationType() {
			return communicationType;
		}
		/**
		 * @param communicationType the communicationType to set
		 */
		public void setCommunicationType(String communicationType) {
			this.communicationType = communicationType;
		}
		
		
	}
	
	/**
	 * This class is used for provide email parameter configuration
	 * @author vinunair
	 *
	 */
	public static class EmailParameter
	{
		String receiptUser;
		String workflow;
		String subject;
		String body;
		String communicationType;
		/**
		 * @return the receiptUser
		 */
		public String getReceiptUser() {
			return receiptUser;
		}
		/**
		 * @param receiptUser the receiptUser to set
		 */
		public void setReceiptUser(String receiptUser) {
			this.receiptUser = receiptUser;
		}
		/**
		 * @return the workflow
		 */
		public String getWorkflow() {
			return workflow;
		}
		/**
		 * @param workflow the workflow to set
		 */
		public void setWorkflow(String workflow) {
			this.workflow = workflow;
		}
		/**
		 * @return the subject
		 */
		public String getSubject() {
			return subject;
		}
		/**
		 * @param subject the subject to set
		 */
		public void setSubject(String subject) {
			this.subject = subject;
		}
		/**
		 * @return the body
		 */
		public String getBody() {
			return body;
		}
		/**
		 * @param body the body to set
		 */
		public void setBody(String body) {
			this.body = body;
		}
		/**
		 * @return the communicationType
		 */
		public String getCommunicationType() {
			return communicationType;
		}
		/**
		 * @param communicationType the communicationType to set
		 */
		public void setCommunicationType(String communicationType) {
			this.communicationType = communicationType;
		}
						
	}

}
