/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;*/






import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.ConfigParamDTO;
import com.tcs.hpp.clinics.clinicadmin.persistence.Parameter;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.convertor.HPPClinicAdminJAXBConvertor;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSysParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSysParamResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ConfigParamMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.ConfigParamMasterResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSystemParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSystemParamResponse;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateSystemParamRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UpdateSystemParamResponse;
import com.tcs.hpp.clinics.services.clinicadmin.util.HPPClinicAdminUtil;
import com.tcs.hpp.monitoring.MonitoringEvent;
import com.tcs.hpp.monitoring.MonitoringMessagePublisher;
import com.tcs.hpp.monitoring.MonitoringMessagePublisherImpl;
import com.tcs.hpp.platform.delegate.BusinessDelegate;
import com.tcs.hpp.platform.delegate.BusinessDelegateImpl;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Description: This class is used to handle all the services related to the Configuration Master Module..
 */

@RestController
@RequestMapping("/clinicAdministration")
public class HPPClinicsConfigParamMasterService {
	public static final String CLASSNAME = "HPPClinicsConfigParamMasterService";
	
	
	private static Logger logger=Logger.getLogger(HPPClinicsConfigParamMasterService.class);
	public HPPClinicsConfigParamMasterService()
	{
		 String trackId=null;
		 UUID idOne = UUID.randomUUID();
		 trackId=idOne.toString();
		 MDC.put(HPPClinicAdminConstants.TRACKING_ID_KEY, trackId);
	}

	/**
	 * This method is used to get all SystemParams data...
	 * @param autoSearchSysParamRequest
	 * @return AutoSearchSysParamResponse
	 */
	@RequestMapping(value = "1.1/autoSuggestionParameters", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getAutoSuggestedSysParameters(@RequestBody AutoSearchSysParamRequest autoSearchSysParamRequest)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_SYSTEM_PARAMS);		
		List<Serializable> getAutoSuggSysParam = null;
		BusinessDelegate delegate = null;
		List<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		AutoSearchSysParamResponse autoSearchSysParamResponse = null;
		long serviceTimeStart = 0L;
		long serviceTimeEnd = 0L;
		long businessProcessStartTime=0L;
		long businessProcessEndTime=0L;
		long totalBusinessProcessingTime = 0L;
		MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsConfigParamMasterServiceHelper.getAutoSuggestedSysParamFieldValidations(autoSearchSysParamRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
				
				formatMsgList = HPPClinicsConfigParamMasterServiceHelper.getAutoSuggestedSysParamBusinessValidations(autoSearchSysParamRequest);
				if(formatMsgList.size()==0)
				{
					/*
					 * Validating the token...
					 */
					BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(autoSearchSysParamRequest.getUserLoginName(),autoSearchSysParamRequest.getToken(),Long.parseLong(autoSearchSysParamRequest.getOrganisationId()));
					
					delegate = new BusinessDelegateImpl();
					businessProcessStartTime = System.currentTimeMillis();
					isValidTokenList = delegate.execute(isValidTokenBCM);
					businessProcessEndTime = System.currentTimeMillis();
					totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

					if(isValidTokenList!=null && isValidTokenList.size()>0)
					{
						isValidToken = (Boolean) isValidTokenList.get(0);
						if(isValidToken)
							//Valid token condition...
						{
							autoSearchSysParamResponse = new AutoSearchSysParamResponse();
							
							/*
							 * Getting Auto suggested users...
							 */
							BusinessComponentMap bcMapAutoSuggUsers = populateBCMForAutoSuggestionSysParam(autoSearchSysParamRequest.getOrganisationId(),autoSearchSysParamRequest.getSearchText());
							
							businessProcessStartTime = System.currentTimeMillis();
							getAutoSuggSysParam = delegate.execute(bcMapAutoSuggUsers);
							businessProcessEndTime = System.currentTimeMillis();
                            totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
							if(getAutoSuggSysParam!=null && getAutoSuggSysParam.size()>0)
							{
								 List<ConfigParamDTO> configParamDTOList = (List<ConfigParamDTO>) getAutoSuggSysParam.get(0);
								 AutoSearchSysParamResponse.SystemParam jaxbSystemParam = null;
								 if(configParamDTOList!=null && configParamDTOList.size()>0)
								 {
									 for(ConfigParamDTO configParamDTO:configParamDTOList)
										{
										    jaxbSystemParam = new AutoSearchSysParamResponse.SystemParam();
										    jaxbSystemParam.setSystemParamterId(configParamDTO.getSystemParameterId().toString());
										    jaxbSystemParam.setParamName(configParamDTO.getParameterName());
										    autoSearchSysParamResponse.getSystemParam().add(jaxbSystemParam);
										}
								 }
								
							}
						}
						else
							//Invalid token
						{
							//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
							return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
						}
					}
				}
				 else 
					 //Fail in Business validation
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
					 	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
					}
			   
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(autoSearchSysParamRequest.getOrganisationId()!=null && autoSearchSysParamRequest.getOrganisationId()!="" && !autoSearchSysParamRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(autoSearchSysParamRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.AUTO_SUGGESTION_PARAMETERS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			if(e.getExceptionMessage().contains("||"))
			{
				 e.setExceptionMessage(e.getExceptionMessage().split("||")[0]);
				 
			}
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.AUTO_SUGGESTION_SYSTEM_PARAMS);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(autoSearchSysParamResponse)).build();
		return new ResponseEntity(autoSearchSysParamResponse,HttpStatus.OK);
		
	}
	/**
	 * This method is rest service for Retrieving the system parameters...
	 * @param GetSystemParamRequest
	 * @return Response
	 */

	@RequestMapping(value = "1.1/retrieveSystemParams", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity retrieveSystemParams(@RequestBody GetSystemParamRequest request)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM);
		BusinessDelegate delegate = null;
		GetSystemParamResponse response =  null;
		GetSystemParamResponse.SystemParam systemParam = null;
		ArrayList<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<Serializable> serviceList = null;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		String count=null;
		long serviceTimeStart = 0L;
		long serviceTimeEnd = 0L;
		long businessProcessStartTime=0L;
		long businessProcessEndTime=0L;
		long totalBusinessProcessingTime = 0L;
		MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		MonitoringEvent monitoringEvent = null;
		try
		{
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 errorMsgList = HPPClinicsConfigParamMasterServiceHelper.retrieveSystemParametersFieldValidations(request);
			
		    if(errorMsgList.size()==0)
		    	//Mandatory fields valid check...
		    {
		     formatMsgList = HPPClinicsConfigParamMasterServiceHelper.retrieveSystemParametersBusinessValidations(request);	
			 if(formatMsgList.size()==0)
			 {
		    	/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						String sysParamId = request.getSysParamId();
						String searchKey = request.getSearchText();
						String orgId = request.getOrganisationId();
						String startRow = request.getStartRow();
						String interval = request.getInterval();
						if(startRow==null || "".equals(startRow))
						{
							startRow  ="1";
						}
						else if(startRow.trim().isEmpty() || startRow.equals("0"))
						{
							startRow="1";
						}
						if(interval==null || "".equals(interval))
						{
							interval=Integer.valueOf(HPPClinicAdminConstants.DEFAULT_NO_OF_RECORDS).toString();
						}
						else if(interval.trim().isEmpty() || interval.equals("0"))
						{
							interval=Integer.valueOf(HPPClinicAdminConstants.DEFAULT_NO_OF_RECORDS).toString();
						}
						BusinessComponentMap bcMap = populateBCMForRetriveSystemParams(sysParamId,searchKey,orgId,startRow,interval);
						businessProcessStartTime = System.currentTimeMillis();
						serviceList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
						response = new GetSystemParamResponse();
						if(serviceList!=null && serviceList.size()>0)
						{
						 List searchConfigParamCountList = (List)serviceList.get(0);
						 if(searchConfigParamCountList.size()>0)
						 {
							 count=searchConfigParamCountList.get(0).toString();
							 List<ConfigParamDTO> configParamDTOs = (List<ConfigParamDTO>)searchConfigParamCountList.get(1);
								
								List<GetSystemParamResponse.SystemParam> systemParams = new ArrayList<GetSystemParamResponse.SystemParam>();
								
								for(ConfigParamDTO configParamDTO : configParamDTOs)
								{
									systemParam = new GetSystemParamResponse.SystemParam();
									
									systemParam.setLevel(configParamDTO.getLevel());
									systemParam.setLevelValueId(configParamDTO.getLevelValueId());
									systemParam.setLevelValueName(configParamDTO.getLevelValueName());
									systemParam.setParamName(configParamDTO.getParameterName());
									systemParam.setParamId(configParamDTO.getParamNameId());
									systemParam.setParamValue(configParamDTO.getParamValue());
									systemParam.setSystemParamterId(configParamDTO.getSystemParameterId());
									systemParam.setOverrideOption(configParamDTO.getOverrideOption());
									
									systemParams.add(systemParam);
								}
								
								response.setSystemParamList(systemParams);
								response.setCount(count);
						 }
							
						}
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
						
		    }
			 }
		       else
		    	   //Fail in Business Validations
				  {
					// return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
		    	   return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
				  }
			}
		    else
		    	//Fail in mandatory fields check...
			{
			  //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
		    	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		    serviceTimeEnd = System.currentTimeMillis();
			if(request.getOrganisationId()!=null && request.getOrganisationId()!="" && !request.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(request.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.RETRIEVE_SYSTEMPARAMS_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.RETRIEVE_SYSTEM_PARAM); 
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(response)).build();
		return new ResponseEntity(response,HttpStatus.OK);
	}
	/**
	 * This method is rest service for Updating the system parameter value...
	 * @param updateSystemParamRequest
	 * @return Response
	 */
	@RequestMapping(value = "1.1/updateSystemParam", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity updateSystemParam(@RequestBody UpdateSystemParamRequest updateSystemParamRequest)
	{
		 logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM);
		 BusinessDelegate delegate = null;
		 HPPClinicAdminJAXBConvertor oHPPClinicAdminJAXBConvertor = null;
		 List<Serializable> isValidTokenList = null;
		 boolean isValidToken = false;
		 List<String> errorMsgList = null;
		 List<String> formatMsgList = null;
		 UpdateSystemParamResponse updateSystemParamResponse = null;
		 long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		 try
		 {
			 serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
			 updateSystemParamResponse = new UpdateSystemParamResponse();
			 errorMsgList = HPPClinicsConfigParamMasterServiceHelper.updateSystemParamFieldValidations(updateSystemParamRequest);
			if(errorMsgList.size()==0)
				//Mandatory fields valid check...
			{
			 formatMsgList = HPPClinicsConfigParamMasterServiceHelper.updateSystemParamBusinessValidations(updateSystemParamRequest);
	   		 if(formatMsgList.size()==0)
	   		 {
				/*
				 * Validating the token...
				 */
				BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(updateSystemParamRequest.getUserLoginName(),updateSystemParamRequest.getToken(),Long.parseLong(updateSystemParamRequest.getOrganisationId()));
				
				delegate = new BusinessDelegateImpl();
				businessProcessStartTime = System.currentTimeMillis();
				isValidTokenList = delegate.execute(isValidTokenBCM);
				businessProcessEndTime = System.currentTimeMillis();
				totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

				if(isValidTokenList!=null && isValidTokenList.size()>0)
				{
					isValidToken = (Boolean) isValidTokenList.get(0);
					if(isValidToken)
						//Valid token condition...
					{
						oHPPClinicAdminJAXBConvertor = new HPPClinicAdminJAXBConvertor();
						Parameter parameter = oHPPClinicAdminJAXBConvertor.convertFromJAXBtoUpdateSystemParamEntity(updateSystemParamRequest);
						long siteId=0;
						if(updateSystemParamRequest.getSystemParam().getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LOCATION) || updateSystemParamRequest.getSystemParam().getLevel().equalsIgnoreCase(HPPClinicAdminConstants.LEVEL_SITE))
						{
							siteId=Long.parseLong(updateSystemParamRequest.getSystemParam().getSiteId());
						}
						BusinessComponentMap bcMap = populateBCMForUpdateSystemParam(parameter,updateSystemParamRequest.getSystemParam().getLevel(),Long.parseLong(updateSystemParamRequest.getOrganisationId()),siteId);
						businessProcessStartTime = System.currentTimeMillis();
						List<Serializable> updateParamList = delegate.execute(bcMap);
						businessProcessEndTime = System.currentTimeMillis();
                        totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
                        if(updateParamList!=null && updateParamList.size()>0)
                        {
                        	String clashAppointmentMsg = (String)updateParamList.get(0);
                        	if(clashAppointmentMsg!=null && !clashAppointmentMsg.trim().isEmpty())
                        	{
                        		updateSystemParamResponse.setSystemParamId(updateSystemParamRequest.getSystemParam().getSysParamNameId());
        						updateSystemParamResponse.setSuccessMessage(clashAppointmentMsg);
                        	}
                        	else
                        	{
                        		updateSystemParamResponse.setSystemParamId(updateSystemParamRequest.getSystemParam().getSysParamNameId());
        						updateSystemParamResponse.setSuccessMessage(HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM_SUCCESS_MESSAGE.replace("<Configuration Parameter>", updateSystemParamRequest.getSystemParam().getSysParamName()).replace("<Level Value>", updateSystemParamRequest.getSystemParam().getLevelValue()));
                        	}
                        }
						
					}
					else
						//Invalid token
					{
						//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
						return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
	   		}
  			  else
  				  //Fail in Business validations check...
  			  {
  				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
  				  return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
  			  }
			}
			else
				//Fail in mandatory fields check...
			{
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
				return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
			serviceTimeEnd = System.currentTimeMillis();
			if(updateSystemParamRequest.getOrganisationId()!=null && updateSystemParamRequest.getOrganisationId()!="" && !updateSystemParamRequest.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(updateSystemParamRequest.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.UPDATE_SYSTEMPARAM_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{			
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e).replace("<Configuration Parameter>", updateSystemParamRequest.getSystemParam().getSysParamName()).replace("<Level Name>", updateSystemParamRequest.getSystemParam().getLevelValue())))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e).replace("<Configuration Parameter>", updateSystemParamRequest.getSystemParam().getSysParamName()).replace("<Level Name>", updateSystemParamRequest.getSystemParam().getLevelValue())),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.UPDATE_SYSTEM_PARAM);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(updateSystemParamResponse)).build();
		return new ResponseEntity(updateSystemParamResponse,HttpStatus.OK);
		
	}
	
	/**
	 * This method is to populate the businessComponentMap for updateSystemParameter service
	 * @param parameter
	 * @param level
	 * @param orgId
	 * @param siteId
	 * @return BusinessComponentMap
	 */
    private BusinessComponentMap populateBCMForUpdateSystemParam(Parameter parameter,String level,long orgId,long siteId) {
    	logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " +HPPClinicAdminConstants.BCM_UPDATE_SYSTEM_PARAM);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(parameter);
		paramList.add(level);
		paramList.add(orgId);
		paramList.add(siteId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"updateSystemParam",paramList);	
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " +HPPClinicAdminConstants.BCM_UPDATE_SYSTEM_PARAM);
		return bcMap;
	}
    /**
     * This method is to populate the businessComponentMap for retrieveSystemParams service
     * @param sysParamId
     * @param searchKey
     * @param orgId
     * @param startRow
     * @param interval
     * @return BusinessComponentMap
     */
	private BusinessComponentMap populateBCMForRetriveSystemParams(String sysParamId, String searchKey, String orgId, String startRow,String interval) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.BCM_RETRIEVE_SYSTEMPARAMS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(sysParamId);
		paramList.add(searchKey);
		paramList.add(Integer.parseInt(startRow));
		paramList.add(Integer.parseInt(interval));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"retrieveSystemParams",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.BCM_RETRIEVE_SYSTEMPARAMS);
		return bcMap;
	
	}
	/**
	 * This method is rest service to fetch list of master parameters...
	 * @param ConfigParamMasterRequest
	 * @return Response
	 */
	/**
	 * This method is a rest service for getting ParametersMaster
	 * @param ConfigParamMasterRequest
	 * @return ConfigParamMasterResponse
	 */
	@RequestMapping(value = "1.1/getParametersMaster", method = RequestMethod.POST,consumes={MediaType.APPLICATION_JSON_VALUE},produces={MediaType.APPLICATION_JSON_VALUE},headers="Accept=application/json")
	public ResponseEntity getParametersMaster(@RequestBody ConfigParamMasterRequest request)
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER);
		BusinessDelegate delegate = null;
		ConfigParamMasterResponse response =  null;
		ConfigParamMasterResponse.SystemParam systemParam = null;
		ArrayList<Serializable> isValidTokenList = null;
		boolean isValidToken = false;
		List<Serializable> serviceList = null;
		List<String> errorMsgList = null;
		List<String> formatMsgList = null;
		long serviceTimeStart = 0L;
		 long serviceTimeEnd = 0L;
		 long businessProcessStartTime=0L;
		 long businessProcessEndTime=0L;
		 long totalBusinessProcessingTime = 0L;
		 MonitoringMessagePublisher oMonitoringMessagePublisher = null;
		 MonitoringEvent monitoringEvent = null;
		try
		{
			serviceTimeStart = System.currentTimeMillis();
			 oMonitoringMessagePublisher = new MonitoringMessagePublisherImpl();
			 monitoringEvent =new  MonitoringEvent();
         errorMsgList = HPPClinicsConfigParamMasterServiceHelper.getParametersMasterFieldValidations(request);
			
		    if(errorMsgList.size()==0)
		    	//Mandatory fields valid check...
		    {
		    formatMsgList = HPPClinicsConfigParamMasterServiceHelper.getParametersMasterBusinessValidations(request);
		   	if(formatMsgList.size()==0)
		    {
			/*
			 * Validating the token...
			 */
			BusinessComponentMap isValidTokenBCM  = HPPClinicAdminUtil.populateBCMForIsValidToken(request.getUserLoginName(),request.getToken(),Long.parseLong(request.getOrganisationId()));
			
			delegate = new BusinessDelegateImpl();
			businessProcessStartTime = System.currentTimeMillis();
			isValidTokenList = (ArrayList<Serializable>) delegate.execute(isValidTokenBCM);
			businessProcessEndTime = System.currentTimeMillis();
			totalBusinessProcessingTime = businessProcessEndTime-businessProcessStartTime;

			if(isValidTokenList!=null && isValidTokenList.size()>0)
			{
				isValidToken = (Boolean) isValidTokenList.get(0);
				if(isValidToken)
					//Valid token condition...
				{
					String orgId = request.getOrganisationId();
					BusinessComponentMap bcMap = populateBCMForGetParametersMaster(orgId);
					businessProcessStartTime = System.currentTimeMillis();
					serviceList = delegate.execute(bcMap);
					businessProcessEndTime = System.currentTimeMillis();
                    totalBusinessProcessingTime = totalBusinessProcessingTime+(businessProcessEndTime-businessProcessStartTime);
					response = new ConfigParamMasterResponse();
					if(serviceList!=null && serviceList.size()>0)
					{
						List<ConfigParamDTO> configParamDTOs = (List<ConfigParamDTO>)serviceList.get(0);
						List<ConfigParamMasterResponse.SystemParam> systemParams = new ArrayList<ConfigParamMasterResponse.SystemParam>();
						for(ConfigParamDTO configParamDTO : configParamDTOs)
						{
							systemParam = new ConfigParamMasterResponse.SystemParam();
							systemParam.setParamId(configParamDTO.getSystemParameterId());
							systemParam.setParamName(configParamDTO.getParameterName());
							systemParam.setHelpText(configParamDTO.getHelpText());
							systemParam.setDatatype(configParamDTO.getDataType());
							systemParams.add(systemParam);
						}
						
						
						response.setSystemParam(systemParams);
					}
				}
				else
					//Invalid token
				{
					//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG))).build();
					return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_CODE,HPPClinicAdminConstants.CLINICADMIN_INVALID_TOKEN_MSG),HttpStatus.INTERNAL_SERVER_ERROR);
			
				}
			  } 
		     }
			 else
			  //Fail in Business validations check...
			 {
				//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()))).build();
				 return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_BUSINESS_FIELDS_MSG+formatMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			 }
			
		    }
		    else
		    	//Fail in mandatory fields check...
			{
			  //return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()))).build();
		    	return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_CODE,HPPClinicAdminConstants.CLINICADMIN_MANDATORY_FIELDS_MSG+errorMsgList.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		    serviceTimeEnd = System.currentTimeMillis();
			if(request.getOrganisationId()!=null && request.getOrganisationId()!="" && !request.getOrganisationId().isEmpty())
			{
				monitoringEvent.setOrgId(Long.parseLong(request.getOrganisationId()));
			}
			else
			{
				monitoringEvent.setOrgId(0L);
			}
			monitoringEvent.setBusinessActivity(HPPClinicAdminConstants.GET_PARAMETERS_MASTER_SERVICE_BA);
			monitoringEvent.setTrackingId(String.valueOf(MDC.get(HPPClinicAdminConstants.TRACKING_ID_KEY)));
			monitoringEvent.setServiceLayerProcessingTime(serviceTimeEnd-serviceTimeStart);
			monitoringEvent.setBusinessLayerProcessingTime(totalBusinessProcessingTime);
			oMonitoringMessagePublisher.publishMonitoringMessage(monitoringEvent);
		}
		catch (HPPApplicationException e) 
		{
			//return setHeaders(Response.status(HPPClinicAdminConstants.ERROR_STATUS_CODE).entity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)))).build();			
			return new ResponseEntity(HPPClinicAdminUtil.prepareErrorResponse(e.getErrorCode(),HPPClinicAdminUtil.manageExecptionMsg(e)),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_PARAMETERS_MASTER);
		//return setHeaders(Response.status(HPPClinicAdminConstants.SUCCESS_STATUS_CODE).entity(response)).build();
		return new ResponseEntity(response,HttpStatus.OK);
	}
	
	/**
	 * This method is used to populate the businessComponentMap for getParametersMaster service
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForGetParametersMaster(String orgId) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.BCM_GET_PARAM_MASTER);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getParametersMaster",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.BCM_GET_PARAM_MASTER);
		return bcMap;
	}

	/**
	 * This method is used to populate the businessComponentMap for autoSuggestionParameters service
	 * @param orgId
	 * @param searchText
	 * @return BusinessComponentMap
	 */
	private BusinessComponentMap populateBCMForAutoSuggestionSysParam(String orgId,String searchText) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_SYS_PARAM_DATA);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(searchText);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getAutoSuggestedSystemParams",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_SYS_PARAM_DATA);
		return bcMap;
	}
/*	private ResponseBuilder setHeaders(ResponseBuilder entity)
	{
		entity.header("Strict-Transport-Security", "max-age=60");
		entity.header("X-Content-Type-Options", "nosniff");
		entity.header("X-Frame-Options", "DENY");
		entity.header("X-WebKit-CSP", "default-src *");
		entity.header("X-XSS-Protection", "1 mode=block");
		return entity;

	}*/
}
