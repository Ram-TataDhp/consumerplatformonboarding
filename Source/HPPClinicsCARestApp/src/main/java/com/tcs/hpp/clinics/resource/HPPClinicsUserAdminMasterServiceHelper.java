/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.APP_NAME;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.FETCH_APP_UPDATE_METHOD;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL;
import static com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants.JAVA_GLOBAL;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.clinicadmin.dto.CreateUserReqDTO;
import com.tcs.hpp.clinics.clinicadmin.dto.UpdateCommunicationDto;
import com.tcs.hpp.clinics.clinicadmin.dto.UpdateUserRequestDTO;
import com.tcs.hpp.clinics.platform.util.BusinessComponentMap;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchUserRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.LogoutRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.UserMasterRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Description: This Class is used to handle the mandatory field validations and business validations for all the User Admin services.
 */
public final class HPPClinicsUserAdminMasterServiceHelper {
	private static Logger logger=Logger.getLogger(HPPClinicsUserAdminMasterServiceHelper.class);
	private HPPClinicsUserAdminMasterServiceHelper()
	{
		
	}
	/**
	 * This method is used to perform logoutService FieldValidations.
	 * @param logoutRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> logoutServiceFieldValidations(LogoutRequest logoutRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.LOGOUT_FIELD_VALIDATION);
		
	   List<String> errorMsgList = null;
	   try
	   {
		errorMsgList = new ArrayList<String>();
		if(logoutRequest.getToken()==null || "".equals(logoutRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(logoutRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(logoutRequest.getUserLoginName()==null || "".equals(logoutRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(logoutRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.LOGOUT_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.LOGOUT_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.LOGOUT_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform getAutoSuggestedUsers FieldValidations.
	 * @param AutoSearchUserRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestedUsersFiledValidations(AutoSearchUserRequest autoSearchUserRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_USERS_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(autoSearchUserRequest.getToken()==null || "".equals(autoSearchUserRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(autoSearchUserRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(autoSearchUserRequest.getUserLoginName()==null || "".equals(autoSearchUserRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(autoSearchUserRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(autoSearchUserRequest.getOrganisationId()==null || "".equals(autoSearchUserRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(autoSearchUserRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(autoSearchUserRequest.getSearchText()==null || "".equals(autoSearchUserRequest.getSearchText()))
			{
				errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
			}
			else if(autoSearchUserRequest.getSearchText().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.SEARCH_TEXT_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_USERS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGESTION_USERS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_USERS_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform getAutoSuggestedUsers BusinessValidations.
	 * @param operationalRoleMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestedUsersBusinessValidations(AutoSearchUserRequest autoSearchUserRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_USERS_BUSINESS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
		
			Long.parseLong(autoSearchUserRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGESTION_USERS_BUSINESS_VALIDATION);
		return formatMsgList;
	}
	/**
	 * This method is used to perform getUserMaster FieldValidations.
	 * @param userMasterRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserMasterFiledValidations(UserMasterRequest userMasterRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_MASTER_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(userMasterRequest.getToken()==null || "".equals(userMasterRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(userMasterRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(userMasterRequest.getUserLoginName()==null || "".equals(userMasterRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(userMasterRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(userMasterRequest.getOrganisationId()==null || "".equals(userMasterRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(userMasterRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GET_USER_DETAILS_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getUserMaster BusinessValidations.
	 * @param userMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getUserMasterBusinessValidations(UserMasterRequest userMasterRequest)throws HPPApplicationException
	{
      logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_USER_MASTER_BUSS_VALIDATION);
		
		List<String> formatMsgList = new ArrayList<String>();
		try
		{
			
			Long.parseLong(userMasterRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.GET_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.GET_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_USER_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to prepare the BCM map for getRssFeed service in homePageData service...
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMapWithHomePageDataFeedMap() {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG +HPPClinicAdminConstants.BCM_HOME_PAGE_DATA);
		List<Serializable> paramList = new ArrayList<Serializable>();
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getHomePageData",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG +HPPClinicAdminConstants.BCM_HOME_PAGE_DATA);
		return bcMap;
	}
	/**
	 * This method is used to prepare the BCM map for Authenticate User service in AppLogin Service...
	 * @param userName
	 * @param password
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBusinessComponentForAuthenticateUser(String userName,String password) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_AUTHENTICATE_USER);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userName);
		paramList.add(password);
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"authenticateUser",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_AUTHENTICATE_USER);
		return authUserBCMap;
	}
	
	
	/**
	 *  This method is used to prepare the BCM map for get User details service in AppLogin Service...
	 * @param userName
	 * @param orgId
	 * @param flag
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForGetUserDtls(String clinicUserId,String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_GET_USER_DETAILS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(orgId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getUserDetailsByUserId",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_GET_USER_DETAILS);
		return bcMap;
	}
	/**
	 * This method is used to prepare the BCM map for get User Master from Option Table in AppLogin Service...
	 * @param optionType
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForGetUserMaster(String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_GET_USERMASTER);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getUserMaster",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_GET_USERMASTER);
		return bcMap;
	}
	/**
	 * This method is used to prepare the BCM map for Auto Suggested Users in AppLogin Service...
	 * @param orgId
	 * @param searchText
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForAutoSuggestionUsers(String orgId,String searchText) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_USERS_DATA);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(Long.parseLong(orgId));
		paramList.add(searchText);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getAutoSuggestedUsers",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_AUTO_SUGGESTION_USERS_DATA);
		return bcMap;
	}
	
	
	/**
	 * The below method is used to prepare the BCM map for Create Clinic User service...
	 * @param createUserReqDTO
	 * @param apkFileLink
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForCreateUser(CreateUserReqDTO createUserReqDTO, String apkFileLink) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_CREATE_USER);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(createUserReqDTO);
		paramList.add(apkFileLink);
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"createUser",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_CREATE_USER);
		return authUserBCMap;
	}
	/**
	 * The below method is used to prepare the BCM map for Clinic Update user...
	 * @param clinicUser
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForUpdateUser(UpdateUserRequestDTO updateUserRequestDTO) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.BCM_UPDATE_USER);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(updateUserRequestDTO);
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_USERTXMANAGER_JNDI,"updateUser",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.BCM_UPDATE_USER);
		return authUserBCMap;
	}
	/**
	 * The below method is used to prepare the BCM map for Change Password service...
	 * @param userLoginName
	 * @param password
	 * @return BusinessComponentMap
	 */ 
	public static BusinessComponentMap populateBCMForChangePassword(String userLoginName,String newPassword,String oldPassword) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_CHANGE_PASSWORD);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		paramList.add(newPassword);
		paramList.add(oldPassword);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"changePassword",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_CHANGE_PASSWORD);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the Reset Password service...
	 * @param userLoginName, password
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForResetPassword(String userLoginName, String password) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_RESET_PASSWORD);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		paramList.add(password);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"resetPassword",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_RESET_PASSWORD);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the Forgot Password service...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForForgotPassword(String userLoginName) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_FORGOT_PASSWORD);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"forgotPassword",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_FORGOT_PASSWORD);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the Forgot Password service...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForCreateUserAuthentication(String userLoginName, String password, String clinicUserId, String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_CREATE_USER_AUTHENTICATION);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		paramList.add(password);
		paramList.add(clinicUserId);
		paramList.add(orgId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"createUserAuthentication",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_CREATE_USER_AUTHENTICATION);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the BCM map for Change Password service...
	 * @param userLoginName
	 * @param password
	 * @return BusinessComponentMap
	 */ 
	public static BusinessComponentMap populateBCMForGetUserBasicDetails(String userLoginName,String clinicUserId,String organisationId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_GET_USER_BASIC_DETAILS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		paramList.add(clinicUserId);
		paramList.add(organisationId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_USERTXMANAGER_JNDI,"getUserBasicDetails",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_GET_USER_BASIC_DETAILS);
		return bcMap;
	}
	
	/**
	 * The below method is used to prepare the UserAssocToClinic service...
	 * @param clinicUserId,  orgId
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForUserAssocToClinic(String clinicUserId, String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_FORGOT_PASSWORD);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(orgId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"userAssocToClinic",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_FORGOT_PASSWORD);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the Logout service...
	 * @param userLoginName
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForLogout(String userLoginName) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_LOGOUT);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICSERVICE_GATEWAY_LOCAL,"logoutUser",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_LOGOUT);
		return authUserBCMap;
	}
	/**
	 * The below method is used to prepare the BCM map for RetrievUsers service...
	 * @param searchText
	 * @param startRow
	 * @param interval
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForRetrieveUsers(String searchText,String startRow,String interval,String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_RETRIEVE_USERS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(searchText);
		paramList.add(startRow);
		paramList.add(interval);
		paramList.add(Long.parseLong(orgId));
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"retrieveUsers",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_RETRIEVE_USERS);
		return authUserBCMap;
	}
	/**
	 * The below method is used to prepare the BCM map for User status upadte service...
	 * @param userId
	 * @param isActive
	 * @param roleType
	 * @param orgId
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForUserStatusUpdate(String userId,String isActive,String roleType,String orgId,String adminUserId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_USER_STATUS);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userId);
		paramList.add(isActive);
		paramList.add(roleType);
		paramList.add(Long.parseLong(orgId));
		paramList.add(Long.parseLong(adminUserId));
		BusinessComponentMap authUserBCMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"userStatusUpdate",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_USER_STATUS);
		return authUserBCMap;
	}
	/**
	 * The below method is used to prepare the BCM map for apk Link service...
	 * @param clinicUser
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForApkLink() {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ HPPClinicAdminConstants.POPULATE_BCM_APK_LINK);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(HPPClinicAdminConstants.CLINIC_APP_STRING);
		BusinessComponentMap bcMap = new BusinessComponentMap(APP_NAME, JAVA_GLOBAL+APP_NAME+HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL, FETCH_APP_UPDATE_METHOD, paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ HPPClinicAdminConstants.POPULATE_BCM_APK_LINK);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the Verify user login name service...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForVerifyUserLoginName(String userLoginName) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_VERIFY_USER_LOGIN_NAME);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(userLoginName);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"verifyUserLoginName",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_VERIFY_USER_LOGIN_NAME);
		return bcMap;
	}
	/**
	 * The below method is used to prepare the user sign up validate service...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForUserSignUpValidate(String clinicUserId, String organisationId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_USER_SIGN_UP_VALIDATE);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(organisationId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"userSignUpValidate",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_USER_SIGN_UP_VALIDATE);
		return bcMap;
	}
	/**
	 * The below method is used to verify the otp sent to user mobile...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForOtpVerification(String clinicUserId, String organisationId, String otpCode) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_OTP_VERIFICATION);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(organisationId);
		paramList.add(otpCode);
		paramList.add(HPPClinicAdminConstants.CLINICS_APP_INDICATOR);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"otpVerification",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_OTP_VERIFICATION);
		return bcMap;
	}
	/**
	 * The below method is used to resend the otp...
	 * @param userLoginName
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForResendSms(String clinicUserId, String organisationId, String mobileNumber) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+ " " + HPPClinicAdminConstants.BCM_OTP_VERIFICATION);
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(organisationId);
		paramList.add(mobileNumber);
		paramList.add(Long.parseLong(clinicUserId));
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"resendSms",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+ " " + HPPClinicAdminConstants.BCM_OTP_VERIFICATION);
		return bcMap;
	}
	
	/**
	 *  This method is used to prepare the BCM map for get Consultation details service in GetUserConsultationTabDetails Service...
	 * @param userName
	 * @param orgId
	 * @param flag
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForGetUserConsultationTabDetails(String clinicUserId,String orgId) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" populateBCMForGetUserConsultationTabDetails");
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(orgId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getUserConsultationTabDetail",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" populateBCMForGetUserConsultationTabDetails");
		return bcMap;
	}
	
	/**
	 *  This method is used to prepare the BCM map for get Consultation details service in UpdateUserConsultationTabDetails Service...
	 * @param userName
	 * @param orgId
	 * @param flag
	 * @return BusinessComponentMap
	 */
	public static BusinessComponentMap populateBCMForUpdateUserConsultationTabDetails(String clinicUserId,String orgId,ArrayList<String> consultation_tab_details) {
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" populateBCMForUpdateUserConsultationTabDetails");
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(orgId);
		paramList.add(consultation_tab_details);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"updateUserConsultationTabDetail",paramList);		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" populateBCMForUpdateUserConsultationTabDetails");
		return bcMap;
	}
	
	public static BusinessComponentMap populateBCMForAcceptTermAndCondition(String clinicUserId,String tc,String ipAddress) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" populateBCMForAcceptTermAndCondition");
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(clinicUserId);
		paramList.add(tc);
		paramList.add(ipAddress);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_GLOBAL_MANAGER_LOCAL,"acceptTermCondition",paramList);
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" populateBCMForAcceptTermAndCondition");
		return bcMap;
	}
	
	public static BusinessComponentMap populateBCMForCommunicationConfig(String organizationId) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" populateBCMForCommunicationConfig");
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(organizationId);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"getCommunicationConfig",paramList);
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" populateBCMForCommunicationConfig");
		return bcMap;
	}
	
	public static BusinessComponentMap populateBCMForUpdateCommunicationConfig(UpdateCommunicationDto updateCommunicationDto) 
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+" populateBCMForUpdateCommunicationConfig");
		ArrayList<Serializable> paramList = new ArrayList<Serializable>();
		paramList.add(updateCommunicationDto);
		BusinessComponentMap bcMap = new BusinessComponentMap(HPPClinicAdminConstants.APP_NAME,HPPClinicAdminConstants.JAVA_GLOBAL+HPPClinicAdminConstants.APP_NAME+HPPClinicAdminConstants.HPPCLINICS_CLINICADMIN_MANAGER_LOCAL,"updateCommunicationConfig",paramList);
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+" populateBCMForUpdateCommunicationConfig");
		return bcMap;
	}
}
