/**
 * 
 */
package com.tcs.hpp.clinics.resource;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.tcs.hpp.clinics.clinicadmin.constants.HPPClinicAdminConstants;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.AutoSearchSpecialityRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GetSpecialityRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalClinicServcieSearchRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.GlobalUserSearchRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.OperationalRoleMasterRequest;
import com.tcs.hpp.clinics.services.clinicadmin.jaxb.SearchGlobalPatientsRequest;
import com.tcs.hpp.platform.exception.HPPApplicationException;

/**
 * @author 219331
 * Descripton: This class is used to handle the GlobalSearch Module field and Business validations...
 */
public final class HPPClinicsGlobalSearchServiceHelper {

	private static Logger logger=Logger.getLogger(HPPClinicsGlobalSearchServiceHelper.class);
	private HPPClinicsGlobalSearchServiceHelper()
	{
		
	}
	
	
	/**
	 * This method is used to perform getOperationalRoleMaster FieldValidations.
	 * @param OperationalRoleMasterRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getOperationalRoleMasterFiledValidations(OperationalRoleMasterRequest operationalRoleMasterRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_ROLE_MASTER_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(operationalRoleMasterRequest.getToken()==null || "".equals(operationalRoleMasterRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(operationalRoleMasterRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(operationalRoleMasterRequest.getUserLoginName()==null || "".equals(operationalRoleMasterRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(operationalRoleMasterRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(operationalRoleMasterRequest.getOrganisationId()==null || "".equals(operationalRoleMasterRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(operationalRoleMasterRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_ROLE_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getOperationalRoleMaster BusinessValidations.
	 * @param operationalRoleMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getOperationalRoleMasterBusinessValidations(OperationalRoleMasterRequest operationalRoleMasterRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_ROLE_MASTER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
		
		Long.parseLong(operationalRoleMasterRequest.getOrganisationId());
	   }catch(NumberFormatException nfe)
		{
			
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_ROLE_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_ROLE_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	
	/**
	 * This method is used to perform getAutoSuggestSpeciality FieldValidations.
	 * @param AutoSearchSpecialityRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestSpecialityFieldValidations(AutoSearchSpecialityRequest autoSearchSpecialityRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGEST_SPECIALITY_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(autoSearchSpecialityRequest.getToken()==null || "".equals(autoSearchSpecialityRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(autoSearchSpecialityRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(autoSearchSpecialityRequest.getUserLoginName()==null || "".equals(autoSearchSpecialityRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(autoSearchSpecialityRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(autoSearchSpecialityRequest.getOrganisationId()==null || "".equals(autoSearchSpecialityRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(autoSearchSpecialityRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			
		}catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGEST_SPECIALITY_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGEST_SPECIALITY_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGEST_SPECIALITY_FIELD_VALIDATION);
		return errorMsgList;
	}
	/**
	 * This method is used to perform getAutoSuggestSpeciality BusinessValidations.
	 * @param AutoSearchSpecialityRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getAutoSuggestSpecialityBusinessValidations(AutoSearchSpecialityRequest autoSearchSpecialityRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGEST_SPECIALITY_BUSINESS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
			Long.parseLong(autoSearchSpecialityRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGEST_SPECIALITY_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_AUTO_SUGGEST_SPECIALITY_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_AUTO_SUGGEST_SPECIALITY_BUSINESS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform getSpecialtyMaster FieldValidations.
	 * @param getSpecialityRequest
	 * @return  List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getSpecialtyMasterFiledValidations(GetSpecialityRequest getSpecialityRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		try
		{
			if(getSpecialityRequest.getToken()==null || "".equals(getSpecialityRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(getSpecialityRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(getSpecialityRequest.getUserLoginName()==null || "".equals(getSpecialityRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(getSpecialityRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(getSpecialityRequest.getOrganisationId()==null || "".equals(getSpecialityRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(getSpecialityRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(getSpecialityRequest.getIsClinicAdminUser()==null || "".equals(getSpecialityRequest.getIsClinicAdminUser()))
			{
				errorMsgList.add(HPPClinicAdminConstants.IS_CLINICADMIN_USER_FIELD);
			}
			else if(getSpecialityRequest.getIsClinicAdminUser().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.IS_CLINICADMIN_USER_FIELD);
			}
			if(errorMsgList.size()==0 && !HPPClinicAdminConstants.YES.equalsIgnoreCase(getSpecialityRequest.getIsClinicAdminUser()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(getSpecialityRequest.getIsClinicAdminUser()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_MSG);
			}
		}
		catch(HPPApplicationException hae)
		{
   			logger.error(hae.getMessage());
   			throw hae;
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_SPEIALTY_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_SPEIALTY_MASTER_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
	    }
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform getSpecialtyMaster BusinessValidations.
	 * @param getSpecialityRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> getSpecialtyMasterBusinessValidations(GetSpecialityRequest getSpecialityRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
		
			Long.parseLong(getSpecialityRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
	    catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_SPECIALTY_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_SPECIALTY_MASTER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GET_SPECIALTY_MASTER_BUSS_VALIDATION);
		return formatMsgList;
	}

	/**
	 * This method is used to perform searchGlobalClinicService FieldValidations.
	 * @param serviceMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchGlobalClinicServiceFieldValidations(GlobalClinicServcieSearchRequest globalClinicServcieSearchRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_CLINIC_SEARCH_FLIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		boolean isCategoryPresent = true;
		boolean isServiceClassificationPresent = true;
		boolean isServiceSearchTextPresent = true;
		if(globalClinicServcieSearchRequest.getToken()==null || "".equals(globalClinicServcieSearchRequest.getToken()))
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		else if(globalClinicServcieSearchRequest.getToken().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
		}
		if(globalClinicServcieSearchRequest.getUserLoginName()==null || "".equals(globalClinicServcieSearchRequest.getUserLoginName()))
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		else if(globalClinicServcieSearchRequest.getUserLoginName().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
		}
		if(globalClinicServcieSearchRequest.getOrganisationId()==null || "".equals(globalClinicServcieSearchRequest.getOrganisationId()))
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		else if(globalClinicServcieSearchRequest.getOrganisationId().trim().isEmpty())
		{
			errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(globalClinicServcieSearchRequest.getServiceCategory()==null || "".equals(globalClinicServcieSearchRequest.getServiceCategory()))
		{
			isCategoryPresent = false;
		}else if(globalClinicServcieSearchRequest.getServiceCategory().trim().isEmpty())
		{
			isCategoryPresent = false;
		}
		if(globalClinicServcieSearchRequest.getServiceClassificationId()==null || "".equals(globalClinicServcieSearchRequest.getServiceClassificationId()))
		{
			isServiceClassificationPresent = false;
		}else if(globalClinicServcieSearchRequest.getServiceClassificationId().trim().isEmpty())
		{
			isServiceClassificationPresent = false;
		}
		if(globalClinicServcieSearchRequest.getServiceName()==null || "".equals(globalClinicServcieSearchRequest.getServiceName()))
		{
			isServiceSearchTextPresent = false;
		}else if(globalClinicServcieSearchRequest.getServiceName().trim().isEmpty())
		{
			isServiceSearchTextPresent = false;
		}
		if(!isCategoryPresent && !isServiceClassificationPresent && !isServiceSearchTextPresent)
		{
			errorMsgList.add(HPPClinicAdminConstants.GLOBAL_SERVICE_SEARCH_ONE_OF_THREE_MUST_MSG);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_CLINIC_SEARCH_FLIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform searchGlobalClinicService BusinessValidations.
	 * @param globalClinicServcieSearchRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchGlobalClinicServiceBusinessValidations(GlobalClinicServcieSearchRequest globalClinicServcieSearchRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.SEARCH_GLOBAL_CLINIC_SERVICE_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
			try
			{
				Long.parseLong(globalClinicServcieSearchRequest.getOrganisationId());
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			try
			{
				if(globalClinicServcieSearchRequest.getServiceClassificationId()!=null && !"".equals(globalClinicServcieSearchRequest.getServiceClassificationId()) && !globalClinicServcieSearchRequest.getServiceClassificationId().trim().isEmpty())
				{
					Long.parseLong(globalClinicServcieSearchRequest.getServiceClassificationId());
				}
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.CLASSIFICATION_ID_FIELD);
			}
		
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.SEARCH_GLOBAL_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
		  throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.SEARCH_GLOBAL_CLINIC_SERVICE_BUSS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.SEARCH_GLOBAL_CLINIC_SERVICE_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	/**
	 * This method is used to perform searchUserGlobal FieldValidations.
	 * @param serviceMasterRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchUserGlobalFieldValidations(GlobalUserSearchRequest globalUserSearchRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_USER_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		boolean isUserNamePresent = true;
		boolean isMobileNoPresent = true;
		boolean isRoleIdPresent = true;
		boolean isSpecialtyPresent = true;
		try
		{
			if(globalUserSearchRequest.getToken()==null || "".equals(globalUserSearchRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(globalUserSearchRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(globalUserSearchRequest.getUserLoginName()==null || "".equals(globalUserSearchRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(globalUserSearchRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(globalUserSearchRequest.getOrganisationId()==null || "".equals(globalUserSearchRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(globalUserSearchRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(globalUserSearchRequest.getIsClinicAdminUser()==null || "".equals(globalUserSearchRequest.getIsClinicAdminUser()))
			{
				errorMsgList.add(HPPClinicAdminConstants.IS_CLINICADMIN_USER_FIELD);
			}
			else if(globalUserSearchRequest.getIsClinicAdminUser().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.IS_CLINICADMIN_USER_FIELD);
			}
			else if(!HPPClinicAdminConstants.YES.equalsIgnoreCase(globalUserSearchRequest.getIsClinicAdminUser()) && !HPPClinicAdminConstants.NO.equalsIgnoreCase(globalUserSearchRequest.getIsClinicAdminUser()))
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_ISCLINICADMIN_VALUE_INVALID_MSG);
			}
			if(globalUserSearchRequest.getUserName()==null || "".equals(globalUserSearchRequest.getUserName()))
			{
				isUserNamePresent = false;
			}else if(globalUserSearchRequest.getUserName().trim().isEmpty())
			{
				isUserNamePresent = false;
			}
			if(globalUserSearchRequest.getPhoneNumber()==null || "".equals(globalUserSearchRequest.getPhoneNumber()))
			{
				isMobileNoPresent = false;
			}else if(globalUserSearchRequest.getPhoneNumber().trim().isEmpty())
			{
				isMobileNoPresent = false;
			}
			if(globalUserSearchRequest.getUserType()==null || "".equals(globalUserSearchRequest.getUserType()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERTYPE_FIELD);
			}else if(globalUserSearchRequest.getUserType().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERTYPE_FIELD);
			}
			else if(globalUserSearchRequest.getUserType().equalsIgnoreCase(HPPClinicAdminConstants.DOCTOR))
			{
				if(globalUserSearchRequest.getSpeciality()==null || "".equals(globalUserSearchRequest.getSpeciality()))
				{
					isSpecialtyPresent = false;
				}else if(globalUserSearchRequest.getSpeciality().trim().isEmpty())
				{
					isSpecialtyPresent = false;
				}
				if(!isUserNamePresent && !isMobileNoPresent && !isSpecialtyPresent)
				{
					errorMsgList.add(HPPClinicAdminConstants.GLOBAL_USER_SEARCH_DOCTOR_ONE_OF_THREE_MUST_MSG);
				}
			}
			else if(globalUserSearchRequest.getUserType().equalsIgnoreCase(HPPClinicAdminConstants.STAFF))
			{
				if(globalUserSearchRequest.getRoleId()==null || "".equals(globalUserSearchRequest.getRoleId()))
				{
					isRoleIdPresent = false;
				}else if(globalUserSearchRequest.getRoleId().trim().isEmpty())
				{
					isRoleIdPresent = false;
				}
				if(!isUserNamePresent && !isMobileNoPresent && !isRoleIdPresent)
				{
					errorMsgList.add(HPPClinicAdminConstants.GLOBAL_USER_SEARCH_STAFF_ONE_OF_THREE_MUST_MSG);
				}
			}
			else
			{
				logger.error(HPPClinicAdminConstants.CLINICADMIN_USERTYPE_VALUE_INVALID_MSG);
				throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_USERTYPE_VALUE_INVALID_CODE, HPPClinicAdminConstants.CLINICADMIN_USERTYPE_VALUE_INVALID_MSG);
			}
				
		}
		catch(HPPApplicationException hae)
		{
    		logger.error(hae.getMessage());
    		throw hae;
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GLOBAL_USER_SEARCH_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GLOBAL_USER_SEARCH_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_USER_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform searchUserGlobal BusinessValidations.
	 * @param globalUserSearchRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchUserGlobalBusinessValidations(GlobalUserSearchRequest globalUserSearchRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_USER_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
		
		try
		{
			Long.parseLong(globalUserSearchRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		if(globalUserSearchRequest.getUserType().equalsIgnoreCase(HPPClinicAdminConstants.STAFF))
		{
			try
			{
				if(globalUserSearchRequest.getRoleId()!=null && !"".equals(globalUserSearchRequest.getRoleId()) && !globalUserSearchRequest.getRoleId().trim().isEmpty())
				{
					Long.parseLong(globalUserSearchRequest.getRoleId());
				}
				
			}
			catch(NumberFormatException nfe)
			{
				formatMsgList.add(HPPClinicAdminConstants.ROLEID_FIELD);
			}
		}
		try
		{
			if(globalUserSearchRequest.getPhoneNumber()!=null && !"".equals(globalUserSearchRequest.getPhoneNumber()) && !globalUserSearchRequest.getPhoneNumber().trim().isEmpty())
			{
				Long.parseLong(globalUserSearchRequest.getPhoneNumber());
			}
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.PHONE_NUM_FIELD);
		}
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_GLOBAL_SEARCH_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GLOBAL_SEARCH_USER_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_USER_BUSS_VALIDATION);
		return formatMsgList;
	}
	
	
	/**
	 * This method is used to perform searchGlobalPatients FieldValidations.
	 * @param searchGlobalPatientsRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchGlobalPatientsFieldValidations(SearchGlobalPatientsRequest searchGlobalPatientsRequest)throws HPPApplicationException
	{
		logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_PATIENTS_FIELD_VALIDATION);
		
		List<String> errorMsgList = new ArrayList<String>();
		boolean isPatientNamePresent = true;
		boolean isUHIDPresent = true;
		boolean isPhoneNumberPresent = true;
		try
		{
			if(searchGlobalPatientsRequest.getToken()==null || "".equals(searchGlobalPatientsRequest.getToken()))
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			else if(searchGlobalPatientsRequest.getToken().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.TOKEN_FIELD);
			}
			if(searchGlobalPatientsRequest.getUserLoginName()==null || "".equals(searchGlobalPatientsRequest.getUserLoginName()))
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			else if(searchGlobalPatientsRequest.getUserLoginName().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.USERLOGINNAME_FIELD);
			}
			if(searchGlobalPatientsRequest.getOrganisationId()==null || "".equals(searchGlobalPatientsRequest.getOrganisationId()))
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			else if(searchGlobalPatientsRequest.getOrganisationId().trim().isEmpty())
			{
				errorMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
			}
			if(searchGlobalPatientsRequest.getPatientName()==null || "".equals(searchGlobalPatientsRequest.getPatientName()))
			{
				isPatientNamePresent = false;
			}else if(searchGlobalPatientsRequest.getPatientName().trim().isEmpty())
			{
				isPatientNamePresent = false;
			}
			if(searchGlobalPatientsRequest.getUHID()==null || "".equals(searchGlobalPatientsRequest.getUHID()))
			{
				isUHIDPresent = false;
			}else if(searchGlobalPatientsRequest.getUHID().trim().isEmpty())
			{
				isUHIDPresent = false;
			}
			if(searchGlobalPatientsRequest.getPhoneNumber()==null || "".equals(searchGlobalPatientsRequest.getPhoneNumber()))
			{
				isPhoneNumberPresent = false;
			}else if(searchGlobalPatientsRequest.getPhoneNumber().trim().isEmpty())
			{
				isPhoneNumberPresent = false;
			}
			if(!isPatientNamePresent && !isUHIDPresent && !isPhoneNumberPresent)
			{
				errorMsgList.add(HPPClinicAdminConstants.GLOBAL_PATIENT_SEARCH_ONE_OF_THREE_MUST_MSG);
			}	
			
		}
		catch(Exception e)
		{
			logger.error(HPPClinicAdminConstants.CLINICADMIN_GLOBAL_PATIENT_SEARCH_FIELD_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GLOBAL_PATIENT_SEARCH_FIELD_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_PATIENTS_FIELD_VALIDATION);
		return errorMsgList;
	}
	
	/**
	 * This method is used to perform searchGlobalPatients BusinessValidations.
	 * @param searchGlobalPatientsRequest
	 * @return List<String>
	 * @throws HPPApplicationException
	 */
	public static List<String> searchGlobalPatientsBusinessValidations(SearchGlobalPatientsRequest searchGlobalPatientsRequest)throws HPPApplicationException
	{
	   logger.info(HPPClinicAdminConstants.METHOD_ENTRY_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_PATIENT_BUSS_VALIDATION);
		
	   List<String> formatMsgList = new ArrayList<String>();
	   try
	   {
	
		try
		{
			Long.parseLong(searchGlobalPatientsRequest.getOrganisationId());
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.ORG_FIELD);
		}
		try
		{
			if(searchGlobalPatientsRequest.getPhoneNumber()!=null && !"".equals(searchGlobalPatientsRequest.getPhoneNumber()) && !searchGlobalPatientsRequest.getPhoneNumber().trim().isEmpty())
			{
				Long.parseLong(searchGlobalPatientsRequest.getPhoneNumber());
			}
		}
		catch(NumberFormatException nfe)
		{
			formatMsgList.add(HPPClinicAdminConstants.PHONE_NUM_FIELD);
		}
	   }
	   catch(Exception e)
		{
		   logger.error(HPPClinicAdminConstants.CLINICADMIN_GLOBAL_SEARCH_PATIENT_BUSINESS_VALIDATION_EXCEPTION_MSG+"::"+e.getMessage());
			throw new HPPApplicationException(HPPClinicAdminConstants.CLINICADMIN_EXCEPTION_CODE, HPPClinicAdminConstants.CLINICADMIN_GLOBAL_SEARCH_PATIENT_BUSINESS_VALIDATION_EXCEPTION_MSG+HPPClinicAdminConstants.EXCEPTION_MSG_SPLIT_TOKNE+e);
		}
		logger.info(HPPClinicAdminConstants.METHOD_EXIT_MSG+HPPClinicAdminConstants.GLOBAL_SEARCH_PATIENT_BUSS_VALIDATION);
		return formatMsgList;
	}
}
