
package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Java class for UpdateTariffResponse element declaration.
* 
* <p>The following schema fragment specifies the expected content contained within this class.
* 
* <pre>
* &lt;element name="UpdateTariffResponse">
*   &lt;complexType>
*     &lt;complexContent>
*       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*         &lt;sequence>
*           &lt;element name="SuccessMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
*         &lt;/sequence>
*       &lt;/restriction>
*     &lt;/complexContent>
*   &lt;/complexType>
* &lt;/element>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 
 "successMessage"
})
@XmlRootElement(name = "UpdateTariffResponse")
public class UpdateTariffResponse {

 @XmlElement(name = "SuccessMessage", required = true)
 private String successMessage;
 
 
/**
 * Gets the value of the successMessage property.
 * @return successMessage
 */
public String getSuccessMessage() {
		return successMessage;
	}

	/**
	 * Sets the value of the successMessage property.
	 * @param successMessage
	 */
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

}
