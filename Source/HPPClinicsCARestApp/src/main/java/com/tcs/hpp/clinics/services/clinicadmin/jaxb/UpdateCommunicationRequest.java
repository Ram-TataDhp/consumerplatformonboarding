package com.tcs.hpp.clinics.services.clinicadmin.jaxb;

public class UpdateCommunicationRequest {
	
	String token;
	String userLoginName;
	String organizationId;
	String receiptUser;
	String workflow;
	String smsMessage;
	String communicationType;
	String mailSubject;
	String mailBody;
	String clinicUserId;
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the userLoginName
	 */
	public String getUserLoginName() {
		return userLoginName;
	}
	/**
	 * @param userLoginName the userLoginName to set
	 */
	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}
	/**
	 * @return the organizationId
	 */
	public String getOrganizationId() {
		return organizationId;
	}
	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	/**
	 * @return the receiptUser
	 */
	public String getReceiptUser() {
		return receiptUser;
	}
	/**
	 * @param receiptUser the receiptUser to set
	 */
	public void setReceiptUser(String receiptUser) {
		this.receiptUser = receiptUser;
	}
	/**
	 * @return the workflow
	 */
	public String getWorkflow() {
		return workflow;
	}
	/**
	 * @param workflow the workflow to set
	 */
	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}
	/**
	 * @return the smsMessage
	 */
	public String getSmsMessage() {
		return smsMessage;
	}
	/**
	 * @param smsMessage the smsMessage to set
	 */
	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}
	/**
	 * @return the communicationType
	 */
	public String getCommunicationType() {
		return communicationType;
	}
	/**
	 * @param communicationType the communicationType to set
	 */
	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}
	/**
	 * @return the mailSubject
	 */
	public String getMailSubject() {
		return mailSubject;
	}
	/**
	 * @param mailSubject the mailSubject to set
	 */
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	/**
	 * @return the mailBody
	 */
	public String getMailBody() {
		return mailBody;
	}
	/**
	 * @param mailBody the mailBody to set
	 */
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	/**
	 * @return the clinicUserId
	 */
	public String getClinicUserId() {
		return clinicUserId;
	}
	/**
	 * @param clinicUserId the clinicUserId to set
	 */
	public void setClinicUserId(String clinicUserId) {
		this.clinicUserId = clinicUserId;
	}
	
		

}
