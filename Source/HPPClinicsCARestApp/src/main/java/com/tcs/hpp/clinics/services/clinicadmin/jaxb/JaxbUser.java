/**
 * 
 */
package com.tcs.hpp.clinics.services.clinicadmin.jaxb;


/**
 * @author 219331
 *
 */
public class JaxbUser {

   
    private String userId;
  
    private String userName;
  
    private String title;
   
    private String gender;
   
    private String bloodGroup;
   
    private String dob;
  
    private String mobileNumber;
  
    private String speciality;
   
    private String email;
   
    private String address;
  
    private String medicalRegNo;
   
    private String signature;
   
    private String userPhoto;
   
    private String roleType;
    
    private String isActive;
   
    private String organisationId;
    private String organisationName;
    private String isVirtualClinic;

	/**
	 * Gets the value of the userId property.
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the value of the userId property.
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the value of the userName property.
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	/**
	 * Sets the value of the userName property.
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the value of the title property.
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the value of the gender property.
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the value of the gender property.
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the value of the bloodGroup property.
	 * @return the bloodGroup
	 */
	public String getBloodGroup() {
		return bloodGroup;
	}

	/**
	 * Sets the value of the bloodGroup property.
	 * @param bloodGroup the bloodGroup to set
	 */
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	/**
	 * Gets the value of the dob property.
	 * @return the dob
	 */
	public String getDOB() {
		return dob;
	}

	/**
	 * Sets the value of the dob property.
	 * @param dob the dob to set
	 */
	public void setDOB(String dob) {
		this.dob = dob;
	}

	/**
	 * Gets the value of the mobileNumber property.
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Sets the value of the mobileNumber property.
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Gets the value of the speciality property.
	 * @return the speciality
	 */
	public String getSpeciality() {
		return speciality;
	}

	/**
	 * Sets the value of the speciality property.
	 * @param speciality the speciality to set
	 */
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	/**
	 * Gets the value of the email property.
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the value of the email property.
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the value of the address property.
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the value of the address property.
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the value of the medicalRegNo property.
	 * @return the medicalRegNo
	 */
	public String getMedicalRegNo() {
		return medicalRegNo;
	}

	/**
	 * Sets the value of the medicalRegNo property.
	 * @param medicalRegNo the medicalRegNo to set
	 */
	public void setMedicalRegNo(String medicalRegNo) {
		this.medicalRegNo = medicalRegNo;
	}

	/**
	 * Gets the value of the signature property.
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the value of the signature property.
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * Gets the value of the userPhoto property.
	 * @return the userPhoto
	 */
	public String getUserPhoto() {
		return userPhoto;
	}

	/**
	 * Sets the value of the userPhoto property.
	 * @param userPhoto the userPhoto to set
	 */
	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	/**
	 * Gets the value of the roleType property.
	 * @return the roleType
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * Sets the value of the roleType property.
	 * @param roleType the roleType to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * Gets the value of the isActive property.
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * Sets the value of the isActive property.
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the value of the organisationId property.
	 * @return the organisationId
	 */
	public String getOrganisationId() {
		return organisationId;
	}

	/**
	 * Sets the value of the organisationId property.
	 * @param organisationId the organisationId to set
	 */
	public void setOrganisationId(String organisationId) {
		this.organisationId = organisationId;
	}

	public String getIsVirtualClinic() {
		return isVirtualClinic;
	}

	public void setIsVirtualClinic(String isVirtualClinic) {
		this.isVirtualClinic = isVirtualClinic;
	}
    
    
    

}
